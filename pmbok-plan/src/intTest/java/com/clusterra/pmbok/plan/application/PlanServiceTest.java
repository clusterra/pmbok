/*
 * Copyright (c) 2014.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * 	http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.clusterra.pmbok.plan.application;

import com.clusterra.iam.core.application.group.GroupDescriptor;
import com.clusterra.iam.core.application.group.GroupService;
import com.clusterra.iam.core.application.membership.AuthorizedMembershipService;
import com.clusterra.iam.core.application.role.RoleDescriptor;
import com.clusterra.iam.core.application.role.RoleService;
import com.clusterra.iam.core.application.tenant.TenantCommandService;
import com.clusterra.iam.core.application.tenant.TenantId;
import com.clusterra.iam.core.application.tracker.IdentityTrackerLifeCycle;
import com.clusterra.iam.core.application.user.DefaultRole;
import com.clusterra.iam.core.application.user.UserCommandService;
import com.clusterra.iam.core.application.user.UserId;
import com.clusterra.iam.core.domain.model.tenant.Tenant;
import com.clusterra.iam.core.testutil.UserRandomUtil;
import com.clusterra.pmbok.plan.domain.model.Task;
import com.clusterra.pmbok.project.domain.model.Project;
import com.clusterra.pmbok.project.domain.service.ProjectDomainService;
import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.ConfigFileApplicationContextInitializer;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.testng.AbstractTransactionalTestNGSpringContextTests;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.util.Date;
import java.util.List;

import static org.apache.commons.lang3.RandomStringUtils.randomAlphabetic;
import static org.fest.assertions.api.Assertions.assertThat;

/**
 * Created by dkuchugurov on 25.03.2014.
 */
@ContextConfiguration(
        value = {"classpath*:META-INF/spring/*.xml"},
        initializers = ConfigFileApplicationContextInitializer.class)
public class PlanServiceTest extends AbstractTransactionalTestNGSpringContextTests {

    @Autowired
    private ProjectDomainService projectDomainService;


    @Autowired
    private TaskService taskService;

    @Autowired
    private TaskQueryService taskQueryService;

    private Project project;

    @Autowired
    private TenantCommandService tenantCommandService;

    private TenantId tenantId;

    @Autowired
    private UserCommandService userCommandService;

    private UserId userId;

    @Autowired
    private RoleService roleService;

    @Autowired
    private GroupService groupService;

    @Autowired
    private AuthorizedMembershipService authorizedMembershipService;

    @Autowired
    private IdentityTrackerLifeCycle identityTrackerLifeCycle;


    @BeforeMethod
    public void beforeMethod() throws Exception {
        Tenant tenant = tenantCommandService.create(randomAlphabetic(10), UserRandomUtil.getRandomEmail());
        tenantId = new TenantId(tenant.getId());
        project = projectDomainService.createProject(tenantId, randomAlphabetic(5));

        userId = new UserId(userCommandService.create(tenantId, randomAlphabetic(5), UserRandomUtil.getRandomEmail(), UserRandomUtil.randomPassword(), randomAlphabetic(5), randomAlphabetic(5)).getId());

        RoleDescriptor role = roleService.createRole(tenantId, DefaultRole.ADMIN);
        GroupDescriptor group = groupService.createGroup(tenantId, randomAlphabetic(5));

        authorizedMembershipService.createAuthorizedMembershipIfNotExists(tenantId, userId, role, group);

        identityTrackerLifeCycle.startTracking(userId, tenantId);
    }


    @Test(enabled = false)
    public void test_add_task() throws Exception {
        PlanId planId = null; //new PlanId(mainPlan.getId());


        Task task = taskService.create(randomAlphabetic(10));

        List<Task> tasks = taskQueryService.findAll(planId);
        assertThat(tasks).hasSize(1);
    }

    @Test(enabled = false)
    public void test_remove_task() throws Exception {
        PlanId planId = null; //new PlanId(mainPlan.getId());


        Task task = taskService.create(randomAlphabetic(10));

        List<Task> tasks = taskQueryService.findAll(planId);
        assertThat(tasks).contains(task);

        TaskId taskId = new TaskId(task.getId());

        tasks = taskQueryService.findAll(planId);
        assertThat(tasks).doesNotContain(task);
    }

    @Test(enabled = false)
    public void test_assign() throws Exception {
        PlanId planId = null; //new PlanId(mainPlan.getId());
        Task task = taskService.create(randomAlphabetic(10));

        Date startDate = new Date();
        Date finishDate = DateTime.now().plusDays(1).toDate();
        Task assigned = taskService.assign(new TaskId(task.getId()), userId, startDate, finishDate);
        assertThat(assigned.getAssigneeUserId()).isEqualTo(userId.getId());
        assertThat(assigned.getStartDate()).isEqualTo(startDate);
        assertThat(assigned.getFinishDate()).isEqualTo(finishDate);
    }

}
