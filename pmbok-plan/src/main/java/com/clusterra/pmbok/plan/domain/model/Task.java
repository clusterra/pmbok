/*
 * Copyright (c) 2014.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * 	http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.clusterra.pmbok.plan.domain.model;

import org.apache.commons.lang3.Validate;
import org.joda.time.DateTime;
import org.springframework.data.annotation.CreatedBy;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedBy;
import org.springframework.data.annotation.LastModifiedDate;

import javax.persistence.Basic;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import java.util.Date;

/**
 * Created by dkuchugurov on 21.03.14.
 */
@Entity
@Table(name = "pmb_task")
@SequenceGenerator(initialValue = 1, name = "pmb_task_seq", sequenceName = "pmb_task_seq")
public class Task {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "pmb_task_seq")
    private Long id;

    @ManyToOne
    private Task parent;

    @Basic
    private String description;

    @Enumerated(EnumType.STRING)
    private Status status;

    @Basic
    private Date startDate;

    @Basic
    private Date finishDate;

    @Basic
    private String assigneeUserId;

    @Basic
    @CreatedDate
    private Date createdDate;

    @Basic
    @CreatedBy
    private String createdByUserId;

    @Basic
    @LastModifiedDate
    private Date modifiedDate;

    @Basic
    @LastModifiedBy
    private String modfiedByUserId;

    Task() {
    }

    public Task(String description) {
        updateDescription(description);
        this.status = Status.NEW;
    }

    public void updateDescription(String description) {
        Validate.notEmpty(description, "description is empty");
        this.description = description;
    }

    public void setStartDate(Date startDate) {
        Validate.notNull(startDate, "startDate is null");
        if (finishDate != null) {
            Validate.isTrue(new DateTime(startDate).isBefore(finishDate.getTime()), "start must be earlier than finish:" + finishDate);
        }
        this.startDate = startDate;
    }

    public void setFinishDate(Date finishDate) {
        Validate.notNull(finishDate, "finishDate is null");
        if (startDate != null) {
            Validate.isTrue(new DateTime(finishDate).isAfter(startDate.getTime()), "finish must be later than start:" + startDate);
        }
        this.finishDate = finishDate;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    public void assign(String assigneeUserId, Date startDate, Date finishDate) {
        this.assigneeUserId = assigneeUserId;
        setStartDate(startDate);
        setFinishDate(finishDate);
    }

    public Long getId() {
        return id;
    }

    public Task getParent() {
        return parent;
    }

    public String getDescription() {
        return description;
    }

    public Date getCreatedDate() {
        return createdDate;
    }

    public String getCreatedByUserId() {
        return createdByUserId;
    }

    public Date getModifiedDate() {
        return modifiedDate;
    }

    public String getModfiedByUserId() {
        return modfiedByUserId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Task task = (Task) o;

        if (id != null ? !id.equals(task.id) : task.id != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        return id != null ? id.hashCode() : 0;
    }

    public String getAssigneeUserId() {
        return assigneeUserId;
    }

    public Date getStartDate() {
        return startDate;
    }

    public Date getFinishDate() {
        return finishDate;
    }
}
