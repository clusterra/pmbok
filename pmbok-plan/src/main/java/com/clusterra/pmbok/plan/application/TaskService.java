/*
 * Copyright (c) 2014.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * 	http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.clusterra.pmbok.plan.application;

import com.clusterra.iam.core.application.user.UserId;
import com.clusterra.pmbok.plan.domain.model.Task;

import java.util.Date;

/**
 * Created by dkuchugurov on 31.03.2014.
 */
public interface TaskService {
    Task create(String description);

    Task assign(TaskId taskId, UserId assignee, Date startDate, Date finishDate) throws TaskNotFoundException;

}
