/*
 * Copyright (c) 2014.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * 	http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.clusterra.pmbok.plan.application;

import com.clusterra.iam.core.application.user.UserId;
import com.clusterra.pmbok.plan.domain.model.Task;
import com.clusterra.pmbok.plan.domain.model.TaskRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;

/**
 * Created by dkuchugurov on 31.03.2014.
 */
@Service
public class TaskServiceImpl implements TaskService {

    @Autowired
    private TaskRepository taskRepository;

    @Transactional
    public Task create(String description) {
        Task task = new Task(description);
        taskRepository.save(task);
        return task;
    }

    @Transactional
    public Task assign(TaskId taskId, UserId assignee, Date startDate, Date finishDate) throws TaskNotFoundException {
        Task task = getTask(taskId);

        task.assign(assignee.getId(), startDate, finishDate);
        return task;
    }

    private Task getTask(TaskId taskId) throws TaskNotFoundException {
        Task task = taskRepository.findOne(taskId.getId());
        if (task == null) {
            throw new TaskNotFoundException(taskId);
        }
        return task;
    }
}
