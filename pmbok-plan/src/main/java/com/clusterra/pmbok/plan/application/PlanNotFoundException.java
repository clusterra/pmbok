/*
 * Copyright (c) 2014.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * 	http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.clusterra.pmbok.plan.application;

/**
 * Created by dkuchugurov on 02.03.14.
 */
public class PlanNotFoundException extends Exception {

    public PlanNotFoundException(String projectId) {
        super("charter not found by project id " + projectId);
    }

    public PlanNotFoundException(PlanId planId) {
        super("charter not found by charter id " + planId);
    }
}
