
    create table pmb_task (
        id int8 not null,
        assigneeUserId varchar(255),
        createdByUserId varchar(255),
        createdDate timestamp,
        description varchar(255),
        finishDate timestamp,
        modfiedByUserId varchar(255),
        modifiedDate timestamp,
        startDate timestamp,
        status varchar(255),
        parent_id int8,
        primary key (id)
    );

    alter table pmb_task 
        add constraint FK_by5y32v3pi4vmof8h9kpvsgo 
        foreign key (parent_id) 
        references pmb_task;

    create sequence pmb_task_seq;
