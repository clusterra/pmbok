/*
 * Copyright (c) 2014.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * 	http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.clusterra.pmbok.rest.project.validate;

import com.clusterra.iam.core.application.tenant.InvalidTenantNameException;
import com.clusterra.iam.core.application.tenant.TenantAlreadyExistsException;
import com.clusterra.iam.core.application.tenant.TenantCommandService;
import com.clusterra.iam.core.application.tenant.TenantId;
import com.clusterra.iam.core.application.tenant.TenantNotFoundException;
import com.clusterra.iam.core.application.tracker.IdentityTrackerLifeCycle;
import com.clusterra.iam.core.application.user.EmailAlreadyExistsException;
import com.clusterra.iam.core.application.user.InvalidEmailException;
import com.clusterra.iam.core.application.user.LoginAlreadyExistsException;
import com.clusterra.iam.core.application.user.UserCommandService;
import com.clusterra.iam.core.application.user.UserId;
import com.clusterra.iam.core.domain.model.tenant.Tenant;
import com.clusterra.iam.core.domain.model.user.User;
import com.clusterra.iam.core.testutil.UserRandomUtil;
import com.clusterra.pmbok.project.application.ProjectCommandService;
import com.clusterra.pmbok.project.application.ProjectQueryService;
import com.clusterra.pmbok.project.domain.model.Project;
import com.clusterra.pmbok.project.domain.model.ProjectVersion;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.ConfigFileApplicationContextInitializer;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.testng.AbstractTestNGSpringContextTests;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;
import org.testng.annotations.BeforeMethod;

import static org.apache.commons.lang3.RandomStringUtils.randomAlphabetic;

/**
 * Created with IntelliJ IDEA.
 *
 * @author Denis Kuchugurov
 *         Date: 03.12.13
 */
@ContextConfiguration(
        value = {"classpath*:META-INF/spring/*.xml"},
        initializers = ConfigFileApplicationContextInitializer.class)
@WebAppConfiguration
public abstract class RestAbstractProjectTests extends AbstractTestNGSpringContextTests {

    @Autowired
    protected TenantCommandService tenantCommandService;

    @Autowired
    protected UserCommandService userCommandService;

    @Autowired
    protected ProjectCommandService projectCommandService;

    @Autowired
    protected ProjectQueryService projectQueryService;

    @Autowired
    private IdentityTrackerLifeCycle identityTrackerLifeCycle;

    @Autowired
    private WebApplicationContext wac;

    protected MockMvc mockMvc;

    protected TenantId tenantId;
    protected UserId userId;

    protected Project project;
    protected ProjectVersion projectVersion;

    @BeforeMethod
    public void setup() throws Exception {
        tenantId = createRandomTenant();
        userId = createRandomUser();
        mockMvc = MockMvcBuilders.webAppContextSetup(wac).build();
        identityTrackerLifeCycle.startTracking(userId, tenantId);

        project = projectCommandService.createProject(randomAlphabetic(5));
        projectVersion = projectQueryService.findAllVersionsBy(project.getProjectId()).iterator().next();
    }

    private UserId createRandomUser() throws EmailAlreadyExistsException, InvalidEmailException, LoginAlreadyExistsException, TenantNotFoundException {
        User user = userCommandService.create(tenantId, randomAlphabetic(5), UserRandomUtil.getRandomEmail(), UserRandomUtil.randomPassword(), randomAlphabetic(5), randomAlphabetic(5));
        return new UserId(user.getId());
    }

    protected TenantId createRandomTenant() throws TenantAlreadyExistsException, InvalidTenantNameException, InvalidEmailException {
        Tenant tenant = tenantCommandService.create(randomAlphabetic(5), UserRandomUtil.getRandomEmail());
        return new TenantId(tenant.getId());
    }


}
