/*
 * Copyright (c) 2014.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * 	http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.clusterra.pmbok.rest.project.validate;

import com.clusterra.pmbok.project.application.ProjectCommandService;
import com.clusterra.pmbok.project.domain.model.Project;
import com.clusterra.pmbok.rest.project.pod.ProjectPod;
import org.springframework.beans.factory.annotation.Autowired;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import javax.validation.ConstraintViolation;
import javax.validation.Validator;
import java.util.Set;

import static org.apache.commons.lang3.RandomStringUtils.randomAlphabetic;
import static org.fest.assertions.api.Assertions.assertThat;

public class UniqueInTenantConstraintValidatorTest extends RestAbstractProjectTests {

    @Autowired
    private ProjectCommandService projectCommandService;

    @Autowired
    private Validator validator;

    private Project project;

    @BeforeMethod
    public void beforeMethod() throws Exception {

        project = projectCommandService.createProject(randomAlphabetic(5));
    }

    @Test
    public void test() {
        Set<ConstraintViolation<ProjectPod>> violations = validator.validate(new ProjectPod(project.getName()));

        assertThat(violations).hasSize(1);
    }
}