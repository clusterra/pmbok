/*
 * Copyright (c) 2014.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * 	http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.clusterra.pmbok.rest.document;

import com.clusterra.pmbok.document.domain.model.document.DocumentId;
import com.clusterra.pmbok.pdf.application.DocumentPdfService;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import static org.mockito.Matchers.any;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

/**
 * Created by dkuchugurov on 07.03.14.
 */
@Test(enabled = false)
public class DocumentPdfControllerTest {


    @InjectMocks
    private DocumentDownloadController controller;

    @Mock
    private DocumentPdfService documentPdfService;

    private MockMvc mockMvc;

    @BeforeMethod
    public void beforeMethod() throws Exception {
        MockitoAnnotations.initMocks(this);
        mockMvc = MockMvcBuilders.standaloneSetup(controller).setMessageConverters(new MappingJackson2HttpMessageConverter()).build();
    }

    public void test_get_download_pdf() throws Exception {

        when(documentPdfService.generatePdf(any(DocumentId.class))).thenReturn(new byte[]{});
        when(documentPdfService.getFileName(any(DocumentId.class))).thenReturn("document.pdf");

        mockMvc.perform(get("/documents/{documentId}/download/pdf", "xxx"))
                .andDo(print())
                .andExpect(status().isOk());

    }

}
