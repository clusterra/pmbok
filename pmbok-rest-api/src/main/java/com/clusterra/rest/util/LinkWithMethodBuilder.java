/*
 * Copyright (c) 2014.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * 	http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.clusterra.rest.util;

import org.springframework.hateoas.Link;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 * @author Denis Kuchugurov.
 *         15.07.2014.
 */
public class LinkWithMethodBuilder extends Link {


    public static LinkWithMethod linkWithMethodGet(Link link) {
        return new LinkWithMethod(link, RequestMethod.GET);
    }

    public static LinkWithMethod linkWithMethodDelete(Link link) {
        return new LinkWithMethod(link, RequestMethod.DELETE);
    }

    public static LinkWithMethod linkWithMethodPost(Link link) {
        return new LinkWithMethod(link, RequestMethod.POST);
    }

    public static LinkWithMethod linkWithMethodPut(Link link) {
        return new LinkWithMethod(link, RequestMethod.PUT);
    }
}
