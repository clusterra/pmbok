/*
 * Copyright (c) 2014.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * 	http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.clusterra.pmbok.rest.template.resource;

import com.clusterra.pmbok.document.domain.model.template.section.SectionType;
import com.clusterra.pmbok.rest.template.TemplateController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.hateoas.mvc.ResourceAssemblerSupport;
import org.springframework.stereotype.Component;

import java.util.Locale;

/**
 * Created by dkuchugurov on 02.05.2014.
 */
@Component
public class SectionTypeResourceAssembler extends ResourceAssemblerSupport<SectionType, SectionTypeResource> {

    @Autowired
    private MessageSource messageSource;


    public SectionTypeResourceAssembler() {
        super(TemplateController.class, SectionTypeResource.class);
    }

    @Override
    protected SectionTypeResource instantiateResource(SectionType entity) {
        return new SectionTypeResource(entity, getName(entity), isMultiple(entity));
    }

    private Boolean isMultiple(SectionType entity) {
        switch (entity) {
            case SECTION_TITLE:
                return false;
            case SECTION_HISTORY:
                return false;
            case SECTION_REFERENCE:
                return false;
            case SECTION_TERM:
                return false;
            case SECTION_TEXT:
                return true;
            case SECTION_CONTENTS:
                return false;
            default:
                return false;
        }
    }

    private String getName(SectionType entity) {
        Locale locale = LocaleContextHolder.getLocale();
        switch (entity) {
            case SECTION_TITLE:
                return messageSource.getMessage("document.section.title.title", null, "title_", locale);
            case SECTION_HISTORY:
                return messageSource.getMessage("document.section.history.title", null, "history_", locale);
            case SECTION_REFERENCE:
                return messageSource.getMessage("document.section.reference.title", null, "reference_", locale);
            case SECTION_TERM:
                return messageSource.getMessage("document.section.term.title", null, "term_", locale);
            case SECTION_TEXT:
                return messageSource.getMessage("document.section.plaint-text.title", null, "plain_text_", locale);
            case SECTION_CONTENTS:
                return messageSource.getMessage("document.section.contents.title", null, "contents_", locale);
            default:
                throw new IllegalArgumentException("unknown section type " + entity);
        }
    }


    @Override
    public SectionTypeResource toResource(SectionType entity) {
        return instantiateResource(entity);
    }
}
