/*
 * Copyright (c) 2014.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * 	http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.clusterra.pmbok.rest.template.resource;

import com.clusterra.iam.core.application.user.UserId;
import com.clusterra.iam.core.application.user.UserNotFoundException;
import com.clusterra.iam.core.application.user.UserQueryService;
import com.clusterra.pmbok.document.domain.model.template.SectionTemplateAlreadyExistsException;
import com.clusterra.pmbok.rest.RelConstants;
import com.clusterra.pmbok.rest.template.TemplateController;
import com.clusterra.pmbok.document.domain.model.document.repo.DocumentRepository;
import com.clusterra.pmbok.document.domain.model.template.Template;
import com.clusterra.pmbok.document.domain.service.template.TemplateNotFoundException;
import com.clusterra.rest.util.RestMethods;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.mvc.ResourceAssemblerSupport;
import org.springframework.stereotype.Component;

import static com.clusterra.rest.util.LinkWithMethodBuilder.linkWithMethodDelete;
import static com.clusterra.rest.util.LinkWithMethodBuilder.linkWithMethodGet;
import static com.clusterra.rest.util.LinkWithMethodBuilder.linkWithMethodPost;
import static com.clusterra.rest.util.LinkWithMethodBuilder.linkWithMethodPut;
import static org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo;
import static org.springframework.hateoas.mvc.ControllerLinkBuilder.methodOn;

/**
 * Created by dkuchugurov on 02.05.2014.
 */
@Component
public class TemplateResourceAssembler extends ResourceAssemblerSupport<Template, TemplateResource> {

    @Autowired
    private UserQueryService userQueryService;

    @Autowired
    private DocumentRepository documentRepository;

    public TemplateResourceAssembler() {
        super(TemplateController.class, TemplateResource.class);
    }

    @Override
    protected TemplateResource instantiateResource(Template entity) {
        try {
            String createdBy = getDisplayName(entity.getCreatedByUserId());
            String modifiedBy = getDisplayName(entity.getModifiedByUserId());
            Integer usageCount = documentRepository.countBy(entity);
            return new TemplateResource(
                    entity.getTemplateId().getId(),
                    entity.getName(),
                    entity.getCreatedDate(),
                    createdBy,
                    entity.getModifiedDate(),
                    modifiedBy,
                    entity.getVersion().getAsString(), usageCount);
        } catch (UserNotFoundException e) {
            throw new RuntimeException(e);
        }
    }

    private String getDisplayName(String userId) throws UserNotFoundException {
        if (userId == null) {
            return "n/a";
        }
        return userQueryService.findUser(new UserId(userId)).getPerson().getDisplayName();
    }


    @Override
    public TemplateResource toResource(Template entity) {
        TemplateResource resource = instantiateResource(entity);
        try {
            String templateId = entity.getTemplateId().getId();
            resource.add(linkWithMethodGet(linkTo(methodOn(TemplateController.class).get(templateId)).withSelfRel()));
            resource.add(linkWithMethodDelete(linkTo(methodOn(TemplateController.class).delete(templateId)).withRel(RestMethods.DELETE.getName())));
            resource.add(linkWithMethodGet(linkTo(methodOn(TemplateController.class).getSections(templateId)).withRel(RelConstants.TEMPLATES_SECTIONS)));
            resource.add(linkWithMethodGet(linkTo(methodOn(TemplateController.class).getAvailableSectionTypes(templateId)).withRel(RelConstants.TEMPLATES_SECTIONS_TYPES)));
            resource.add(linkWithMethodPost(linkTo(methodOn(TemplateController.class).addHistorySection(templateId, null)).withRel(RelConstants.TEMPLATES_SECTION_ADD_HISTORY)));
            resource.add(linkWithMethodPost(linkTo(methodOn(TemplateController.class).addTermSection(templateId, null)).withRel(RelConstants.TEMPLATES_SECTION_ADD_TERM)));
            resource.add(linkWithMethodPost(linkTo(methodOn(TemplateController.class).addReferenceSection(templateId, null)).withRel(RelConstants.TEMPLATES_SECTION_ADD_REFERENCE)));
            resource.add(linkWithMethodPost(linkTo(methodOn(TemplateController.class).addTextSection(templateId, null)).withRel(RelConstants.TEMPLATES_SECTION_ADD_TEXT)));
            resource.add(linkWithMethodPost(linkTo(methodOn(TemplateController.class).addTitleSection(templateId, null)).withRel(RelConstants.TEMPLATES_SECTION_ADD_TITLE)));
            resource.add(linkWithMethodPost(linkTo(methodOn(TemplateController.class).addContentsSection(templateId, null)).withRel(RelConstants.TEMPLATES_SECTION_ADD_CONTENTS)));
            resource.add(linkWithMethodPut(linkTo(methodOn(TemplateController.class).updateTemplateName(templateId, null)).withRel(RelConstants.TEMPLATES_UPDATE_NAME)));

            return resource;
        } catch (SectionTemplateAlreadyExistsException | TemplateNotFoundException e) {
            throw new RuntimeException(e);
        }
    }
}
