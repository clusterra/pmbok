package com.clusterra.pmbok.rest.reference.resource;


import org.springframework.hateoas.ResourceSupport;

import java.util.Date;

public class ReferenceResource extends ResourceSupport {

    private final String referenceId;

    private final String number;

    private final String name;

    private final Date publicationDate;

    private final String author;

    public ReferenceResource(String referenceId, String number, String name, Date publicationDate, String author) {
        this.referenceId = referenceId;
        this.number = number;
        this.name = name;
        this.publicationDate = publicationDate;
        this.author = author;
    }

    public String getReferenceId() {
        return referenceId;
    }

    public String getNumber() {
        return number;
    }

    public String getName() {
        return name;
    }

    public Date getPublicationDate() {
        return publicationDate;
    }

    public String getAuthor() {
        return author;
    }
}
