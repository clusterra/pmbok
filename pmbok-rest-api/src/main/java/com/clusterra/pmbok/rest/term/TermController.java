/*
 * Copyright 2014 www.equmo.org
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * 	http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.clusterra.pmbok.rest.term;

import com.clusterra.pmbok.document.application.document.DocumentQueryService;
import com.clusterra.pmbok.document.domain.model.document.DocumentId;
import com.clusterra.pmbok.rest.term.resource.TermResource;
import com.clusterra.pmbok.rest.term.resource.TermResourceAssembler;
import com.clusterra.pmbok.term.application.TermService;
import com.clusterra.pmbok.term.domain.model.term.Term;
import com.clusterra.pmbok.term.domain.model.term.TermId;
import com.clusterra.pmbok.term.domain.service.TermNotFoundException;
import org.apache.commons.lang3.StringUtils;
import com.clusterra.iam.core.application.tenant.TenantNotFoundException;
import com.clusterra.iam.core.application.tracker.IdentityTracker;
import com.clusterra.iam.core.application.tracker.NotAuthenticatedException;
import com.clusterra.pmbok.document.domain.model.template.SectionTemplateNotFoundException;
import com.clusterra.pmbok.document.domain.service.document.DocumentNotFoundException;
import com.clusterra.rest.util.ResponseMessage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.support.DefaultMessageSourceResolvable;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.data.web.PagedResourcesAssembler;
import org.springframework.hateoas.PagedResources;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

/**
 * Created with IntelliJ IDEA.
 *
 * @author Denis Kuchugurov
 *         Date: 05.02.14
 */
@RestController
@RequestMapping(value = "pmbok/terms", produces = {MediaType.APPLICATION_JSON_VALUE})
public class TermController {

    @Autowired
    private TermService termService;

    @Autowired
    private IdentityTracker identityTracker;

    @Autowired
    private TermResourceAssembler termResourceAssembler;

    @Autowired
    private DocumentQueryService documentQueryService;

    @RequestMapping(value = "", method = RequestMethod.POST)
    public ResponseEntity<TermResource> create(@Valid @RequestBody TermPod pod) throws NotAuthenticatedException, TenantNotFoundException {
        Term term = termService.create(identityTracker.currentTenant(), pod.getName(), pod.getDescription());
        return new ResponseEntity<>(termResourceAssembler.toResource(term), HttpStatus.CREATED);
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    public ResponseEntity<TermResource> get(@PathVariable String id) throws TermNotFoundException {
        Term term = termService.findBy(new TermId(id));
        return new ResponseEntity<>(termResourceAssembler.toResource(term), HttpStatus.OK);
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.PUT)
    public ResponseEntity<TermResource> update(@PathVariable String id, @RequestBody TermPod pod) throws TermNotFoundException {
        Term term = termService.update(new TermId(id), pod.getName(), pod.getDescription());
        return new ResponseEntity<>(termResourceAssembler.toResource(term), HttpStatus.OK);
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
    public ResponseEntity<DefaultMessageSourceResolvable> delete(@PathVariable String id) throws TermNotFoundException {
        termService.delete(new TermId(id));
        return new ResponseEntity<>(ResponseMessage.message("term deleted"), HttpStatus.NO_CONTENT);
    }

    @RequestMapping(value = "/search", method = RequestMethod.GET)
    public ResponseEntity<PagedResources<TermResource>> search(@PageableDefault Pageable pageable,
                                                               @RequestParam(required = false) String searchBy,
                                                               @RequestParam(required = false) String documentId,
                                                               PagedResourcesAssembler<Term> assembler) throws NotAuthenticatedException, DocumentNotFoundException, SectionTemplateNotFoundException {
        if (!StringUtils.isEmpty(documentId)) {
            DocumentId docId = new DocumentId(documentId);
            Page<Term> termPage = documentQueryService.getAssociatedTerms(pageable, docId, searchBy);
            return new ResponseEntity<>(assembler.toResource(termPage, new TermResourceAssembler(docId, true, false)), HttpStatus.OK);
        }
        Page<Term> termPage = termService.findBy(pageable, searchBy);
        return new ResponseEntity<>(assembler.toResource(termPage, termResourceAssembler), HttpStatus.OK);
    }

}
