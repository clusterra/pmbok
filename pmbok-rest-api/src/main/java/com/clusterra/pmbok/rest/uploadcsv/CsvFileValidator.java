/*
 * Copyright (c) 2014.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * 	http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.clusterra.pmbok.rest.uploadcsv;

import org.springframework.web.multipart.MultipartFile;


/**
 * Created by Denis Kuchugurov on 02.06.2014.
 */
public abstract class CsvFileValidator {


    public static void validate(MultipartFile file) throws InvalidCsvFileException {

        String filename = file.getOriginalFilename();

        if (!(filename.toLowerCase().endsWith(".csv") || filename.toLowerCase().endsWith(".txt"))) {
            throw new InvalidCsvFileException(filename, "invalid file type, only .txt or .csv supported");
        }
    }

}


