/*
 * Copyright 2014 www.equmo.org
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * 	http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.clusterra.pmbok.rest.reference;

import com.clusterra.pmbok.reference.application.csv.InvalidCsvReferenceContentException;
import com.clusterra.pmbok.reference.domain.service.ReferenceNotFoundException;
import com.clusterra.pmbok.reference.application.csv.ReferenceCsvLoaderException;
import org.springframework.http.HttpStatus;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

import java.util.Arrays;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 *
 * @author Denis Kuchugurov
 *         Date: 06.02.14
 */
@ControllerAdvice
@SuppressWarnings("unused")
public class ReferenceExceptionHandler {

    @ExceptionHandler(ReferenceNotFoundException.class)
    @ResponseStatus(HttpStatus.UNPROCESSABLE_ENTITY)
    @ResponseBody
    public List<ObjectError> handle(ReferenceNotFoundException exception) {
        return Arrays.asList(new ObjectError("reference", exception.getMessage()));
    }

    @ExceptionHandler(ReferenceCsvLoaderException.class)
    @ResponseStatus(HttpStatus.UNPROCESSABLE_ENTITY)
    @ResponseBody
    public List<ObjectError> handle(ReferenceCsvLoaderException exception) {
        return Arrays.asList(new ObjectError("reference", exception.getMessage()));
    }

    @ExceptionHandler(InvalidCsvReferenceContentException.class)
    @ResponseStatus(HttpStatus.UNPROCESSABLE_ENTITY)
    @ResponseBody
    public List<ObjectError> handle(InvalidCsvReferenceContentException exception) {
        return Arrays.asList(new ObjectError("reference", exception.getMessage()));
    }

}
