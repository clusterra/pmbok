/*
 * Copyright (c) 2014.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * 	http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.clusterra.pmbok.rest.term.resource;

import com.clusterra.iam.core.application.tracker.NotAuthenticatedException;
import com.clusterra.pmbok.document.domain.model.document.DocumentId;
import com.clusterra.pmbok.document.domain.model.template.SectionTemplateNotFoundException;
import com.clusterra.pmbok.document.domain.service.document.DocumentNotFoundException;
import com.clusterra.pmbok.rest.RelConstants;
import com.clusterra.pmbok.rest.document.DocumentController;
import com.clusterra.pmbok.rest.term.TermController;
import com.clusterra.pmbok.term.domain.model.term.Term;
import com.clusterra.pmbok.term.domain.service.TermNotFoundException;
import com.clusterra.rest.util.RestMethods;
import org.springframework.hateoas.mvc.ResourceAssemblerSupport;
import org.springframework.stereotype.Component;

import static com.clusterra.rest.util.LinkWithMethodBuilder.linkWithMethodDelete;
import static com.clusterra.rest.util.LinkWithMethodBuilder.linkWithMethodPost;
import static com.clusterra.rest.util.LinkWithMethodBuilder.linkWithMethodPut;
import static org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo;
import static org.springframework.hateoas.mvc.ControllerLinkBuilder.methodOn;

/**
 * Created by dkuchugurov on 02.05.2014.
 */
@Component
public class TermResourceAssembler extends ResourceAssemblerSupport<Term, TermResource> {

    private final DocumentId documentId;
    private final Boolean unAssociateLink;
    private final Boolean associateLink;

    public TermResourceAssembler() {
        this(null, true, true);
    }

    public TermResourceAssembler(DocumentId documentId, Boolean unAssociateLink, Boolean associateLink) {
        super(TermController.class, TermResource.class);
        this.documentId = documentId;
        this.unAssociateLink = unAssociateLink;
        this.associateLink = associateLink;
    }

    @Override
    protected TermResource instantiateResource(Term entity) {
        return new TermResource(
                entity.getTermId().getId(),
                entity.getCreatedDate(),
                entity.getCreatedByUserId(),
                entity.getModifiedDate(),
                entity.getModifiedByUserId(),
                entity.getName(),
                entity.getDescription()
        );
    }

    @Override
    public TermResource toResource(Term term) {
        try {
            TermResource resource = createResourceWithId(term.getTermId(), term);
            resource.add(linkWithMethodPut(linkTo(methodOn(TermController.class).get(resource.getTermId())).withSelfRel()));
            resource.add(linkWithMethodPut(linkTo(methodOn(TermController.class).update(resource.getTermId(), null)).withRel(RestMethods.UPDATE.getName())));
            resource.add(linkWithMethodDelete(linkTo(methodOn(TermController.class).delete(resource.getTermId())).withRel(RestMethods.DELETE.getName())));

            if (documentId != null && unAssociateLink) {
                resource.add(linkWithMethodDelete(linkTo(methodOn(DocumentController.class).unAssociateTerm(documentId.getId(), term.getTermId().getId())).withRel(RelConstants.TERMS_UN_ASSOCIATE)));
            }
            if (documentId != null && associateLink) {
                resource.add(linkWithMethodPost(linkTo(methodOn(DocumentController.class).associateTerm(documentId.getId(), term.getTermId().getId())).withRel(RelConstants.TERMS_ASSOCIATE)));
            }
            return resource;
        } catch (SectionTemplateNotFoundException | DocumentNotFoundException | NotAuthenticatedException | TermNotFoundException e) {
            throw new RuntimeException(e);
        }
    }
}
