/*
 * Copyright (c) 2014.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * 	http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.clusterra.pmbok.rest.project.resource;

import com.clusterra.pmbok.document.domain.model.template.Template;
import com.clusterra.pmbok.rest.project.ProjectController;
import org.springframework.hateoas.mvc.ResourceAssemblerSupport;
import org.springframework.stereotype.Component;

/**
 * Created by dkuchugurov on 25.06.2014.
 */
@Component
public class DocumentTypeResourceAssembler extends ResourceAssemblerSupport<Template, DocumentTypeResource> {

    public DocumentTypeResourceAssembler() {
        super(ProjectController.class, DocumentTypeResource.class);
    }

    @Override
    protected DocumentTypeResource instantiateResource(Template entity) {
        return new DocumentTypeResource(entity.getTemplateId().getId(), entity.getName());
    }

    @Override
    public DocumentTypeResource toResource(Template entity) {
        return instantiateResource(entity);
    }
}
