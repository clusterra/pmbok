/*
 * Copyright (c) 2014.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * 	http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.clusterra.pmbok.rest.document;

import com.clusterra.pmbok.document.domain.model.document.DocumentId;
import com.clusterra.pmbok.pdf.application.DocumentPdfService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created by dkuchugurov on 26.02.14.
 */
@RestController
@RequestMapping(value = "pmbok/documents")
public class DocumentDownloadController {


    @Autowired
    private DocumentPdfService documentPdfService;


    @RequestMapping(value = "/{id}/download", method = RequestMethod.GET)
    public ResponseEntity<byte[]> download(@PathVariable String id) throws Exception {
        DocumentId documentId = new DocumentId(id);
        byte[] bytes = documentPdfService.generatePdf(documentId);
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.parseMediaType("application/pdf"));
        String filename = documentPdfService.getFileName(documentId);
        headers.setContentDispositionFormData(filename, filename);
        headers.setCacheControl("must-revalidate, post-check=0, pre-check=0");

        return new ResponseEntity<>(bytes, headers, HttpStatus.OK);
    }

}
