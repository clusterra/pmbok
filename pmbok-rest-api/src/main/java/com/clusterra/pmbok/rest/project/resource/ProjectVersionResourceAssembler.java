/*
 * Copyright (c) 2014.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * 	http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.clusterra.pmbok.rest.project.resource;

import com.clusterra.iam.core.application.tracker.NotAuthenticatedException;
import com.clusterra.pmbok.project.domain.model.ProjectVersion;
import com.clusterra.pmbok.project.domain.service.ProjectNotFoundException;
import com.clusterra.pmbok.project.domain.service.ProjectVersionNotFoundException;
import com.clusterra.pmbok.rest.RelConstants;
import com.clusterra.pmbok.rest.document.DocumentController;
import com.clusterra.pmbok.rest.project.ProjectController;
import org.springframework.hateoas.mvc.ControllerLinkBuilder;
import org.springframework.hateoas.mvc.ResourceAssemblerSupport;
import org.springframework.stereotype.Component;

import static org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo;
import static org.springframework.hateoas.mvc.ControllerLinkBuilder.methodOn;

/**
 * Created by dkuchugurov on 02.05.2014.
 */
@Component
public class ProjectVersionResourceAssembler extends ResourceAssemblerSupport<ProjectVersion, ProjectVersionResource> {

    public ProjectVersionResourceAssembler() {
        super(ProjectController.class, ProjectVersionResource.class);
    }

    @Override
    protected ProjectVersionResource instantiateResource(ProjectVersion entity) {
        return new ProjectVersionResource(
                entity.getProjectVersionId().getId(),
                entity.getLabel(),
                entity.getValue(),
                entity.isMain());
    }

    @Override
    public ProjectVersionResource toResource(ProjectVersion version) {
        try {
            ProjectVersionResource resource = createResourceWithId(version.getProjectVersionId(), version, version.getProject().getProjectId());
            resource.add(ControllerLinkBuilder.linkTo(methodOn(DocumentController.class).search(null, null, version.getProjectVersionId().getId(), null, null)).withRel(RelConstants.PROJECT_VERSION_SEARCH_DOCUMENTS));
            resource.add(linkTo(methodOn(ProjectController.class).docTypes(version.getProjectVersionId().getId())).withRel(RelConstants.PROJECT_VERSION_AVAILABLE_DOCUMENT_TYPES));
            resource.add(linkTo(methodOn(ProjectController.class).deleteVersion(version.getProjectVersionId().getId())).withRel(RelConstants.PROJECT_VERSION_DELETE));

            return resource;
        } catch (NotAuthenticatedException | ProjectVersionNotFoundException | ProjectNotFoundException e) {
            throw new RuntimeException(e);
        }
    }

}
