/*
 * Copyright (c) 2014.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * 	http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.clusterra.pmbok.rest.project.validate;

import com.clusterra.iam.core.application.tracker.NotAuthenticatedException;
import com.clusterra.iam.core.application.tracker.IdentityTracker;
import com.clusterra.pmbok.project.domain.model.repo.ProjectRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

/**
 * Created by dkuchugurov on 27.04.2014.
 */
public class UniqueInTenantConstraintValidator implements ConstraintValidator<UniqueInTenant, String> {

    @SuppressWarnings("unused")
    private static final Logger logger = LoggerFactory.getLogger(UniqueInTenantConstraintValidator.class);

    @Autowired
    private ProjectRepository projectRepository;

    @Autowired
    private IdentityTracker identityTracker;

    @Override
    public void initialize(UniqueInTenant uu) {
    }

    @Override
    public boolean isValid(String name, ConstraintValidatorContext cxt) {
        try {
            String id = identityTracker.currentTenant().getId();
            return name != null && projectRepository.findByTenantIdAndName(id, name) == null;
        } catch (NotAuthenticatedException e) {
            throw new RuntimeException(e);
        }
    }
}
