package com.clusterra.pmbok.rest.reference.resource;


import com.clusterra.iam.core.application.tracker.NotAuthenticatedException;
import com.clusterra.pmbok.document.domain.model.document.DocumentId;
import com.clusterra.pmbok.reference.domain.model.reference.Reference;
import com.clusterra.pmbok.reference.domain.service.ReferenceNotFoundException;
import com.clusterra.pmbok.rest.RelConstants;
import com.clusterra.pmbok.rest.document.DocumentController;
import com.clusterra.pmbok.document.domain.model.template.SectionTemplateNotFoundException;
import com.clusterra.pmbok.document.domain.service.document.DocumentNotFoundException;
import com.clusterra.pmbok.rest.reference.ReferenceController;
import com.clusterra.rest.util.RestMethods;
import org.springframework.hateoas.mvc.ControllerLinkBuilder;
import org.springframework.hateoas.mvc.ResourceAssemblerSupport;
import org.springframework.stereotype.Component;

import static com.clusterra.rest.util.LinkWithMethodBuilder.linkWithMethodDelete;
import static com.clusterra.rest.util.LinkWithMethodBuilder.linkWithMethodGet;
import static com.clusterra.rest.util.LinkWithMethodBuilder.linkWithMethodPost;
import static com.clusterra.rest.util.LinkWithMethodBuilder.linkWithMethodPut;
import static org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo;
import static org.springframework.hateoas.mvc.ControllerLinkBuilder.methodOn;

@Component
public class ReferenceResourceAssembler extends ResourceAssemblerSupport<Reference, ReferenceResource> {

    private final DocumentId documentId;
    private final Boolean unAssociateLink;
    private final Boolean associateLink;

    public ReferenceResourceAssembler() {
        this(null, true, true);
    }

    public ReferenceResourceAssembler(DocumentId documentId, Boolean unAssociateLink, Boolean associateLink) {
        super(ReferenceController.class, ReferenceResource.class);
        this.documentId = documentId;
        this.unAssociateLink = unAssociateLink;
        this.associateLink = associateLink;
    }

    @Override
    protected ReferenceResource instantiateResource(Reference entity) {
        return new ReferenceResource(
                entity.getReferenceId().getId(),
                entity.getNumber(),
                entity.getName(),
                entity.getPublicationDate(),
                entity.getAuthor());
    }

    @Override
    public ReferenceResource toResource(Reference reference) {
        try {
            ReferenceResource resource = instantiateResource(reference);
            resource.add(linkWithMethodGet(linkTo(methodOn(ReferenceController.class).get(resource.getReferenceId())).withSelfRel()));
            resource.add(linkWithMethodPut(linkTo(methodOn(ReferenceController.class).update(resource.getReferenceId(), null)).withRel(RestMethods.UPDATE.getName())));
            resource.add(linkWithMethodDelete(linkTo(methodOn(ReferenceController.class).delete(resource.getReferenceId())).withRel(RestMethods.DELETE.getName())));
            if (documentId != null && unAssociateLink) {
                resource.add(linkWithMethodDelete(ControllerLinkBuilder.linkTo(methodOn(DocumentController.class).unAssociateReference(documentId.getId(), reference.getReferenceId().getId())).withRel(RelConstants.REFERENCES_UN_ASSOCIATE)));
            }
            if (documentId != null && associateLink) {
                resource.add(linkWithMethodPost(linkTo(methodOn(DocumentController.class).associateReference(documentId.getId(), reference.getReferenceId().getId())).withRel(RelConstants.REFERENCES_ASSOCIATE)));
            }
            return resource;
        } catch (SectionTemplateNotFoundException | DocumentNotFoundException | NotAuthenticatedException | ReferenceNotFoundException e) {
            throw new RuntimeException(e);
        }
    }
}
