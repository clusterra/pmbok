/*
 * Copyright (c) 2014.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * 	http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.clusterra.pmbok.rest.document.resource;

import com.clusterra.iam.core.application.user.UserNotFoundException;
import com.clusterra.iam.core.application.user.UserQueryService;
import com.clusterra.pmbok.document.domain.model.document.DocumentRevision;
import com.clusterra.pmbok.rest.RelConstants;
import com.clusterra.pmbok.rest.document.DocumentController;
import com.clusterra.pmbok.rest.project.ProjectController;
import com.clusterra.pmbok.document.application.document.DocumentQueryService;
import com.clusterra.pmbok.document.domain.model.document.Document;
import com.clusterra.pmbok.document.domain.model.document.Status;
import com.clusterra.pmbok.document.domain.service.document.DocumentNotFoundException;
import com.clusterra.pmbok.rest.document.DocumentDownloadController;
import com.clusterra.rest.util.RestMethods;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.hateoas.mvc.ControllerLinkBuilder;
import org.springframework.hateoas.mvc.ResourceAssemblerSupport;
import org.springframework.stereotype.Component;

import java.util.Locale;

import static com.clusterra.rest.util.LinkWithMethodBuilder.linkWithMethodDelete;
import static org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo;
import static org.springframework.hateoas.mvc.ControllerLinkBuilder.methodOn;

/**
 * Created by dkuchugurov on 02.05.2014.
 */
@Component
public class DocumentResourceAssembler extends ResourceAssemblerSupport<Document, DocumentResource> {

    @Autowired
    private UserQueryService userQueryService;

    @Autowired
    private MessageSource messageSource;

    @Autowired
    private DocumentQueryService documentQueryService;

    public DocumentResourceAssembler() {
        super(DocumentController.class, DocumentResource.class);
    }

    @Override
    protected DocumentResource instantiateResource(Document entity) {
        try {
            String createdBy = userQueryService.findUser(entity.getCreatedByUserId()).getPerson().getDisplayName();
            String modifiedBy = userQueryService.findUser(entity.getModifiedByUserId()).getPerson().getDisplayName();
            String documentType = entity.getTemplate().getName();
            DocumentRevision revision = documentQueryService.getRevision(entity.getDocumentId());
            String status = getStatus(entity.getStatus());
            return new DocumentResource(
                    entity.getDocumentId(),
                    entity.getProjectVersionId(),
                    entity.getProjectId(),
                    status,
                    entity.getCreatedDate(),
                    createdBy,
                    entity.getModifiedDate(),
                    modifiedBy,
                    documentType,
                    revision);
        } catch (UserNotFoundException | DocumentNotFoundException e) {
            throw new RuntimeException(e);
        }
    }

    private String getStatus(Status status) {
        Locale locale = LocaleContextHolder.getLocale();
        switch (status) {
            case NEW:
                return messageSource.getMessage("document.status.new", null, locale);
            case EDITING:
                return messageSource.getMessage("document.status.editing", null, locale);
            case PUBLISHED:
                return messageSource.getMessage("document.status.published", null, locale);
            case APPROVED:
                return messageSource.getMessage("document.status.approved", null, locale);
            default:
                throw new IllegalArgumentException("unknown status " + status);
        }
    }

    @Override
    public DocumentResource toResource(Document document) {
        try {
            DocumentResource resource = createResourceWithId(document.getDocumentId(), document);
            resource.add(linkWithMethodDelete(linkTo(methodOn(DocumentController.class).delete(document.getDocumentId().getId())).withRel(RestMethods.DELETE.getName())));

            resource.add(ControllerLinkBuilder.linkTo(methodOn(ProjectController.class).get(document.getProjectId().getId())).withRel(RelConstants.DOCUMENT_PROJECT));
            resource.add(linkTo(methodOn(DocumentController.class).sections(document.getDocumentId().getId())).withRel(RelConstants.DOCUMENT_SECTIONS));

            if (!document.getStatus().equals(Status.PUBLISHED)) {
                resource.add(linkTo(methodOn(DocumentController.class).publish(document.getDocumentId().getId(), null)).withRel(RelConstants.DOCUMENT_PUBLISH));
            }
            if (!document.getStatus().equals(Status.APPROVED)) {
                resource.add(linkTo(methodOn(DocumentController.class).approve(document.getDocumentId().getId(), null)).withRel(RelConstants.DOCUMENT_APPROVE));
            }

            resource.add(linkTo(methodOn(DocumentDownloadController.class).download(document.getDocumentId().getId())).withRel(RelConstants.DOCUMENT_DOWNLOAD));
            resource.add(linkTo(methodOn(ProjectController.class).getVersion(document.getProjectVersionId().getId())).withRel(RelConstants.DOCUMENT_VERSION));
            return resource;
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

}
