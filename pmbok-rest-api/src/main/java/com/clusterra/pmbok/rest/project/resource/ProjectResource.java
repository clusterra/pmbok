/*
 * Copyright (c) 2014.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * 	http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.clusterra.pmbok.rest.project.resource;

import org.springframework.hateoas.ResourceSupport;

import java.util.Date;

/**
 * Created by dkuchugurov on 06.05.2014.
 */
public class ProjectResource extends ResourceSupport {

    private final String projectId;

    private final String name;

    private final String createdBy;

    private final Date createdDate;

    private final String modifiedBy;

    private final Date modifiedDate;


    public ProjectResource(String projectId, String name, String createdBy, Date createdDate, String modifiedBy, Date modifiedDate) {
        this.projectId = projectId;
        this.name = name;
        this.createdBy = createdBy;
        this.createdDate = createdDate;
        this.modifiedBy = modifiedBy;
        this.modifiedDate = modifiedDate;
    }

    public String getProjectId() {
        return projectId;
    }

    public String getName() {
        return name;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public Date getCreatedDate() {
        return createdDate;
    }

    public String getModifiedBy() {
        return modifiedBy;
    }

    public Date getModifiedDate() {
        return modifiedDate;
    }

}
