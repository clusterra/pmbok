/*
 * Copyright 2014 www.equmo.org
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * 	http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.clusterra.pmbok.rest.reference;

import com.clusterra.pmbok.document.application.document.DocumentQueryService;
import com.clusterra.pmbok.document.domain.model.document.DocumentId;
import com.clusterra.pmbok.document.domain.model.template.SectionTemplateNotFoundException;
import com.clusterra.pmbok.reference.application.ReferenceService;
import com.clusterra.pmbok.reference.domain.model.reference.Reference;
import com.clusterra.pmbok.reference.domain.model.reference.ReferenceId;
import com.clusterra.pmbok.reference.domain.service.ReferenceNotFoundException;
import com.clusterra.pmbok.rest.reference.resource.ReferenceResource;
import com.clusterra.pmbok.rest.reference.resource.ReferenceResourceAssembler;
import org.apache.commons.lang3.StringUtils;
import com.clusterra.iam.core.application.tenant.TenantNotFoundException;
import com.clusterra.iam.core.application.tracker.IdentityTracker;
import com.clusterra.iam.core.application.tracker.NotAuthenticatedException;
import com.clusterra.pmbok.document.domain.service.document.DocumentNotFoundException;
import com.clusterra.rest.util.ResponseMessage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.support.DefaultMessageSourceResolvable;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.data.web.PagedResourcesAssembler;
import org.springframework.hateoas.PagedResources;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

/**
 * Created with IntelliJ IDEA.
 *
 * @author Denis Kuchugurov
 *         Date: 05.02.14
 */
@RestController
@RequestMapping(value = "pmbok/references", produces = {MediaType.APPLICATION_JSON_VALUE})
public class ReferenceController {

    @Autowired
    private ReferenceService referenceService;

    @Autowired
    private IdentityTracker identityTracker;

    @Autowired
    private ReferenceResourceAssembler referenceResourceAssembler;

    @Autowired
    private DocumentQueryService documentQueryService;

    @RequestMapping(value = "", method = RequestMethod.POST)
    public ResponseEntity<ReferenceResource> create(@Valid @RequestBody ReferencePod pod) throws NotAuthenticatedException, TenantNotFoundException {
        Reference reference = referenceService.create(identityTracker.currentTenant(), pod.getNumber(), pod.getName(), pod.getPublicationDate(), pod.getAuthor());
        return new ResponseEntity<>(referenceResourceAssembler.toResource(reference), HttpStatus.CREATED);
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    public ResponseEntity<ReferenceResource> get(@PathVariable String id) throws ReferenceNotFoundException {
        Reference reference = referenceService.findBy(new ReferenceId(id));
        return new ResponseEntity<>(referenceResourceAssembler.toResource(reference), HttpStatus.OK);
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.PUT)
    public ResponseEntity<ReferenceResource> update(@PathVariable String id, @Valid @RequestBody ReferencePod pod) throws ReferenceNotFoundException {
        Reference reference = referenceService.update(new ReferenceId(id), pod.getNumber(), pod.getName(), pod.getPublicationDate(), pod.getAuthor());

        return new ResponseEntity<>(referenceResourceAssembler.toResource(reference), HttpStatus.OK);
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
    public ResponseEntity<DefaultMessageSourceResolvable> delete(@PathVariable String id) throws ReferenceNotFoundException {
        referenceService.delete(new ReferenceId(id));
        return new ResponseEntity<>(ResponseMessage.message("deleted"), HttpStatus.NO_CONTENT);
    }

    @RequestMapping(value = "/search", method = RequestMethod.GET)
    public ResponseEntity<PagedResources<ReferenceResource>> search(@PageableDefault Pageable pageable,
                                                                    @RequestParam(required = false, defaultValue = "") String searchBy,
                                                                    @RequestParam(required = false, defaultValue = "") String documentId,
                                                                    PagedResourcesAssembler<Reference> assembler) throws NotAuthenticatedException, DocumentNotFoundException, SectionTemplateNotFoundException {
        if (!StringUtils.isEmpty(documentId)) {
            DocumentId docId = new DocumentId(documentId);
            Page<Reference> referencePage = documentQueryService.getAssociatedReferences(pageable, docId, searchBy);
            return new ResponseEntity<>(assembler.toResource(referencePage, new ReferenceResourceAssembler(docId, true, false)), HttpStatus.OK);
        }
        Page<Reference> referencePage = referenceService.findBy(pageable, searchBy);
        return new ResponseEntity<>(assembler.toResource(referencePage, referenceResourceAssembler), HttpStatus.OK);
    }

}
