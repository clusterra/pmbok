/*
 * Copyright (c) 2014.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * 	http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.clusterra.pmbok.rest.project.pod;

/**
 * Created by kepkap on 18/11/14.
 */
public class StakeholdersPod {

    private final StakeholderPod customerOrganization;
    private final StakeholderPod customerManager;

    private final StakeholderPod executiveOrganization;
    private final StakeholderPod executiveManager;


    public StakeholdersPod(StakeholderPod customerOrganization, StakeholderPod customerManager, StakeholderPod executiveOrganization, StakeholderPod executiveManager) {
        this.customerOrganization = customerOrganization;
        this.customerManager = customerManager;
        this.executiveOrganization = executiveOrganization;
        this.executiveManager = executiveManager;
    }

    public StakeholderPod getCustomerOrganization() {
        return customerOrganization;
    }

    public StakeholderPod getCustomerManager() {
        return customerManager;
    }

    public StakeholderPod getExecutiveOrganization() {
        return executiveOrganization;
    }

    public StakeholderPod getExecutiveManager() {
        return executiveManager;
    }
}
