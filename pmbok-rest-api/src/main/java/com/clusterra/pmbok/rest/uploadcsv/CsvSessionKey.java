/*
 * Copyright 2014 www.equmo.org
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * 	http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.clusterra.pmbok.rest.uploadcsv;


import com.clusterra.iam.session.application.SessionStorageKey;

/**
 * Created by Denis Kuchugurov
 * 06.01.14
 */
public enum CsvSessionKey implements SessionStorageKey {

    UPLOAD_CSV_SESSION_KEY("upload_csv_session_key");

    private final String key;

    CsvSessionKey(String key) {
        this.key = key;
    }

    public String getKey() {
        return key;
    }
}
