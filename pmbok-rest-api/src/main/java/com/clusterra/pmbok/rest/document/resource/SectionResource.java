/*
 * Copyright (c) 2014.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * 	http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.clusterra.pmbok.rest.document.resource;

import com.clusterra.pmbok.document.domain.model.template.section.SectionType;
import org.springframework.hateoas.ResourceSupport;

/**
 * Created by Denis Kuchugurov on 29.05.2014.
 */
public class SectionResource extends ResourceSupport {

    private final String sectionTemplateId;

    private final SectionType sectionType;

    private final String title;

    private Object content;

    public SectionResource(String sectionTemplateId, SectionType sectionType, String title) {
        this.sectionTemplateId = sectionTemplateId;
        this.sectionType = sectionType;
        this.title = title;
    }

    public Object getContent() {
        return content;
    }

    public void setContent(Object content) {
        this.content = content;
    }

    public String getSectionTemplateId() {
        return sectionTemplateId;
    }

    public SectionType getSectionType() {
        return sectionType;
    }

    public String getTitle() {
        return title;
    }
}
