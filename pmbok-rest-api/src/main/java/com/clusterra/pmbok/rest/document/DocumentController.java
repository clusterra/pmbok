/*
 * Copyright 2014 www.equmo.org
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * 	http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.clusterra.pmbok.rest.document;

import com.clusterra.pmbok.document.domain.model.template.TemplateId;
import com.clusterra.pmbok.document.domain.model.template.section.SectionTemplateId;
import com.clusterra.pmbok.document.domain.service.document.DocumentAlreadyExistsException;
import com.clusterra.pmbok.document.domain.service.document.DocumentNotEditableException;
import com.clusterra.pmbok.project.domain.model.ProjectVersionId;
import com.clusterra.pmbok.project.domain.service.ProjectNotFoundException;
import com.clusterra.pmbok.rest.document.resource.DocumentResource;
import com.clusterra.pmbok.term.domain.model.term.TermId;
import com.clusterra.pmbok.term.domain.service.TermNotFoundException;
import org.apache.commons.lang3.StringUtils;
import com.clusterra.iam.core.application.tracker.NotAuthenticatedException;
import com.clusterra.pmbok.document.application.document.DocumentCommandService;
import com.clusterra.pmbok.document.application.document.DocumentQueryService;
import com.clusterra.pmbok.document.domain.model.document.Document;
import com.clusterra.pmbok.document.domain.model.document.DocumentId;
import com.clusterra.pmbok.document.domain.model.document.section.SectionContent;
import com.clusterra.pmbok.document.domain.model.document.section.text.PersistedTextSectionContent;
import com.clusterra.pmbok.document.domain.model.template.SectionTemplateNotFoundException;
import com.clusterra.pmbok.document.domain.service.document.DocumentNotFoundException;
import com.clusterra.pmbok.document.domain.service.template.TemplateNotFoundException;
import com.clusterra.pmbok.project.domain.model.ProjectId;
import com.clusterra.pmbok.project.domain.service.ProjectVersionNotFoundException;
import com.clusterra.pmbok.reference.domain.model.reference.ReferenceId;
import com.clusterra.pmbok.reference.domain.service.ReferenceNotFoundException;
import com.clusterra.pmbok.rest.document.resource.DocumentResourceAssembler;
import com.clusterra.pmbok.rest.document.resource.SectionResource;
import com.clusterra.pmbok.rest.document.resource.SectionResourceAssembler;
import com.clusterra.rest.util.ResponseMessage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.support.DefaultMessageSourceResolvable;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.web.PageableDefault;
import org.springframework.data.web.PagedResourcesAssembler;
import org.springframework.hateoas.ExposesResourceFor;
import org.springframework.hateoas.PagedResources;
import org.springframework.hateoas.Resources;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindException;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by dkuchugurov on 26.02.14.
 */
@RestController
@ExposesResourceFor(Document.class)
@RequestMapping(value = "pmbok/documents", produces = {MediaType.APPLICATION_JSON_VALUE})
public class DocumentController {

    @Autowired
    private DocumentCommandService documentCommandService;

    @Autowired
    private DocumentQueryService documentQueryService;

    @Autowired
    private SectionResourceAssembler sectionResourceAssembler;

    @Autowired
    private DocumentResourceAssembler documentResourceAssembler;


    @RequestMapping(value = "", method = RequestMethod.POST)
    public ResponseEntity<DocumentResource> create(@Valid @RequestBody DocumentPod documentPod,
                                                   BindingResult bindingResult
    ) throws BindException, ProjectNotFoundException, DocumentAlreadyExistsException, ProjectVersionNotFoundException, NotAuthenticatedException, TemplateNotFoundException {
        if (bindingResult.hasErrors()) {
            throw new BindException(bindingResult);
        }

        Document document = documentCommandService.create(new ProjectVersionId(documentPod.getProjectVersionId()), new TemplateId(documentPod.getTemplateId()));
        return new ResponseEntity<>(documentResourceAssembler.toResource(document), HttpStatus.CREATED);
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    public ResponseEntity<DocumentResource> get(@PathVariable String id) throws DocumentNotFoundException, DocumentNotEditableException {
        Document document = documentQueryService.findBy(new DocumentId(id));
        return new ResponseEntity<>(documentResourceAssembler.toResource(document), HttpStatus.OK);
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
    public ResponseEntity<DefaultMessageSourceResolvable> delete(@PathVariable String id) throws DocumentNotFoundException {
        documentCommandService.deleteBy(new DocumentId(id));
        return new ResponseEntity<>(ResponseMessage.message("deleted"), HttpStatus.NO_CONTENT);
    }

    @RequestMapping(value = "/{id}/publish", method = RequestMethod.PUT)
    public ResponseEntity<DocumentResource> publish(@PathVariable String id, @RequestBody CommentPod commentPod) throws DocumentNotFoundException, DocumentNotEditableException {
        Document document = documentCommandService.publish(new DocumentId(id), commentPod.getComment());
        return new ResponseEntity<>(documentResourceAssembler.toResource(document), HttpStatus.ACCEPTED);

    }

    @RequestMapping(value = "/{id}/approve", method = RequestMethod.PUT)
    public ResponseEntity<DocumentResource> approve(@PathVariable String id, @RequestBody CommentPod commentPod) throws DocumentNotFoundException, DocumentNotEditableException {
        Document document = documentCommandService.approve(new DocumentId(id), commentPod.getComment());
        return new ResponseEntity<>(documentResourceAssembler.toResource(document), HttpStatus.ACCEPTED);
    }

    @RequestMapping(value = "/{id}/edit", method = RequestMethod.PUT)
    public ResponseEntity<DocumentResource> edit(@PathVariable String id, @RequestBody CommentPod commentPod) throws DocumentNotFoundException, DocumentNotEditableException {
        Document document = documentCommandService.edit(new DocumentId(id));
        return new ResponseEntity<>(documentResourceAssembler.toResource(document), HttpStatus.ACCEPTED);
    }

    @RequestMapping(value = "/search", method = RequestMethod.GET)
    public ResponseEntity<PagedResources<DocumentResource>> search(@PageableDefault Pageable pageable,
                                                                   @RequestParam(required = false) String projectId,
                                                                   @RequestParam(required = false) String projectVersionId,
                                                                   @RequestParam(required = false) String searchBy,
                                                                   PagedResourcesAssembler<Document> assembler
    ) throws ProjectVersionNotFoundException, NotAuthenticatedException, ProjectNotFoundException {
        Page<Document> documents;
        pageable = fixSorting(pageable);
        if (!StringUtils.isEmpty(projectVersionId)) {
            documents = documentQueryService.findBy(pageable, new ProjectVersionId(projectVersionId), searchBy);
        } else if (!StringUtils.isEmpty(projectId)) {
            documents = documentQueryService.findBy(pageable, new ProjectId(projectId), searchBy);
        } else {
            documents = documentQueryService.findBy(pageable, searchBy);
        }
        PagedResources<DocumentResource> pagedResources = assembler.toResource(documents, documentResourceAssembler);
        return new ResponseEntity<>(pagedResources, HttpStatus.OK);
    }

    @RequestMapping(value = "/{id}/sections", method = RequestMethod.GET)
    public ResponseEntity<Resources<SectionResource>> sections(@PathVariable String id) throws DocumentNotFoundException, TemplateNotFoundException, NotAuthenticatedException {
        List<SectionContent> sectionContents = documentQueryService.findSectionContents(new DocumentId(id));
        return new ResponseEntity<>(new Resources<>(sectionResourceAssembler.toResources(sectionContents)), HttpStatus.OK);
    }

    @RequestMapping(value = "/{id}/terms/{termId}", method = RequestMethod.POST)
    public ResponseEntity<DefaultMessageSourceResolvable> associateTerm(@PathVariable String id, @PathVariable String termId) throws TermNotFoundException, NotAuthenticatedException, DocumentNotFoundException, SectionTemplateNotFoundException {
        documentCommandService.createAssociation(new DocumentId(id), new TermId(termId));
        return new ResponseEntity<>(ResponseMessage.message("association created"), HttpStatus.CREATED);
    }

    @RequestMapping(value = "/{id}/terms/{termId}", method = RequestMethod.DELETE)
    public ResponseEntity<DefaultMessageSourceResolvable> unAssociateTerm(@PathVariable String id, @PathVariable String termId) throws TermNotFoundException, NotAuthenticatedException, DocumentNotFoundException, SectionTemplateNotFoundException {
        documentCommandService.deleteAssociation(new DocumentId(id), new TermId(termId));
        return new ResponseEntity<>(ResponseMessage.message("association removed"), HttpStatus.NO_CONTENT);
    }

    @RequestMapping(value = "/{id}/references/{referenceId}", method = RequestMethod.POST)
    public ResponseEntity<DefaultMessageSourceResolvable> associateReference(@PathVariable String id, @PathVariable String referenceId) throws ReferenceNotFoundException, NotAuthenticatedException, DocumentNotFoundException, SectionTemplateNotFoundException {
        documentCommandService.createAssociation(new DocumentId(id), new ReferenceId(referenceId));
        return new ResponseEntity<>(ResponseMessage.message("association created"), HttpStatus.CREATED);
    }

    @RequestMapping(value = "/{id}/references/{referenceId}", method = RequestMethod.DELETE)
    public ResponseEntity<DefaultMessageSourceResolvable> unAssociateReference(@PathVariable String id, @PathVariable String referenceId) throws ReferenceNotFoundException, DocumentNotFoundException, NotAuthenticatedException, SectionTemplateNotFoundException {
        documentCommandService.deleteAssociation(new DocumentId(id), new ReferenceId(referenceId));
        return new ResponseEntity<>(ResponseMessage.message("association removed"), HttpStatus.NO_CONTENT);
    }

    @RequestMapping(value = "/{id}/sections/{sectionTemplateId}/update-text", method = RequestMethod.POST)
    public ResponseEntity<SectionResource> updateText(@PathVariable String id, @PathVariable String sectionTemplateId, @Valid @RequestBody TextPod textPod) throws DocumentNotFoundException, SectionTemplateNotFoundException {
        PersistedTextSectionContent persistedTextSectionContent = documentCommandService.updateTextSection(new DocumentId(id), new SectionTemplateId(sectionTemplateId), textPod.getText());
        return new ResponseEntity<>(sectionResourceAssembler.toResource(persistedTextSectionContent), HttpStatus.OK);
    }

    private static Pageable fixSorting(Pageable pageable) {

        if (pageable.getSort() == null) {
            return pageable;
        }
        List<Sort.Order> orders = new ArrayList<>(1);
        for (Sort.Order order : pageable.getSort()) {
            switch (order.getProperty()) {
                case "documentType":
                    orders.add(new Sort.Order(order.getDirection(), "template.name"));
                    break;
                case "documentId":
                    orders.add(new Sort.Order(order.getDirection(), "id"));
                    break;
                default:
                    orders.add(order);

            }
        }
        return new PageRequest(pageable.getPageNumber(), pageable.getPageSize(), new Sort(orders));
    }

}
