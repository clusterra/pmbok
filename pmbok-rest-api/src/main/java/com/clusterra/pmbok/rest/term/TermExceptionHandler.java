/*
 * Copyright 2014 www.equmo.org
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * 	http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.clusterra.pmbok.rest.term;

import com.clusterra.pmbok.term.domain.service.TermNotFoundException;
import com.clusterra.pmbok.term.application.csv.InvalidCsvTermContentException;
import com.clusterra.pmbok.term.application.csv.TermCsvLoaderException;
import org.springframework.http.HttpStatus;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

import java.util.Arrays;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 *
 * @author Denis Kuchugurov
 *         Date: 06.02.14
 */
@ControllerAdvice
@SuppressWarnings("unused")
public class TermExceptionHandler {

    @ExceptionHandler(TermNotFoundException.class)
    @ResponseStatus(HttpStatus.UNPROCESSABLE_ENTITY)
    @ResponseBody
    public List<ObjectError> handle(TermNotFoundException exception) {
        return Arrays.asList(new ObjectError("term", exception.getMessage()));
    }

    @ExceptionHandler(TermCsvLoaderException.class)
    @ResponseStatus(HttpStatus.UNPROCESSABLE_ENTITY)
    @ResponseBody
    public List<ObjectError> handle(TermCsvLoaderException exception) {
        return Arrays.asList(new ObjectError("term", exception.getMessage()));
    }

    @ExceptionHandler(InvalidCsvTermContentException.class)
    @ResponseStatus(HttpStatus.UNPROCESSABLE_ENTITY)
    @ResponseBody
    public List<ObjectError> handle(InvalidCsvTermContentException exception) {
        return Arrays.asList(new ObjectError("term", exception.getMessage()));
    }
}
