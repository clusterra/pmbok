/*
 * Copyright (c) 2014.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * 	http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.clusterra.pmbok.rest.template.resource;

import com.clusterra.iam.core.application.user.UserId;
import com.clusterra.iam.core.application.user.UserNotFoundException;
import com.clusterra.iam.core.application.user.UserQueryService;
import com.clusterra.pmbok.document.domain.model.template.section.SectionTemplate;
import com.clusterra.pmbok.rest.RelConstants;
import com.clusterra.pmbok.rest.template.TemplateController;
import com.clusterra.pmbok.document.domain.model.template.SectionTemplateNotFoundException;
import com.clusterra.pmbok.document.domain.service.template.TemplateNotFoundException;
import com.clusterra.rest.util.RestMethods;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.mvc.ResourceAssemblerSupport;
import org.springframework.stereotype.Component;

import static com.clusterra.rest.util.LinkWithMethodBuilder.linkWithMethodDelete;
import static com.clusterra.rest.util.LinkWithMethodBuilder.linkWithMethodGet;
import static com.clusterra.rest.util.LinkWithMethodBuilder.linkWithMethodPut;
import static org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo;
import static org.springframework.hateoas.mvc.ControllerLinkBuilder.methodOn;

/**
 * Created by dkuchugurov on 02.05.2014.
 */
@Component
public class SectionTemplateResourceAssembler extends ResourceAssemblerSupport<SectionTemplate, SectionTemplateResource> {

    @Autowired
    private UserQueryService userQueryService;

    public SectionTemplateResourceAssembler() {
        super(TemplateController.class, SectionTemplateResource.class);
    }

    @Override
    protected SectionTemplateResource instantiateResource(SectionTemplate entity) {
        try {
            String createdBy = getDisplayName(entity.getCreatedByUserId());
            String modifiedBy = getDisplayName(entity.getModifiedByUserId());
            return new SectionTemplateResource(
                    entity.getSectionTemplateId().getId(),
                    entity.getName(),
                    entity.getCreatedDate(),
                    createdBy,
                    entity.getModifiedDate(),
                    modifiedBy,
                    entity.getType().name(),
                    entity.getOrderIndex());
        } catch (UserNotFoundException e) {
            throw new RuntimeException(e);
        }
    }

    private String getDisplayName(String userId) throws UserNotFoundException {
        if (userId == null) {
            return "n/a";
        }
        return userQueryService.findUser(new UserId(userId)).getPerson().getDisplayName();
    }


    @Override
    public SectionTemplateResource toResource(SectionTemplate entity) {
        SectionTemplateResource resource = instantiateResource(entity);
        try {
            String templateId = entity.getTemplate().getTemplateId().getId();
            String sectionTemplateId = entity.getSectionTemplateId().getId();
            resource.add(linkWithMethodGet(linkTo(methodOn(TemplateController.class).getSection(templateId, sectionTemplateId)).withSelfRel()));
            resource.add(linkWithMethodPut(linkTo(methodOn(TemplateController.class).updateSectionOrder(templateId, sectionTemplateId, null)).withRel(RelConstants.TEMPLATES_SECTION_UPDATE_ORDER)));
            resource.add(linkWithMethodPut(linkTo(methodOn(TemplateController.class).updateSectionName(templateId, sectionTemplateId, null)).withRel(RelConstants.TEMPLATES_SECTION_UPDATE_NAME)));
            resource.add(linkWithMethodDelete(linkTo(methodOn(TemplateController.class).removeSection(templateId, sectionTemplateId)).withRel(RestMethods.DELETE.getName())));
            return resource;
        } catch (SectionTemplateNotFoundException | TemplateNotFoundException e) {
            throw new RuntimeException(e);
        }
    }
}
