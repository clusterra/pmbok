/*
 * Copyright (c) 2014.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * 	http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.clusterra.pmbok.rest.document.resource;

import com.clusterra.pmbok.document.domain.model.document.DocumentRevision;
import org.springframework.hateoas.ResourceSupport;

import java.util.Date;

/**
 * Created by dkuchugurov on 25.06.2014.
 */
public class HistoryEntryResource extends ResourceSupport {

    private final String revision;

    private final Date createdDate;

    private final String createdBy;

    private final String comment;

    public HistoryEntryResource(DocumentRevision documentRevision, Date createdDate, String createdBy, String comment) {
        this.revision = documentRevision.getRevision();
        this.createdDate = createdDate;
        this.createdBy = createdBy;
        this.comment = comment;
    }

    public String getRevision() {
        return revision;
    }

    public Date getCreatedDate() {
        return createdDate;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public String getComment() {
        return comment;
    }
}
