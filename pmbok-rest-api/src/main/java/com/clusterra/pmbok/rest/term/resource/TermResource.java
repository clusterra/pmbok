/*
 * Copyright (c) 2014.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * 	http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.clusterra.pmbok.rest.term.resource;

import org.springframework.hateoas.ResourceSupport;

import java.util.Date;

/**
 * Created by Denis Kuchugurov on 29.05.2014.
 */
public class TermResource extends ResourceSupport {

    private final String termId;

    private final Date createdDate;

    private final String createdByUserId;

    private final Date modifiedDate;

    private final String modifiedByUserId;

    private final String name;

    private final String description;

    public TermResource(String termId, Date createdDate, String createdByUserId, Date modifiedDate, String modifiedByUserId, String name, String description) {
        this.termId = termId;
        this.createdDate = createdDate;
        this.createdByUserId = createdByUserId;
        this.modifiedDate = modifiedDate;
        this.modifiedByUserId = modifiedByUserId;
        this.name = name;
        this.description = description;
    }

    public String getTermId() {
        return termId;
    }

    public Date getCreatedDate() {
        return createdDate;
    }

    public String getCreatedByUserId() {
        return createdByUserId;
    }

    public Date getModifiedDate() {
        return modifiedDate;
    }

    public String getModifiedByUserId() {
        return modifiedByUserId;
    }

    public String getName() {
        return name;
    }

    public String getDescription() {
        return description;
    }
}
