/*
 * Copyright (c) 2014.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * 	http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.clusterra.pmbok.rest.document.resource;

import com.clusterra.iam.core.application.user.UserId;
import com.clusterra.iam.core.application.user.UserNotFoundException;
import com.clusterra.iam.core.application.user.UserQueryService;
import com.clusterra.pmbok.document.application.document.DocumentQueryService;
import com.clusterra.pmbok.document.domain.model.document.DocumentRevision;
import com.clusterra.pmbok.document.domain.model.history.HistoryEntry;
import com.clusterra.pmbok.rest.document.DocumentController;
import com.clusterra.pmbok.document.domain.service.document.DocumentNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.mvc.ResourceAssemblerSupport;
import org.springframework.stereotype.Component;

/**
 * Created by dkuchugurov on 25.06.2014.
 */
@Component
public class HistoryEntryResourceAssembler extends ResourceAssemblerSupport<HistoryEntry, HistoryEntryResource> {

    @Autowired
    private UserQueryService userQueryService;

    @Autowired
    private DocumentQueryService documentQueryService;

    public HistoryEntryResourceAssembler() {
        super(DocumentController.class, HistoryEntryResource.class);
    }

    @Override
    protected HistoryEntryResource instantiateResource(HistoryEntry entity) {
        try {
            String createdBy = userQueryService.findUser(new UserId(entity.getCreatedByUserId())).getPerson().getDisplayName();
            DocumentRevision documentRevision = documentQueryService.getRevision(entity.getDocument().getDocumentId());
            return new HistoryEntryResource(
                    documentRevision,
                    entity.getCreatedDate(),
                    createdBy,
                    entity.getComment());
        } catch (UserNotFoundException | DocumentNotFoundException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public HistoryEntryResource toResource(HistoryEntry entity) {
        return instantiateResource(entity);
    }
}
