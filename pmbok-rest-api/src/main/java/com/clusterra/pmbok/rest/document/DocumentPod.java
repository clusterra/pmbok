/*
 * Copyright (c) 2014.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * 	http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.clusterra.pmbok.rest.document;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

import javax.validation.constraints.NotNull;

/**
 * Created by dkuchugurov on 01.05.2014.
 */
public class DocumentPod {


    @NotNull
    private final String templateId;

    @NotNull
    private final String projectVersionId;

    @JsonCreator
    public DocumentPod(
            @JsonProperty("documentType") String templateId,
            @JsonProperty("projectVersionId") String projectVersionId) {
        this.templateId = templateId;
        this.projectVersionId = projectVersionId;
    }


    public String getProjectVersionId() {
        return projectVersionId;
    }

    public String getTemplateId() {
        return templateId;
    }

}
