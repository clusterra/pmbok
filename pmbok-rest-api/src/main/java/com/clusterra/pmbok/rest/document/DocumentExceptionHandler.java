/*
 * Copyright (c) 2014.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * 	http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.clusterra.pmbok.rest.document;

import com.clusterra.pmbok.document.domain.model.template.SectionTemplateNotFoundException;
import com.clusterra.pmbok.document.domain.service.document.DocumentNotFoundException;
import org.springframework.http.HttpStatus;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

import java.util.Arrays;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 *
 * @author Denis Kuchugurov
 *         Date: 20.11.13
 */
@ControllerAdvice
@SuppressWarnings("unused")
public class DocumentExceptionHandler {


    @ExceptionHandler(value = DocumentNotFoundException.class)
    @ResponseStatus(value = HttpStatus.NOT_FOUND, reason = "document not found")
    @ResponseBody
    public List<FieldError> handle(DocumentNotFoundException exception) {
        return Arrays.asList(new FieldError("document", "id", exception.getMessage()));
    }

    @ExceptionHandler(SectionTemplateNotFoundException.class)
    @ResponseStatus(HttpStatus.UNPROCESSABLE_ENTITY)
    @ResponseBody
    public List<FieldError> handle(SectionTemplateNotFoundException exception) {
        return Arrays.asList(new FieldError("sectionTemplate", "id", exception.getMessage()));
    }
}
