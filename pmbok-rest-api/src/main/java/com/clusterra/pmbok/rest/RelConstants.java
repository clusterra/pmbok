/*
 * Copyright (c) 2014.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * 	http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.clusterra.pmbok.rest;

/**
 * @author Denis Kuchugurov.
 *         29.07.2014.
 */
public interface RelConstants {


    String PROJECTS = "pmbok.projects";
    String PROJECTS_BY_ID = PROJECTS + ".by.id";
    String PROJECTS_SEARCH = PROJECTS + ".search";
    String PROJECTS_SEARCH_BY = PROJECTS + ".search.by";

    String PROJECT_VERSIONS = "pmbok.project.versions";
    String PROJECT_VERSION_CREATE = "pmbok.project.versions.create";
    String PROJECT_VERSION_DELETE = "pmbok.project.version.delete";
    String PROJECT_VERSION_AVAILABLE_DOCUMENT_TYPES = "pmbok.project.version.available-document-types";
    String PROJECT_VERSION_SEARCH_DOCUMENTS = "pmbok.documents.by-project-version";
    String PROJECT_SEARCH_DOCUMENTS = "pmbok.documents.search.by-project";


    String DOCUMENTS = "pmbok.documents";
    String DOCUMENTS_BY_ID = DOCUMENTS + ".by.id";
    String DOCUMENTS_SEARCH = DOCUMENTS + ".search";
    String DOCUMENTS_SEARCH_BY = DOCUMENTS + ".search.by";
    String DOCUMENTS_SEARCH_BY_PROJECT_VERSION = DOCUMENTS + ".search.by-project-version";
    String DOCUMENTS_SEARCH_BY_PROJECT = DOCUMENTS + ".search.by-project";

    String DOCUMENT_PROJECT = "pmbok.project";
    String DOCUMENT_SECTIONS = "pmbok.document.sections";
    String DOCUMENT_PUBLISH = "pmbok.document.publish";
    String DOCUMENT_APPROVE = "pmbok.document.approve";
    String DOCUMENT_DOWNLOAD = "pmbok.document.download";
    String DOCUMENT_VERSION = "pmbok.document.version";

    String DOCUMENT_SECTION_TEMPLATE_UPDATE_TEXT = "pmbok.section.update-text";

    String REFERENCES = "pmbok.references";
    String REFERENCES_SEARCH = REFERENCES + ".search";
    String REFERENCES_SEARCH_BY = REFERENCES + ".search.by";
    String REFERENCES_SEARCH_BY_SECTION = REFERENCES + ".search.by-section";
    //TODO same as above
    String REFERENCES_SEARCH_BY_DOCUMENT = "pmbok.section.reference.search";

    String REFERENCES_UPLOAD = REFERENCES + ".upload";
    String REFERENCES_LOAD = REFERENCES + ".load";
    //TODO rel naming
    String REFERENCES_UN_ASSOCIATE = "pmbok.section.un-associate-reference";
    String REFERENCES_ASSOCIATE = "pmbok.section.associate-reference";


    String TERMS = "pmbok.terms";
    String TERMS_SEARCH = TERMS + ".search";
    String TERMS_SEARCH_BY = TERMS + ".search.by";
    String TERMS_SEARCH_BY_SECTION = TERMS + ".search.by-section";
    //todo same as above
    String TERMS_SEARCH_BY_DOCUMENT = "pmbok.section.term.search";

    String TERMS_UPLOAD = TERMS + ".upload";
    String TERMS_LOAD = TERMS + ".load";
    //TODO rel naming
    String TERMS_UN_ASSOCIATE = "pmbok.section.un-associate-term";
    String TERMS_ASSOCIATE = "pmbok.section.associate-term";


    String TEMPLATES = "pmbok.templates";
    String TEMPLATES_BY_ID = TEMPLATES + ".by.id";
    String TEMPLATES_SEARCH_BY = TEMPLATES + ".search.by";
    String TEMPLATES_SEARCH = TEMPLATES + ".search";
    String TEMPLATES_SECTIONS = TEMPLATES + ".sections";
    String TEMPLATES_SECTIONS_TYPES = TEMPLATES + ".section-types";
    String TEMPLATES_SECTION_ADD_HISTORY = TEMPLATES + ".section.add-history";
    String TEMPLATES_SECTION_ADD_TERM = TEMPLATES + ".section.add-term";
    String TEMPLATES_SECTION_ADD_REFERENCE = TEMPLATES + ".section.add-reference";
    String TEMPLATES_SECTION_ADD_TEXT = TEMPLATES + ".section.add-text";
    String TEMPLATES_SECTION_ADD_TITLE = TEMPLATES + ".section.add-title";
    String TEMPLATES_SECTION_ADD_CONTENTS = TEMPLATES + ".section.add-contents";
    String TEMPLATES_SECTION_UPDATE_ORDER = TEMPLATES + ".section.set-order";
    String TEMPLATES_SECTION_UPDATE_NAME = TEMPLATES + ".section.set-name";
    String TEMPLATES_UPDATE_NAME = TEMPLATES + ".set-name";
}