/*
 * Copyright 2012 - 2013 www.equmo.org
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * 	http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.clusterra.pmbok.rest.project;

import com.clusterra.pmbok.project.domain.service.IncorrectProjectVersionException;
import com.clusterra.pmbok.project.domain.service.ProjectNotFoundException;
import com.clusterra.pmbok.project.domain.service.ProjectVersionNotFoundException;
import org.springframework.http.HttpStatus;
import org.springframework.validation.BindException;
import org.springframework.validation.FieldError;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

import java.util.Arrays;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 *
 * @author Denis Kuchugurov
 *         Date: 20.11.13
 */
@ControllerAdvice
@SuppressWarnings("unused")
public class ProjectExceptionHandler {


    @ExceptionHandler(BindException.class)
    @ResponseStatus(HttpStatus.UNPROCESSABLE_ENTITY)
    @ResponseBody
    public List<ObjectError> handle(BindException exception) {
        return exception.getAllErrors();
    }

    @ExceptionHandler(IncorrectProjectVersionException.class)
    @ResponseStatus(HttpStatus.UNPROCESSABLE_ENTITY)
    @ResponseBody
    public List<FieldError> handle(IncorrectProjectVersionException exception) {
        return Arrays.asList(new FieldError("projectVersion", "value", exception.getMessage()));
    }

    @ExceptionHandler(ProjectVersionNotFoundException.class)
    @ResponseStatus(HttpStatus.NOT_FOUND)
    @ResponseBody
    public List<FieldError> handle(ProjectVersionNotFoundException exception) {
        return Arrays.asList(new FieldError("projectVersion", "value", exception.getMessage()));
    }

    @ExceptionHandler(value = ProjectNotFoundException.class)
    @ResponseStatus(value = HttpStatus.NOT_FOUND, reason = "project not found")
    @ResponseBody
    public List<FieldError> handle(ProjectNotFoundException exception) {
        return Arrays.asList(new FieldError("project", "id", exception.getMessage()));
    }
}
