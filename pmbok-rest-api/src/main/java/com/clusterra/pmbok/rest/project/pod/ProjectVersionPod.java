/*
 * Copyright (c) 2014.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * 	http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.clusterra.pmbok.rest.project.pod;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by dkuchugurov on 10.05.2014.
 */
public class ProjectVersionPod {

    private final String label;

    private final Integer value;

    private final Boolean isMain;


    @JsonCreator
    public ProjectVersionPod(
            @JsonProperty("label") String label,
            @JsonProperty("value") Integer value,
            @JsonProperty("isMain") Boolean isMain) {
        this.label = label;
        this.value = value;
        this.isMain = isMain;
    }

    public String getLabel() {
        return label;
    }

    public Integer getValue() {
        return value;
    }

    public boolean getIsMain() {
        return isMain;
    }


}
