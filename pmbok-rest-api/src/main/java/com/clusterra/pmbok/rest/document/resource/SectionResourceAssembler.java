/*
 * Copyright (c) 2014.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * 	http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.clusterra.pmbok.rest.document.resource;

import com.clusterra.pmbok.document.domain.model.document.section.SectionContentVisitor;
import com.clusterra.pmbok.document.domain.model.document.section.history.HistorySectionContent;
import com.clusterra.pmbok.document.domain.model.document.section.reference.ReferenceSectionContent;
import com.clusterra.pmbok.document.domain.model.document.section.term.TermSectionContent;
import com.clusterra.pmbok.document.domain.model.document.section.text.TextSectionContent;
import com.clusterra.pmbok.document.domain.model.document.section.title.TitleSectionContent;
import com.clusterra.pmbok.rest.RelConstants;
import com.clusterra.pmbok.rest.document.DocumentController;
import com.clusterra.pmbok.rest.term.resource.TermResource;
import com.clusterra.pmbok.rest.term.resource.TermResourceAssembler;
import com.clusterra.pmbok.document.domain.model.document.section.SectionContent;
import com.clusterra.pmbok.document.domain.model.document.section.toc.TocSectionContent;
import com.clusterra.pmbok.rest.reference.ReferenceController;
import com.clusterra.pmbok.rest.reference.resource.ReferenceResource;
import com.clusterra.pmbok.rest.reference.resource.ReferenceResourceAssembler;
import com.clusterra.pmbok.rest.term.TermController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.mvc.ResourceAssemblerSupport;
import org.springframework.stereotype.Component;

import java.util.List;

import static com.clusterra.rest.util.LinkWithMethodBuilder.linkWithMethodPost;
import static org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo;
import static org.springframework.hateoas.mvc.ControllerLinkBuilder.methodOn;

/**
 * Created by dkuchugurov on 02.05.2014.
 */
@Component
public class SectionResourceAssembler extends ResourceAssemblerSupport<SectionContent, SectionResource> implements SectionContentVisitor<SectionResource> {

    @Autowired
    private HistoryEntryResourceAssembler historyEntryResourceAssembler;

    @Autowired
    private TitleSectionResourceAssembler titleSectionResourceAssembler;

    public SectionResourceAssembler() {
        super(DocumentController.class, SectionResource.class);
    }

    @Override
    protected SectionResource instantiateResource(SectionContent entity) {
        String title = entity.getSectionTemplate().getTitle();
        return new SectionResource(entity.getSectionTemplate().getSectionTemplateId().getId(), entity.getSectionTemplate().getType(), title);
    }

    @Override
    public SectionResource toResource(SectionContent sectionContent) {
        try {
            SectionResource resource = sectionContent.accept(this);
            resource.add(linkTo(methodOn(DocumentController.class).get(sectionContent.getDocumentId().getId())).withRel("pmbok.doc"));
            return resource;
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public SectionResource visit(TitleSectionContent content) throws Exception {
        SectionResource resource = instantiateResource(content);
        resource.setContent(titleSectionResourceAssembler.toResource(content));
        return resource;
    }

    @Override
    public SectionResource visit(HistorySectionContent content) throws Exception {
        SectionResource resource = instantiateResource(content);
        resource.setContent(historyEntryResourceAssembler.toResources(content.getHistoryEntries()));
        return resource;
    }

    @Override
    public SectionResource visit(ReferenceSectionContent content) throws Exception {
        SectionResource resource = instantiateResource(content);
        List<ReferenceResource> refContent = new ReferenceResourceAssembler(content.getDocumentId(), true, false).toResources(content.getReferences());
        resource.add(linkWithMethodPost(linkTo(methodOn(DocumentController.class).associateReference(content.getDocumentId().getId(), "REFERENCE_ID")).withRel(RelConstants.REFERENCES_ASSOCIATE)));
        resource.add(linkTo(methodOn(ReferenceController.class).search(null, null, content.getDocumentId().getId(), null)).withRel(RelConstants.REFERENCES_SEARCH_BY_DOCUMENT));
        resource.setContent(refContent);
        return resource;
    }

    @Override
    public SectionResource visit(TermSectionContent content) throws Exception {
        SectionResource resource = instantiateResource(content);
        List<TermResource> termContent = new TermResourceAssembler(content.getDocumentId(), true, false).toResources(content.getTerms());
        resource.add(linkWithMethodPost(linkTo(methodOn(DocumentController.class).associateTerm(content.getDocumentId().getId(), "TERM_ID")).withRel(RelConstants.TERMS_ASSOCIATE)));
        resource.add(linkTo(methodOn(TermController.class).search(null, null, content.getDocumentId().getId(), null)).withRel(RelConstants.TERMS_SEARCH_BY_DOCUMENT));
        resource.setContent(termContent);
        return resource;
    }

    @Override
    public SectionResource visit(TextSectionContent content) throws Exception {
        SectionResource resource = instantiateResource(content);
        resource.setContent(content.getText());
        resource.add(linkWithMethodPost(linkTo(methodOn(DocumentController.class).updateText(content.getDocumentId().getId(), content.getSectionTemplate().getSectionTemplateId().getId(), null)).withRel(RelConstants.DOCUMENT_SECTION_TEMPLATE_UPDATE_TEXT)));
        return resource;
    }

    @Override
    public SectionResource visit(TocSectionContent content) throws Exception {
        SectionResource resource = instantiateResource(content);
        resource.setContent(content.getTocElements());
        return resource;
    }
}
