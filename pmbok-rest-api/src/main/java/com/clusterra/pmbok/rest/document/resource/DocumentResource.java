/*
 * Copyright (c) 2014.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * 	http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.clusterra.pmbok.rest.document.resource;

import com.clusterra.pmbok.project.domain.model.ProjectVersionId;
import com.clusterra.pmbok.document.domain.model.document.DocumentId;
import com.clusterra.pmbok.document.domain.model.document.DocumentRevision;
import com.clusterra.pmbok.project.domain.model.ProjectId;
import org.springframework.hateoas.ResourceSupport;

import java.util.Date;

/**
 * Created by dkuchugurov on 06.05.2014.
 */
public class DocumentResource extends ResourceSupport {

    private final String documentId;

    private final String projectVersionId;

    private final String projectId;

    private final String status;

    private final Date createdDate;

    private final String createdBy;

    private final Date modifiedDate;

    private final String modifiedBy;

    private final String documentType;

    private final String revision;

    public DocumentResource(DocumentId documentId, ProjectVersionId projectVersionId, ProjectId projectId, String status, Date createdDate, String createdBy, Date modifiedDate, String modifiedBy, String documentType, DocumentRevision revision) {
        this.documentId = documentId.getId();
        this.projectVersionId = projectVersionId.getId();
        this.projectId = projectId.getId();
        this.status = status;
        this.createdDate = createdDate;
        this.createdBy = createdBy;
        this.modifiedDate = modifiedDate;
        this.modifiedBy = modifiedBy;
        this.documentType = documentType;
        this.revision = revision.getRevision();
    }

    public String getStatus() {
        return status;
    }

    public Date getCreatedDate() {
        return createdDate;
    }

    public String getProjectVersionId() {
        return projectVersionId;
    }

    public String getProjectId() {
        return projectId;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public Date getModifiedDate() {
        return modifiedDate;
    }

    public String getDocumentId() {
        return documentId;
    }

    public String getModifiedBy() {
        return modifiedBy;
    }

    public String getDocumentType() {
        return documentType;
    }

    public String getRevision() {
        return revision;
    }
}
