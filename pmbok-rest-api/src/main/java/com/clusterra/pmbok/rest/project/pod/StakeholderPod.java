/*
 * Copyright (c) 2014.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * 	http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.clusterra.pmbok.rest.project.pod;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.clusterra.pmbok.project.domain.model.stakeholder.StakeholderRole;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 * Created by kepkap on 17/11/14.
 */
public class StakeholderPod {


    @Size(min = 4, max = 20)
    private final String name;

    @NotNull
    private final StakeholderRole role;


    @JsonCreator
    public StakeholderPod(
            @JsonProperty("name") String name,
            @JsonProperty("role") StakeholderRole role) {
        this.name = name;
        this.role = role;
    }

    public String getName() {
        return name;
    }

    public StakeholderRole getRole() {
        return role;
    }
}
