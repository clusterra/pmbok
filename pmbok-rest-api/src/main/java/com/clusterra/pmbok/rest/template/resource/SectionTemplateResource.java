/*
 * Copyright (c) 2014.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * 	http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.clusterra.pmbok.rest.template.resource;

import org.springframework.hateoas.ResourceSupport;

import java.util.Date;

/**
 * @author Denis Kuchugurov.
 *         24.07.2014.
 */
public class SectionTemplateResource extends ResourceSupport {

    private final String sectionTemplateId;

    private final String name;

    private final String type;

    private final Date createdDate;

    private final String createdBy;

    private final Date modifiedDate;

    private final String modifiedBy;

    private final Integer orderIndex;

    public SectionTemplateResource(String sectionTemplateId, String name, Date createdDate, String createdBy, Date modifiedDate, String modifiedBy, String type, Integer orderIndex) {
        this.sectionTemplateId = sectionTemplateId;
        this.name = name;
        this.createdDate = createdDate;
        this.createdBy = createdBy;
        this.modifiedDate = modifiedDate;
        this.modifiedBy = modifiedBy;
        this.type = type;
        this.orderIndex = orderIndex;
    }


    public String getSectionTemplateId() {
        return sectionTemplateId;
    }

    public String getName() {
        return name;
    }

    public Date getCreatedDate() {
        return createdDate;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public Date getModifiedDate() {
        return modifiedDate;
    }

    public String getModifiedBy() {
        return modifiedBy;
    }

    public String getType() {
        return type;
    }

    public Integer getOrderIndex() {
        return orderIndex;
    }
}
