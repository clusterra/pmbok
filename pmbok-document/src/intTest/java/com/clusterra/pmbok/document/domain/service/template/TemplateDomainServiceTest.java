/*
 * Copyright (c) 2014.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * 	http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.clusterra.pmbok.document.domain.service.template;

import com.clusterra.iam.core.application.tenant.TenantCommandService;
import com.clusterra.iam.core.application.tenant.TenantId;
import com.clusterra.iam.core.domain.model.tenant.Tenant;
import com.clusterra.iam.core.testutil.UserRandomUtil;
import com.clusterra.pmbok.document.domain.model.template.SectionTemplateAlreadyExistsException;
import com.clusterra.pmbok.document.domain.model.template.Template;
import com.clusterra.pmbok.document.domain.model.template.TemplateId;
import com.clusterra.pmbok.document.domain.model.template.TemplateStatus;
import com.clusterra.pmbok.document.domain.model.template.section.SectionTemplate;
import com.clusterra.pmbok.document.domain.model.template.section.SectionType;
import org.apache.commons.lang3.RandomStringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.ConfigFileApplicationContextInitializer;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.testng.AbstractTransactionalTestNGSpringContextTests;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.util.Comparator;
import java.util.List;

import static org.apache.commons.lang3.RandomStringUtils.randomAlphabetic;
import static org.fest.assertions.api.Assertions.assertThat;
import static org.fest.assertions.api.Assertions.fail;


@ContextConfiguration(
        value = {"classpath*:META-INF/spring/*.xml"},
        initializers = ConfigFileApplicationContextInitializer.class)
public class TemplateDomainServiceTest extends AbstractTransactionalTestNGSpringContextTests {

    @Autowired
    private TemplateDomainService templateDomainService;

    private Template template;

    @Autowired
    private TenantCommandService tenantCommandService;

    private TenantId tenantId;


    @BeforeMethod
    public void before() throws Exception {
        Tenant tenant = tenantCommandService.create(randomAlphabetic(10), UserRandomUtil.getRandomEmail());
        tenantId = new TenantId(tenant.getId());
        template = templateDomainService.createTemplate(tenantId, 0, 1, randomAlphabetic(10));
    }

    @Test
    public void test_find_all() throws Exception {
        Template template2 = templateDomainService.createTemplate(tenantId, 0, 1, randomAlphabetic(10));
        Template template3 = templateDomainService.createTemplate(tenantId, 0, 1, randomAlphabetic(10));
        List<Template> templates = templateDomainService.findAllTemplates(tenantId);
        assertThat(templates).contains(template, template2, template3);
    }

    @Test
    public void test_find_all_pageable() throws Exception {
        Template template2 = templateDomainService.createTemplate(tenantId, 0, 1, randomAlphabetic(10));
        Template template3 = templateDomainService.createTemplate(tenantId, 0, 1, randomAlphabetic(10));
        Template template4 = templateDomainService.createTemplate(tenantId, 0, 1, randomAlphabetic(10));
        Template template5 = templateDomainService.createTemplate(tenantId, 0, 1, randomAlphabetic(10));

        List<Template> list = templateDomainService.findBy(tenantId, new PageRequest(0, 10), "").getContent();
        assertThat(list).hasSize(5);
        assertThat(list).contains(template, template2, template3, template4, template5);
    }

    @Test
    public void test_find_all_search_by() throws Exception {
        String searchBy = randomAlphabetic(10);
        Template template2 = templateDomainService.createTemplate(tenantId, 0, 1, searchBy + randomAlphabetic(5));
        Template template3 = templateDomainService.createTemplate(tenantId, 0, 1, randomAlphabetic(10));
        Template template4 = templateDomainService.createTemplate(tenantId, 0, 1, randomAlphabetic(5) + searchBy);
        String searchBy2 = randomAlphabetic(10);
        Template template5 = templateDomainService.createTemplate(tenantId, 0, 1, searchBy2);

        List<Template> list = templateDomainService.findBy(tenantId, new PageRequest(0, 10), searchBy).getContent();
        assertThat(list).hasSize(2);
        assertThat(list).containsOnly(template2, template4);

        list = templateDomainService.findBy(tenantId, new PageRequest(0, 10), searchBy2).getContent();
        assertThat(list).hasSize(1);
        assertThat(list).containsExactly(template5);

        list = templateDomainService.findBy(tenantId, new PageRequest(0, 10), randomAlphabetic(10)).getContent();
        assertThat(list).isEmpty();
    }

    @Test
    public void test_find_all_order_by() throws Exception {
        Template template1 = templateDomainService.createTemplate(tenantId, 0, 1, "123abc_" + randomAlphabetic(10));
        Template template2 = templateDomainService.createTemplate(tenantId, 0, 1, "123def_" + randomAlphabetic(10));
        Template template3 = templateDomainService.createTemplate(tenantId, 0, 1, "123ghi_" + randomAlphabetic(10));

        Pageable pageable = new PageRequest(0, 10, new Sort("name", "version", "status", "createdByUserId"));

        List<Template> list = templateDomainService.findBy(tenantId, pageable, null).getContent();
        assertThat(template1).isEqualTo(list.get(0));
        assertThat(template2).isEqualTo(list.get(1));
//        assertThat(list).isSortedAccordingTo((o1, o2) -> o1.getName().compareTo(o2.getName()));

    }

    @Test(expectedExceptions = TemplateAlreadyExistsException.class)
    public void test_create_throws_exception() throws Exception {
        String name = randomAlphabetic(10);
        templateDomainService.createTemplate(tenantId, 0, 1, name);
        templateDomainService.createTemplate(tenantId, 0, 1, name);
    }

    @Test
    public void test_create_validation() throws Exception {
        try {
            templateDomainService.createTemplate(null, 0, 0, randomAlphabetic(10));
            fail("should fail");
        } catch (NullPointerException e) {
            assertThat(e).hasMessageContaining("null");
        }
        try {
            templateDomainService.createTemplate(tenantId, 0, 0, null);
            fail("should fail");
        } catch (NullPointerException e) {
            assertThat(e).hasMessageContaining("null");
        }
        try {
            templateDomainService.createTemplate(tenantId, -1, 0, randomAlphabetic(10));
            fail("should fail");
        } catch (IllegalArgumentException e) {
            assertThat(e).hasMessageContaining("100");
        }
        try {
            templateDomainService.createTemplate(tenantId, 0, -1, randomAlphabetic(10));
            fail("should fail");
        } catch (IllegalArgumentException e) {
            assertThat(e).hasMessageContaining("100");
        }
    }

    @Test
    public void test_delete() throws Exception {
        templateDomainService.addTextSection(template.getTemplateId(), "History");
        templateDomainService.deleteTemplate(template.getTemplateId());
    }

    @Test(expectedExceptions = TemplateNotFoundException.class)
    public void test_delete_throws_exception_1() throws Exception {
        templateDomainService.deleteTemplate(new TemplateId(randomAlphabetic(10)));
    }

    @Test
    public void test_add_section() throws Exception {
        templateDomainService.markReady(template.getTemplateId());
        assertThat(templateDomainService.findBy(template.getTemplateId()).getStatus()).isEqualTo(TemplateStatus.READY);
        templateDomainService.addTextSection(template.getTemplateId(), randomAlphabetic(10));
        List<SectionTemplate> sectionTemplates = templateDomainService.findSectionTemplates(template.getTemplateId());
        assertThat(templateDomainService.findBy(template.getTemplateId()).getStatus()).isEqualTo(TemplateStatus.EDITING);
        assertThat(sectionTemplates).hasSize(1);
        assertThat(sectionTemplates.get(0).getType()).isEqualTo(SectionType.SECTION_TEXT);
    }

    @Test
    public void test_remove_section() throws Exception {

        templateDomainService.addTextSection(template.getTemplateId(), randomAlphabetic(10));
        templateDomainService.addTextSection(template.getTemplateId(), randomAlphabetic(10));
        List<SectionTemplate> sectionTemplates = templateDomainService.findSectionTemplates(template.getTemplateId());
        assertThat(sectionTemplates).hasSize(2);
        templateDomainService.removeSection(template.getTemplateId(), sectionTemplates.get(0).getSectionTemplateId());
        sectionTemplates = templateDomainService.findSectionTemplates(template.getTemplateId());
        assertThat(sectionTemplates).hasSize(1);
    }

    @Test
    public void test_add_section_is_ordered() throws Exception {

        SectionTemplate template1 = templateDomainService.addTextSection(template.getTemplateId(), randomAlphabetic(10));
        SectionTemplate template2 = templateDomainService.addTextSection(template.getTemplateId(), randomAlphabetic(10));
        SectionTemplate template3 = templateDomainService.addTextSection(template.getTemplateId(), randomAlphabetic(10));
        SectionTemplate template4 = templateDomainService.addTextSection(template.getTemplateId(), randomAlphabetic(10));
        SectionTemplate template5 = templateDomainService.addTextSection(template.getTemplateId(), randomAlphabetic(10));

        List<SectionTemplate> sectionTemplates = templateDomainService.findSectionTemplates(template.getTemplateId());
        assertThat(sectionTemplates).isSortedAccordingTo(new Comparator<SectionTemplate>() {
            @Override
            public int compare(SectionTemplate o1, SectionTemplate o2) {
                return o1.getOrderIndex().compareTo(o2.getOrderIndex());
            }
        });

        assertThat(template1.getOrderIndex()).isEqualTo(0);
        assertThat(template2.getOrderIndex()).isEqualTo(1);
        assertThat(template3.getOrderIndex()).isEqualTo(2);
        assertThat(template4.getOrderIndex()).isEqualTo(3);
        assertThat(template5.getOrderIndex()).isEqualTo(4);
    }

    @Test
    public void test_set_order_2_to_1() throws Exception {

        SectionTemplate sectionTemplate_0 = templateDomainService.addTextSection(template.getTemplateId(), "0_" + randomAlphabetic(10));
        SectionTemplate sectionTemplate_1 = templateDomainService.addTextSection(template.getTemplateId(), "1_" + randomAlphabetic(10));
        SectionTemplate sectionTemplate_2 = templateDomainService.addTextSection(template.getTemplateId(), "2_" + randomAlphabetic(10));
        SectionTemplate sectionTemplate_3 = templateDomainService.addTextSection(template.getTemplateId(), "3_" + randomAlphabetic(10));
        SectionTemplate sectionTemplate_4 = templateDomainService.addTextSection(template.getTemplateId(), "4_" + randomAlphabetic(10));


        sectionTemplate_1 = templateDomainService.updateSectionOrder(template.getTemplateId(), sectionTemplate_1.getSectionTemplateId(), 0);

        List<SectionTemplate> sections = templateDomainService.findSectionTemplates(template.getTemplateId());

        assertThat(sections.get(0).getName()).isEqualTo(sectionTemplate_1.getName());
        assertThat(sections.get(1).getName()).isEqualTo(sectionTemplate_0.getName());
        assertThat(sections.get(2).getName()).isEqualTo(sectionTemplate_2.getName());
        assertThat(sections.get(3).getName()).isEqualTo(sectionTemplate_3.getName());
        assertThat(sections.get(4).getName()).isEqualTo(sectionTemplate_4.getName());
    }

    @Test
    public void test_set_order_4_to_5() throws Exception {

        SectionTemplate sectionTemplate_1 = templateDomainService.addTextSection(template.getTemplateId(), "1_" + randomAlphabetic(10));
        SectionTemplate sectionTemplate_2 = templateDomainService.addTextSection(template.getTemplateId(), "2_" + randomAlphabetic(10));
        SectionTemplate sectionTemplate_3 = templateDomainService.addTextSection(template.getTemplateId(), "3_" + randomAlphabetic(10));
        SectionTemplate sectionTemplate_4 = templateDomainService.addTextSection(template.getTemplateId(), "4_" + randomAlphabetic(10));
        SectionTemplate sectionTemplate_5 = templateDomainService.addTextSection(template.getTemplateId(), "5_" + randomAlphabetic(10));


        sectionTemplate_4 = templateDomainService.updateSectionOrder(template.getTemplateId(), sectionTemplate_4.getSectionTemplateId(), 5);

        List<SectionTemplate> sections = templateDomainService.findSectionTemplates(template.getTemplateId());

        assertThat(sections.get(0).getName()).isEqualTo(sectionTemplate_1.getName());
        assertThat(sections.get(1).getName()).isEqualTo(sectionTemplate_2.getName());
        assertThat(sections.get(2).getName()).isEqualTo(sectionTemplate_3.getName());
        assertThat(sections.get(3).getName()).isEqualTo(sectionTemplate_5.getName());
        assertThat(sections.get(4).getName()).isEqualTo(sectionTemplate_4.getName());
    }

    @Test
    public void test_set_order_5_to_1() throws Exception {

        SectionTemplate sectionTemplate_0 = templateDomainService.addTextSection(template.getTemplateId(), "0_" + randomAlphabetic(10));
        SectionTemplate sectionTemplate_1 = templateDomainService.addTextSection(template.getTemplateId(), "1_" + randomAlphabetic(10));
        SectionTemplate sectionTemplate_2 = templateDomainService.addTextSection(template.getTemplateId(), "2_" + randomAlphabetic(10));
        SectionTemplate sectionTemplate_3 = templateDomainService.addTextSection(template.getTemplateId(), "3_" + randomAlphabetic(10));
        SectionTemplate sectionTemplate_4 = templateDomainService.addTextSection(template.getTemplateId(), "4_" + randomAlphabetic(10));

        sectionTemplate_4 = templateDomainService.updateSectionOrder(template.getTemplateId(), sectionTemplate_4.getSectionTemplateId(), 0);

        List<SectionTemplate> sections = templateDomainService.findSectionTemplates(template.getTemplateId());

        assertThat(sections.get(0)).isEqualTo(sectionTemplate_4);
        assertThat(sections.get(1)).isEqualTo(sectionTemplate_0);
        assertThat(sections.get(2)).isEqualTo(sectionTemplate_1);
        assertThat(sections.get(3)).isEqualTo(sectionTemplate_2);
        assertThat(sections.get(4)).isEqualTo(sectionTemplate_3);
    }

    @Test
    public void test_set_order_3_same_place() throws Exception {

        SectionTemplate sectionTemplate_0 = templateDomainService.addTextSection(template.getTemplateId(), "0_" + randomAlphabetic(10));
        SectionTemplate sectionTemplate_1 = templateDomainService.addTextSection(template.getTemplateId(), "1_" + randomAlphabetic(10));
        SectionTemplate sectionTemplate_2 = templateDomainService.addTextSection(template.getTemplateId(), "2_" + randomAlphabetic(10));
        SectionTemplate sectionTemplate_3 = templateDomainService.addTextSection(template.getTemplateId(), "3_" + randomAlphabetic(10));
        SectionTemplate sectionTemplate_4 = templateDomainService.addTextSection(template.getTemplateId(), "4_" + randomAlphabetic(10));


        sectionTemplate_2 = templateDomainService.updateSectionOrder(template.getTemplateId(), sectionTemplate_2.getSectionTemplateId(), 2);

        List<SectionTemplate> sections = templateDomainService.findSectionTemplates(template.getTemplateId());

        assertThat(sections.get(0)).isEqualTo(sectionTemplate_0);
        assertThat(sections.get(1)).isEqualTo(sectionTemplate_1);
        assertThat(sections.get(2)).isEqualTo(sectionTemplate_2);
        assertThat(sections.get(3)).isEqualTo(sectionTemplate_3);
        assertThat(sections.get(4)).isEqualTo(sectionTemplate_4);
    }

    @Test(expectedExceptions = SectionTemplateAlreadyExistsException.class)
    public void test_add_history_section_throws_exception() throws Exception {
        templateDomainService.addHistorySection(template.getTemplateId(), randomAlphabetic(5));
        templateDomainService.addHistorySection(template.getTemplateId(), randomAlphabetic(5));
    }

    @Test(expectedExceptions = SectionTemplateAlreadyExistsException.class)
    public void test_add_term_section_throws_exception() throws Exception {
        templateDomainService.addTermSection(template.getTemplateId(), randomAlphabetic(5));
        templateDomainService.addTermSection(template.getTemplateId(), randomAlphabetic(5));
    }

    @Test(expectedExceptions = SectionTemplateAlreadyExistsException.class)
    public void test_add_reference_section_throws_exception() throws Exception {
        templateDomainService.addReferenceSection(template.getTemplateId(), randomAlphabetic(5));
        templateDomainService.addReferenceSection(template.getTemplateId(), randomAlphabetic(5));
    }

    @Test
    public void test_update_template_name() throws Exception {
        String newName = RandomStringUtils.randomAlphabetic(10);
        templateDomainService.updateTemplateName(template.getTemplateId(), newName);
        assertThat(templateDomainService.findBy(template.getTemplateId()).getName()).isEqualTo(newName);
    }

    @Test
    public void test_contents_section() throws Exception {

        SectionTemplate sectionTemplate_0 = templateDomainService.addTocSection(template.getTemplateId(), "0_" + randomAlphabetic(10));
        SectionTemplate sectionTemplate_1 = templateDomainService.addTextSection(template.getTemplateId(), "1_" + randomAlphabetic(10));

        List<SectionTemplate> sections = templateDomainService.findSectionTemplates(template.getTemplateId());

        assertThat(sections.get(0)).isEqualTo(sectionTemplate_0);
        assertThat(sections.get(1)).isEqualTo(sectionTemplate_1);
    }

}