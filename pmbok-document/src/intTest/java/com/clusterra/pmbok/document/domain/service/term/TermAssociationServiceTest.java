/*
 * Copyright (c) 2014.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * 	http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.clusterra.pmbok.document.domain.service.term;

import com.clusterra.iam.core.application.group.GroupDescriptor;
import com.clusterra.iam.core.application.group.GroupService;
import com.clusterra.iam.core.application.membership.AuthorizedMembershipService;
import com.clusterra.iam.core.application.role.RoleDescriptor;
import com.clusterra.iam.core.application.role.RoleService;
import com.clusterra.iam.core.application.tenant.TenantCommandService;
import com.clusterra.iam.core.application.tenant.TenantId;
import com.clusterra.iam.core.application.tracker.IdentityTrackerLifeCycle;
import com.clusterra.iam.core.application.user.DefaultRole;
import com.clusterra.iam.core.application.user.UserCommandService;
import com.clusterra.iam.core.application.user.UserId;
import com.clusterra.iam.core.domain.model.tenant.Tenant;
import com.clusterra.iam.core.testutil.UserRandomUtil;
import com.clusterra.pmbok.document.domain.model.document.Document;
import com.clusterra.pmbok.document.domain.model.document.DocumentId;
import com.clusterra.pmbok.document.domain.model.template.TemplateId;
import com.clusterra.pmbok.document.domain.service.document.DocumentDomainService;
import com.clusterra.pmbok.document.domain.service.template.TemplateDomainService;
import com.clusterra.pmbok.project.domain.model.ProjectId;
import com.clusterra.pmbok.project.domain.model.ProjectVersion;
import com.clusterra.pmbok.project.domain.service.ProjectDomainService;
import com.clusterra.pmbok.term.domain.model.term.Term;
import com.clusterra.pmbok.term.domain.service.TermDomainService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.ConfigFileApplicationContextInitializer;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.testng.AbstractTransactionalTestNGSpringContextTests;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import static org.apache.commons.lang3.RandomStringUtils.randomAlphabetic;
import static org.fest.assertions.api.Assertions.assertThat;

/**
 * Created with IntelliJ IDEA.
 *
 * @author Denis Kuchugurov
 *         Date: 05.02.14
 */
@ContextConfiguration(
        value = {"classpath*:META-INF/spring/*.xml"},
        initializers = ConfigFileApplicationContextInitializer.class)
public class TermAssociationServiceTest extends AbstractTransactionalTestNGSpringContextTests {

    @Autowired
    private TermDomainService termDomainService;

    @Autowired
    private TermAssociationService termAssociationService;

    @Autowired
    private DocumentDomainService documentDomainService;

    @Autowired
    private ProjectDomainService projectDomainService;

    @Autowired
    private TemplateDomainService templateDomainService;

    private Term term1;
    private Term term2;
    private DocumentId documentId;

    @Autowired
    private TenantCommandService tenantCommandService;

    private TenantId tenantId;

    @Autowired
    private UserCommandService userCommandService;

    @Autowired
    private RoleService roleService;

    @Autowired
    private GroupService groupService;

    @Autowired
    private AuthorizedMembershipService authorizedMembershipService;

    @Autowired
    private IdentityTrackerLifeCycle identityTrackerLifeCycle;


    @BeforeMethod
    public void beforeMethod() throws Exception {
        Tenant tenant = tenantCommandService.create(randomAlphabetic(10), UserRandomUtil.getRandomEmail());
        tenantId = new TenantId(tenant.getId());
        startTrackingUser();

        term1 = termDomainService.create(tenantId, randomAlphabetic(5), randomAlphabetic(5));
        term2 = termDomainService.create(tenantId, randomAlphabetic(5), randomAlphabetic(5));
        ProjectId projectId = projectDomainService.createProject(tenantId, randomAlphabetic(10)).getProjectId();
        ProjectVersion projectVersion = projectDomainService.createVersion(tenantId, projectId, randomAlphabetic(5), 2, false);
        TemplateId templateId = templateDomainService.createTemplate(tenantId, 1, 0, randomAlphabetic(10)).getTemplateId();
        Document document = documentDomainService.create(tenantId, projectVersion.getProjectVersionId(), templateId);
        documentId = document.getDocumentId();
    }

    private void startTrackingUser() throws Exception {
        UserId userId = new UserId(userCommandService.create(tenantId, randomAlphabetic(5), UserRandomUtil.getRandomEmail(), UserRandomUtil.randomPassword(), randomAlphabetic(5), randomAlphabetic(5)).getId());

        RoleDescriptor role = roleService.createRole(tenantId, DefaultRole.ADMIN);
        GroupDescriptor group = groupService.createGroup(tenantId, randomAlphabetic(5));

        authorizedMembershipService.createAuthorizedMembershipIfNotExists(tenantId, userId, role, group);

        identityTrackerLifeCycle.startTracking(userId, tenantId);
    }

    @Test
    public void test_find_all() throws Exception {
        Page<Term> list = termDomainService.findBy(tenantId, new PageRequest(0, 100), "");
        assertThat(list).isNotEmpty();
        Term one = termDomainService.findBy(term1.getTermId());
        assertThat(list).contains(one);
    }

    @Test
    public void test_find_by_document_and_search_by() throws Exception {
        Term term_1 = termDomainService.create(tenantId, randomAlphabetic(5), randomAlphabetic(5));
        String name = randomAlphabetic(10);
        String searchBy = name.substring(2, 7);
        Term term_2 = termDomainService.create(tenantId, name, randomAlphabetic(5));
        Term term_3 = termDomainService.create(tenantId, randomAlphabetic(5), randomAlphabetic(5));
        Term term_4 = termDomainService.create(tenantId, randomAlphabetic(5), randomAlphabetic(5));

        Page<Term> page = termDomainService.findBy(tenantId, new PageRequest(0, 100), "");
        assertThat(page.getContent()).hasSize(6);

        termAssociationService.createAssociation(tenantId, documentId, term_1.getTermId());
        termAssociationService.createAssociation(tenantId, documentId, term_2.getTermId());
        termAssociationService.createAssociation(tenantId, documentId, term_3.getTermId());
        termAssociationService.createAssociation(tenantId, documentId, term_4.getTermId());

        page = termAssociationService.findBy(tenantId, new PageRequest(0, 100), documentId, "");
        assertThat(page.getContent()).contains(term_1, term_2, term_3, term_4);
        page = termAssociationService.findBy(tenantId, new PageRequest(0, 100), documentId, searchBy);
        assertThat(page.getContent()).containsOnly(term_2);
    }


    @Test
    public void test_delete_doc_with_terms() throws Exception {
        termAssociationService.createAssociation(tenantId, documentId, term1.getTermId());
        documentDomainService.deleteBy(documentId);
    }

    @Test
    public void test_association_pageable() throws Exception {
        termAssociationService.createAssociation(tenantId, documentId, term1.getTermId());
        termAssociationService.createAssociation(tenantId, documentId, term2.getTermId());
        Page<Term> termPage = termAssociationService.findBy(tenantId, new PageRequest(0, 1), documentId, null);
        assertThat(termPage.getContent()).hasSize(1);
    }

    @Test
    public void test_associate_is_idempotent() throws Exception {
        termAssociationService.createAssociation(tenantId, documentId, term1.getTermId());
        termAssociationService.createAssociation(tenantId, documentId, term1.getTermId());
        termAssociationService.createAssociation(tenantId, documentId, term1.getTermId());

        Page<Term> termPage = termAssociationService.findBy(tenantId, new PageRequest(0, 100), documentId, null);
        assertThat(termPage.getContent()).hasSize(1);
    }

    @Test
    public void test_unAssociate_is_idempotent() throws Exception {
        termAssociationService.createAssociation(tenantId, documentId, term1.getTermId());
        Page<Term> terms = termAssociationService.findBy(tenantId, new PageRequest(0, 100), documentId, null);
        assertThat(terms.getContent()).hasSize(1);

        termAssociationService.deleteAssociation(tenantId, documentId, term1.getTermId());
        termAssociationService.deleteAssociation(tenantId, documentId, term1.getTermId());
        Page<Term> termPage = termAssociationService.findBy(tenantId, new PageRequest(0, 100), documentId, null);

        assertThat(termPage.getContent()).isEmpty();
    }
}
