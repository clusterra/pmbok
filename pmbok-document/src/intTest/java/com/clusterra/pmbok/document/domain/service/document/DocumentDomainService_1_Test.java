/*
 * Copyright (c) 2014.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * 	http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.clusterra.pmbok.document.domain.service.document;

import com.clusterra.pmbok.document.domain.AbstractDocumentTransactionalTest;
import com.clusterra.pmbok.document.domain.model.document.Document;
import com.clusterra.pmbok.document.domain.model.document.Status;
import com.clusterra.pmbok.project.domain.model.ProjectVersion;
import com.clusterra.pmbok.project.domain.service.ProjectVersionNotFoundException;
import org.fest.assertions.core.Condition;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.testng.annotations.Test;

import java.util.Comparator;
import java.util.List;

import static org.apache.commons.lang3.RandomStringUtils.randomAlphabetic;
import static org.fest.assertions.api.Assertions.assertThat;

/**
 * Created with IntelliJ IDEA.
 *
 * @author Denis Kuchugurov
 *         Date: 07.12.13
 */
public class DocumentDomainService_1_Test extends AbstractDocumentTransactionalTest {

    @Test
    public void test_create_doc() throws Exception {
        List<Document> list = documentDomainService.findBy(tenantId, new PageRequest(0, 100), projectId, "").getContent();
        assertThat(list).hasSize(1);
    }

    @Test(expectedExceptions = DocumentNotFoundException.class)
    public void test_delete_doc() throws Exception {
        Document document = documentDomainService.findBy(documentId);
        assertThat(document).isNotNull();

        documentDomainService.deleteBy(document.getDocumentId());
        documentDomainService.findBy(documentId);
    }

    @Test
    public void test_created_doc_has_initial_version() throws Exception {
        List<Document> list = documentDomainService.findBy(tenantId, new PageRequest(0, 100), projectId, "").getContent();

        assertThat(list).hasSize(1);
        Document doc = documentDomainService.findBy(list.get(0).getDocumentId());
        assertThat(doc.getRevision()).isEqualTo(0);
        assertThat(projectDomainQueryService.findVersionBy(projectVersionId).isMain()).isEqualTo(true);
    }

    @Test(expectedExceptions = DocumentAlreadyExistsException.class)
    public void test_create_doc_same_version_throws_exception_1() throws Exception {
        documentDomainService.create(tenantId, projectVersionId, template.getTemplateId());
    }

    @Test
    public void test_create_doc_many_versions_but_single_main_tag() throws Exception {
        documentDomainService.create(tenantId, projectDomainService.createVersion(tenantId, projectId, randomAlphabetic(5), 3, false).getProjectVersionId(), template.getTemplateId());

        documentDomainService.create(tenantId, projectDomainService.createVersion(tenantId, projectId, randomAlphabetic(5), 4, false).getProjectVersionId(), template.getTemplateId());
        documentDomainService.create(tenantId, projectDomainService.createVersion(tenantId, projectId, randomAlphabetic(5), 5, false).getProjectVersionId(), template.getTemplateId());
        documentDomainService.create(tenantId, projectDomainService.createVersion(tenantId, projectId, randomAlphabetic(5), 6, false).getProjectVersionId(), template.getTemplateId());

        List<Document> all = documentDomainService.findBy(tenantId, new PageRequest(0, 100, Sort.Direction.ASC, "revision"), projectId, "").getContent();

        assertThat(all).hasSize(5);
        assertThat(all).areExactly(1, new Condition<Document>() {
            @Override
            public boolean matches(Document value) {
                try {
                    return projectDomainQueryService.findVersionBy(value.getProjectVersionId()).isMain().equals(true);
                } catch (ProjectVersionNotFoundException e) {
                    throw new RuntimeException(e);
                }
            }
        });

        assertThat(all).isSortedAccordingTo(new Comparator<Document>() {
            @Override
            public int compare(Document o1, Document o2) {
                return o1.getRevision().compareTo(o2.getRevision());
            }
        });
    }


    @Test
    public void test_created_doc_has_status_new() throws Exception {
        Document doc = documentDomainService.findBy(documentId);
        assertThat(doc.getStatus()).isEqualTo(Status.NEW);
    }

    @Test
    public void test_publish_doc_increments_version() throws Exception {
        Document doc = documentDomainService.findBy(documentId);
        assertThat(doc.getStatus()).isEqualTo(Status.NEW);
        assertThat(doc.getRevision()).isEqualTo(0);

        documentDomainService.publish(doc.getDocumentId(), randomAlphabetic(5));
        doc = documentDomainService.findBy(documentId);
        assertThat(doc.getStatus()).isEqualTo(Status.PUBLISHED);
        assertThat(doc.getRevision()).isEqualTo(1);
    }

    @Test
    public void test_approve_doc_publishes_also() throws Exception {
        Document doc = documentDomainService.findBy(documentId);
        assertThat(doc.getStatus()).isEqualTo(Status.NEW);
        assertThat(doc.getRevision()).isEqualTo(0);

        documentDomainService.approve(doc.getDocumentId(), randomAlphabetic(5));
        doc = documentDomainService.findBy(doc.getDocumentId());
        assertThat(doc.getStatus()).isEqualTo(Status.APPROVED);
        assertThat(doc.getRevision()).isEqualTo(1);
    }

    @Test
    public void test_approve_doc() throws Exception {
        Document doc = documentDomainService.findBy(documentId);
        assertThat(doc.getStatus()).isEqualTo(Status.NEW);
        assertThat(doc.getRevision()).isEqualTo(0);

        documentDomainService.publish(doc.getDocumentId(), randomAlphabetic(5));
        doc = documentDomainService.findBy(documentId);
        assertThat(doc.getStatus()).isEqualTo(Status.PUBLISHED);
        assertThat(doc.getRevision()).isEqualTo(1);
        documentDomainService.approve(doc.getDocumentId(), randomAlphabetic(5));
        doc = documentDomainService.findBy(documentId);
        assertThat(doc.getStatus()).isEqualTo(Status.APPROVED);
        assertThat(doc.getRevision()).isEqualTo(1);

    }

    @Test(expectedExceptions = DocumentNotEditableException.class)
    public void test_approved_doc_cannot_be_edited() throws Exception {
        Document doc = documentDomainService.findBy(documentId);
        documentDomainService.approve(doc.getDocumentId(), randomAlphabetic(5));
        documentDomainService.edit(doc.getDocumentId());
    }

    @Test
    public void test_delete_project_deletes_documents() throws Exception {
        documentDomainService.create(tenantId, projectDomainService.createVersion(tenantId, projectId, randomAlphabetic(5), 3, false).getProjectVersionId(), template.getTemplateId());
        documentDomainService.create(tenantId, projectDomainService.createVersion(tenantId, projectId, randomAlphabetic(5), 4, false).getProjectVersionId(), template.getTemplateId());

        projectDomainService.deleteProject(projectId);

        List<Document> documents = documentDomainService.findBy(tenantId, new PageRequest(0, 100), projectId, "").getContent();

        assertThat(documents.isEmpty());
        List<ProjectVersion> projectVersions = projectDomainQueryService.findAllVersionsBy(projectId);
        assertThat(projectVersions.isEmpty());
    }
}
