/*
 * Copyright (c) 2014.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * 	http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.clusterra.pmbok.document.application.template;

import com.clusterra.pmbok.document.domain.AbstractDocumentTest;
import com.clusterra.pmbok.document.domain.service.document.listen.TemplateUsedException;
import org.testng.annotations.Test;

import static org.fest.assertions.api.Assertions.assertThat;
import static org.fest.assertions.api.Assertions.fail;


public class TemplateServiceTest extends AbstractDocumentTest {

    @Test
    public void test_delete_with_document() throws Exception {
        try {
            templateService.deleteTemplate(template.getTemplateId());
            fail("should fail deletion of used template");
        } catch (TemplateUsedException e) {
            assertThat(e).hasMessageContaining(template.getTemplateId().toString());
            assertThat(e).hasMessageContaining(documentId.toString());
        }
    }
}