/*
 * Copyright (c) 2014.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * 	http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.clusterra.pmbok.document.domain.service.document;

import com.clusterra.pmbok.document.domain.model.template.Template;
import com.clusterra.pmbok.document.domain.AbstractDocumentTransactionalTest;
import com.clusterra.pmbok.document.domain.model.document.Document;
import com.clusterra.pmbok.project.domain.model.Project;
import org.springframework.data.domain.PageRequest;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.util.List;
import java.util.Set;

import static org.apache.commons.lang3.RandomStringUtils.randomAlphabetic;
import static org.fest.assertions.api.Assertions.assertThat;

/**
 * Created with IntelliJ IDEA.
 *
 * @author Denis Kuchugurov
 *         Date: 07.12.13
 */
public class DocumentDomainService_2_Test extends AbstractDocumentTransactionalTest {


    private Template planTemplate;

    @BeforeMethod
    public void beforeMethod() throws Exception {
        planTemplate = templateDomainService.createTemplate(tenantId, 0, 1, randomAlphabetic(10));

        documentDomainService.create(tenantId, projectDomainService.createVersion(tenantId, projectId, randomAlphabetic(5), 3, false).getProjectVersionId(), template.getTemplateId());
        documentDomainService.create(tenantId, projectDomainService.createVersion(tenantId, projectId, randomAlphabetic(5), 4, false).getProjectVersionId(), template.getTemplateId());
        documentDomainService.create(tenantId, projectVersionId, planTemplate.getTemplateId());
        documentDomainService.create(tenantId, projectDomainService.createVersion(tenantId, projectId, randomAlphabetic(5), 5, false).getProjectVersionId(), planTemplate.getTemplateId());
    }

    @Test
    public void test_find_all() throws Exception {
        List<Document> list = documentDomainService.findBy(tenantId, new PageRequest(0, 10), projectId, "").getContent();
        assertThat(list).hasSize(5);
    }

    @Test
    public void test_find_all_search_by() throws Exception {
        List<Document> list = documentDomainService.findBy(tenantId, new PageRequest(0, 10), projectId, planTemplate.getName()).getContent();
        assertThat(list).hasSize(2);

        list = documentDomainService.findBy(tenantId, new PageRequest(0, 10), projectId, "new").getContent();
        assertThat(list).hasSize(5);
    }

    @Test
    public void test_find_by_tenant() throws Exception {
        List<Document> documents = documentDomainService.findBy(tenantId, new PageRequest(0, 100), "").getContent();
        assertThat(documents).isNotEmpty();
    }

    @Test
    public void test_document_types() {
        Set<Template> documentTypes = documentDomainService.findUsedTemplates(projectVersionId);
        assertThat(documentTypes).containsExactly(template, planTemplate);
    }

    @Test
    public void test_document_types_new_project() throws Exception {
        Project project = projectDomainService.createProject(tenantId, randomAlphabetic(10));
        Set<Template> templates = documentDomainService.findUsedTemplates(projectDomainQueryService.findAllVersionsBy(project.getProjectId()).iterator().next().getProjectVersionId());
        assertThat(templates).isEmpty();
    }


}
