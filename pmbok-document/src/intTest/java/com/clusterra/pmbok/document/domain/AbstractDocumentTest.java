/*
 * Copyright (c) 2014.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * 	http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.clusterra.pmbok.document.domain;

import com.clusterra.iam.core.application.group.GroupDescriptor;
import com.clusterra.iam.core.application.group.GroupService;
import com.clusterra.iam.core.application.membership.AuthorizedMembershipService;
import com.clusterra.iam.core.application.role.RoleDescriptor;
import com.clusterra.iam.core.application.role.RoleService;
import com.clusterra.iam.core.application.tenant.TenantCommandService;
import com.clusterra.iam.core.application.tenant.TenantId;
import com.clusterra.iam.core.application.tracker.IdentityTrackerLifeCycle;
import com.clusterra.iam.core.application.user.DefaultRole;
import com.clusterra.iam.core.application.user.UserCommandService;
import com.clusterra.iam.core.application.user.UserId;
import com.clusterra.iam.core.domain.model.tenant.Tenant;
import com.clusterra.iam.core.testutil.UserRandomUtil;
import com.clusterra.pmbok.document.application.document.DocumentCommandService;
import com.clusterra.pmbok.document.application.template.TemplateService;
import com.clusterra.pmbok.document.domain.model.document.Document;
import com.clusterra.pmbok.document.domain.model.document.DocumentId;
import com.clusterra.pmbok.document.domain.model.template.Template;
import com.clusterra.pmbok.document.domain.model.template.section.SectionTemplate;
import com.clusterra.pmbok.project.application.ProjectCommandService;
import com.clusterra.pmbok.project.application.ProjectQueryService;
import com.clusterra.pmbok.project.domain.model.Project;
import com.clusterra.pmbok.project.domain.model.ProjectId;
import com.clusterra.pmbok.project.domain.model.ProjectVersion;
import com.clusterra.pmbok.project.domain.model.ProjectVersionId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.ConfigFileApplicationContextInitializer;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.testng.AbstractTransactionalTestNGSpringContextTests;
import org.testng.annotations.BeforeMethod;

import static org.apache.commons.lang3.RandomStringUtils.randomAlphabetic;

/**
 * @author Denis Kuchugurov.
 *         09.07.2014.
 */
@ContextConfiguration(
        value = {"classpath*:META-INF/spring/*.xml"},
        initializers = ConfigFileApplicationContextInitializer.class)
public class AbstractDocumentTest extends AbstractTransactionalTestNGSpringContextTests {

    @Autowired
    protected DocumentCommandService documentCommandService;

    @Autowired
    protected ProjectCommandService projectCommandService;

    @Autowired
    protected ProjectQueryService projectQueryService;

    @Autowired
    protected TemplateService templateService;

    protected DocumentId documentId;
    protected ProjectId projectId;
    protected ProjectVersionId projectVersionId;
    protected Template template;
    protected SectionTemplate textSectionTemplate;

    @Autowired
    private TenantCommandService tenantCommandService;

    private TenantId tenantId;

    @Autowired
    private UserCommandService userCommandService;

    @Autowired
    private RoleService roleService;

    @Autowired
    private GroupService groupService;

    @Autowired
    private AuthorizedMembershipService authorizedMembershipService;

    @Autowired
    private IdentityTrackerLifeCycle identityTrackerLifeCycle;


    @BeforeMethod
    public void setup_document() throws Exception {
        Tenant tenant = tenantCommandService.create(randomAlphabetic(10), UserRandomUtil.getRandomEmail());
        tenantId = new TenantId(tenant.getId());

        startTrackingUser();
        template = templateService.createTemplate(0, 1, randomAlphabetic(10));

        textSectionTemplate = templateService.addTextSection(template.getTemplateId(), randomAlphabetic(10));

        Project project = projectCommandService.createProject(randomAlphabetic(10));
        projectId = project.getProjectId();
        ProjectVersion projectVersion = projectCommandService.createVersion(projectId, randomAlphabetic(5), 2, true);
        projectVersionId = projectVersion.getProjectVersionId();
        Document document = documentCommandService.create(projectVersionId, template.getTemplateId());
        documentId = document.getDocumentId();
    }

    private void startTrackingUser() throws Exception {
        UserId userId = new UserId(userCommandService.create(tenantId, randomAlphabetic(5), UserRandomUtil.getRandomEmail(), UserRandomUtil.randomPassword(), randomAlphabetic(5), randomAlphabetic(5)).getId());

        RoleDescriptor role = roleService.createRole(tenantId, DefaultRole.ADMIN);
        GroupDescriptor group = groupService.createGroup(tenantId, randomAlphabetic(5));

        authorizedMembershipService.createAuthorizedMembershipIfNotExists(tenantId, userId, role, group);

        identityTrackerLifeCycle.startTracking(userId, tenantId);
    }

}
