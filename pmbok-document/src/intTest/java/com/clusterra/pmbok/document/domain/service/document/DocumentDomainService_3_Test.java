/*
 * Copyright (c) 2014.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * 	http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.clusterra.pmbok.document.domain.service.document;

import com.clusterra.pmbok.document.domain.AbstractDocumentTransactionalTest;
import com.clusterra.pmbok.document.domain.model.document.Document;
import com.clusterra.pmbok.document.domain.model.document.DocumentId;
import com.clusterra.pmbok.document.domain.model.document.section.PersistedSectionContent;
import com.clusterra.pmbok.document.domain.model.document.section.repo.PersistedSectionContentRepository;
import com.clusterra.pmbok.document.domain.model.document.section.text.PersistedTextSectionContent;
import com.clusterra.pmbok.document.domain.model.template.SectionTemplateAlreadyExistsException;
import com.clusterra.pmbok.document.domain.model.template.section.SectionTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.testng.annotations.Test;

import java.util.List;

import static org.apache.commons.lang3.RandomStringUtils.randomAlphabetic;
import static org.fest.assertions.api.Assertions.assertThat;

/**
 * Created by dkuchugurov on 02.04.2014.
 */
public class DocumentDomainService_3_Test extends AbstractDocumentTransactionalTest {

    @Autowired
    private PersistedSectionContentRepository<PersistedSectionContent> repository;

    @Test
    public void test_update() throws Exception {
        String newText = randomAlphabetic(50);
        PersistedTextSectionContent section = documentDomainService.updateTextSection(documentId, textSectionTemplate.getSectionTemplateId(), newText);

        assertThat(section.getText()).isEqualTo(newText);
    }

    @Test
    public void test_find_all() throws Exception {
        String text = randomAlphabetic(10);
        PersistedTextSectionContent content = documentDomainService.updateTextSection(documentId, textSectionTemplate.getSectionTemplateId(), text);
        assertThat(content.getText()).isEqualTo(text);
    }

    @Test
    public void test_delete_doc_with_sections() throws Exception {

        SectionTemplate section2 = templateDomainService.addTermSection(template.getTemplateId(), randomAlphabetic(10));
        SectionTemplate section3 = templateDomainService.addTextSection(template.getTemplateId(), randomAlphabetic(10));

        documentDomainService.updateTextSection(documentId, section2.getSectionTemplateId(), randomAlphabetic(10));
        documentDomainService.updateTextSection(documentId, section3.getSectionTemplateId(), randomAlphabetic(10));

        PersistedSectionContent sectionContent2 = repository.findBy(document, section2);
        PersistedSectionContent sectionContent3 = repository.findBy(document, section3);
        assertThat(sectionContent2).isNotNull();
        assertThat(sectionContent3).isNotNull();

        documentDomainService.deleteBy(documentId);
        List<Document> documents = documentDomainService.findBy(tenantId, new PageRequest(0, 100), projectId, "").getContent();
        assertThat(documents).isEmpty();

        sectionContent2 = repository.findBy(document, section2);
        sectionContent3 = repository.findBy(document, section3);
        assertThat(sectionContent2).isNull();
        assertThat(sectionContent3).isNull();
    }

    @Test(expectedExceptions = DocumentNotFoundException.class)
    public void test_create_history_section_throws_exception_1() throws Exception {
        documentDomainService.updateTextSection(new DocumentId(randomAlphabetic(10)), textSectionTemplate.getSectionTemplateId(), randomAlphabetic(10));
    }

    @Test(expectedExceptions = SectionTemplateAlreadyExistsException.class)
    public void test_create_history_section_throws_exception_2() throws Exception {
        templateDomainService.addReferenceSection(template.getTemplateId(), randomAlphabetic(5));
        templateDomainService.addReferenceSection(template.getTemplateId(), randomAlphabetic(5));
    }

}
