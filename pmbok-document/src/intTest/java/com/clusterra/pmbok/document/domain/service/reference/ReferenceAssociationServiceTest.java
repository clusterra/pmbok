/*
 * Copyright (c) 2014.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * 	http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.clusterra.pmbok.document.domain.service.reference;

import com.clusterra.iam.core.application.group.GroupDescriptor;
import com.clusterra.iam.core.application.group.GroupService;
import com.clusterra.iam.core.application.membership.AuthorizedMembershipService;
import com.clusterra.iam.core.application.role.RoleDescriptor;
import com.clusterra.iam.core.application.role.RoleService;
import com.clusterra.iam.core.application.tenant.TenantCommandService;
import com.clusterra.iam.core.application.tenant.TenantId;
import com.clusterra.iam.core.application.tracker.IdentityTrackerLifeCycle;
import com.clusterra.iam.core.application.user.DefaultRole;
import com.clusterra.iam.core.application.user.UserCommandService;
import com.clusterra.iam.core.application.user.UserId;
import com.clusterra.iam.core.domain.model.tenant.Tenant;
import com.clusterra.iam.core.testutil.UserRandomUtil;
import com.clusterra.pmbok.document.application.template.TemplateService;
import com.clusterra.pmbok.document.domain.model.document.Document;
import com.clusterra.pmbok.document.domain.model.document.DocumentId;
import com.clusterra.pmbok.document.domain.model.template.TemplateId;
import com.clusterra.pmbok.document.domain.service.document.DocumentDomainService;
import com.clusterra.pmbok.project.domain.model.Project;
import com.clusterra.pmbok.project.domain.model.ProjectVersion;
import com.clusterra.pmbok.project.domain.service.ProjectDomainService;
import com.clusterra.pmbok.reference.domain.model.reference.Reference;
import com.clusterra.pmbok.reference.domain.service.ReferenceDomainService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.ConfigFileApplicationContextInitializer;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.testng.AbstractTransactionalTestNGSpringContextTests;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.util.Date;

import static org.apache.commons.lang3.RandomStringUtils.randomAlphabetic;
import static org.fest.assertions.api.Assertions.assertThat;

/**
 * Created with IntelliJ IDEA.
 *
 * @author Denis Kuchugurov
 *         Date: 05.02.14
 */
@ContextConfiguration(
        value = {"classpath*:META-INF/spring/*.xml"},
        initializers = ConfigFileApplicationContextInitializer.class)
public class ReferenceAssociationServiceTest extends AbstractTransactionalTestNGSpringContextTests {

    @Autowired
    private ReferenceDomainService referenceDomainService;

    @Autowired
    private DocumentDomainService documentDomainService;

    @Autowired
    private ProjectDomainService projectDomainService;

    @Autowired
    private TemplateService templateDomainService;

    @Autowired
    private ReferenceAssociationService referenceAssociationService;

    private Reference reference1;
    private Reference reference2;
    private DocumentId documentId;

    @Autowired
    private TenantCommandService tenantCommandService;

    private TenantId tenantId;

    @Autowired
    private UserCommandService userCommandService;

    @Autowired
    private RoleService roleService;

    @Autowired
    private GroupService groupService;

    @Autowired
    private AuthorizedMembershipService authorizedMembershipService;

    @Autowired
    private IdentityTrackerLifeCycle identityTrackerLifeCycle;


    @BeforeMethod
    public void beforeMethod() throws Exception {
        Tenant tenant = tenantCommandService.create(randomAlphabetic(10), UserRandomUtil.getRandomEmail());
        tenantId = new TenantId(tenant.getId());

        startTrackingUser();

        reference1 = referenceDomainService.create(tenantId, randomAlphabetic(5), randomAlphabetic(5), new Date(), randomAlphabetic(5));
        reference2 = referenceDomainService.create(tenantId, randomAlphabetic(5), randomAlphabetic(5), new Date(), randomAlphabetic(5));
        Project project = projectDomainService.createProject(tenantId, randomAlphabetic(10));
        ProjectVersion projectVersion = projectDomainService.createVersion(tenantId, project.getProjectId(), randomAlphabetic(10), 2, false);
        TemplateId templateId = templateDomainService.createTemplate(1, 0, randomAlphabetic(10)).getTemplateId();

        Document document = documentDomainService.create(tenantId, projectVersion.getProjectVersionId(), templateId);
        documentId = document.getDocumentId();
    }

    private void startTrackingUser() throws Exception {
        UserId userId = new UserId(userCommandService.create(tenantId, randomAlphabetic(5), UserRandomUtil.getRandomEmail(), UserRandomUtil.randomPassword(), randomAlphabetic(5), randomAlphabetic(5)).getId());

        RoleDescriptor role = roleService.createRole(tenantId, DefaultRole.ADMIN);
        GroupDescriptor group = groupService.createGroup(tenantId, randomAlphabetic(5));

        authorizedMembershipService.createAuthorizedMembershipIfNotExists(tenantId, userId, role, group);

        identityTrackerLifeCycle.startTracking(userId, tenantId);
    }

    @Test
    public void test_find_by_document_and_search_by() throws Exception {
        Reference reference_1 = referenceDomainService.create(tenantId, randomAlphabetic(5), randomAlphabetic(5), new Date(), randomAlphabetic(5));
        String number = randomAlphabetic(10);
        String searchBy = number.substring(2, 7);
        Reference reference_2 = referenceDomainService.create(tenantId, number, randomAlphabetic(5), new Date(), randomAlphabetic(5));
        Reference reference_3 = referenceDomainService.create(tenantId, randomAlphabetic(5), randomAlphabetic(5), new Date(), randomAlphabetic(5));
        Reference reference_4 = referenceDomainService.create(tenantId, randomAlphabetic(5), randomAlphabetic(5), new Date(), randomAlphabetic(5));

        Page<Reference> page = referenceDomainService.findBy(tenantId, new PageRequest(0, 100), "");
        assertThat(page.getContent()).hasSize(7);

        referenceAssociationService.createAssociation(tenantId, documentId, reference_1.getReferenceId());
        referenceAssociationService.createAssociation(tenantId, documentId, reference_2.getReferenceId());
        referenceAssociationService.createAssociation(tenantId, documentId, reference_3.getReferenceId());
        referenceAssociationService.createAssociation(tenantId, documentId, reference_4.getReferenceId());


        page = referenceAssociationService.findBy(tenantId, new PageRequest(0, 100), documentId, "");
        assertThat(page.getContent()).contains(reference_1, reference_2, reference_3, reference_4);
        page = referenceAssociationService.findBy(tenantId, new PageRequest(0, 100), documentId, searchBy);
        assertThat(page.getContent()).containsOnly(reference_2);
    }

    @Test
    public void test_association_pageable() throws Exception {
        referenceAssociationService.createAssociation(tenantId, documentId, reference1.getReferenceId());
        referenceAssociationService.createAssociation(tenantId, documentId, reference2.getReferenceId());
        Page<Reference> referencePage = referenceAssociationService.findBy(tenantId, new PageRequest(0, 1), documentId, "");
        assertThat(referencePage.getContent()).hasSize(1);
    }

    @Test
    public void test_associate_is_idempotent() throws Exception {
        referenceAssociationService.createAssociation(tenantId, documentId, reference1.getReferenceId());
        referenceAssociationService.createAssociation(tenantId, documentId, reference1.getReferenceId());
        referenceAssociationService.createAssociation(tenantId, documentId, reference1.getReferenceId());
        referenceAssociationService.createAssociation(tenantId, documentId, reference1.getReferenceId());
        Page<Reference> referencePage = referenceAssociationService.findBy(tenantId, new PageRequest(0, 100), documentId, "");
        assertThat(referencePage.getContent()).hasSize(2);
    }

    @Test
    public void test_unAssociate_is_idempotent() throws Exception {
        referenceAssociationService.createAssociation(tenantId, documentId, reference1.getReferenceId());
        Page<Reference> references = referenceAssociationService.findBy(tenantId, new PageRequest(0, 100), documentId, "");
        assertThat(references.getContent()).hasSize(2);

        referenceAssociationService.deleteAssociation(tenantId, documentId, reference1.getReferenceId());
        referenceAssociationService.deleteAssociation(tenantId, documentId, reference1.getReferenceId());
        Page<Reference> referencePage = referenceAssociationService.findBy(tenantId, new PageRequest(0, 100), documentId, "");

        assertThat(referencePage.getContent()).hasSize(1);
    }
}
