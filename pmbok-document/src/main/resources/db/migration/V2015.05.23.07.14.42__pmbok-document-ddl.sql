
    create table pmb_doc (
        id varchar(255) not null,
        createdByUserId varchar(255),
        createdDate timestamp,
        modifiedByUserId varchar(255),
        modifiedDate timestamp,
        projectId varchar(255) not null,
        projectVersionId varchar(255) not null,
        revision int4 not null,
        status varchar(255) not null,
        tenantId varchar(255) not null,
        touchCounter int4 not null,
        template_id varchar(255),
        primary key (id)
    );

    create table pmb_doc_history_entry (
        id varchar(255) not null,
        comment varchar(3000),
        createdByUserId varchar(255),
        createdDate timestamp,
        revision varchar(255) not null,
        document_id varchar(255),
        primary key (id)
    );

    create table pmb_doc_reference_association (
        id varchar(255) not null,
        createdByUserId varchar(255),
        createdDate timestamp,
        referenceId varchar(255) not null,
        tenantId varchar(255) not null,
        document_id varchar(255),
        primary key (id)
    );

    create table pmb_doc_template (
        id varchar(255) not null,
        createdByUserId varchar(255),
        createdDate timestamp,
        modifiedByUserId varchar(255),
        modifiedDate timestamp,
        name varchar(255),
        status varchar(255) not null,
        tenantId varchar(255) not null,
        touchCounter int4 not null,
        major int4,
        minor int4,
        primary key (id)
    );

    create table pmb_doc_term_association (
        id varchar(255) not null,
        createdByUserId varchar(255),
        createdDate timestamp,
        tenantId varchar(255) not null,
        termId varchar(255) not null,
        document_id varchar(255),
        primary key (id)
    );

    create table pmb_section (
        id varchar(255) not null,
        createdByUserId varchar(255),
        createdDate timestamp,
        modifiedByUserId varchar(255),
        modifiedDate timestamp,
        document_id varchar(255),
        sectionTemplate_id varchar(255),
        primary key (id)
    );

    create table pmb_section_template (
        DTYPE varchar(31) not null,
        id varchar(255) not null,
        createdByUserId varchar(255),
        createdDate timestamp,
        modifiedByUserId varchar(255),
        modifiedDate timestamp,
        name varchar(255),
        orderIndex int4 not null,
        type varchar(255) not null,
        template_id varchar(255),
        primary key (id)
    );

    create table pmb_section_text (
        text varchar(3000),
        id varchar(255) not null,
        primary key (id)
    );

    alter table pmb_doc_reference_association 
        add constraint UK_9w9ynvovnr046fm3o1lfr1wtn  unique (referenceId, document_id);

    alter table pmb_doc_term_association 
        add constraint UK_8incfgvbqc6s5pnkb5kico6lh  unique (termId, document_id);

    alter table pmb_doc 
        add constraint FK_tm1wxm0o0fo5gevof6joss7u 
        foreign key (template_id) 
        references pmb_doc_template;

    alter table pmb_doc_history_entry 
        add constraint FK_3nf007db99972qf4r2gd8ptnl 
        foreign key (document_id) 
        references pmb_doc;

    alter table pmb_doc_reference_association 
        add constraint FK_hf04jompschbp5qd20ux0od0g 
        foreign key (document_id) 
        references pmb_doc;

    alter table pmb_doc_term_association 
        add constraint FK_hi3x0a5p2vkh471uh5c9xlvy9 
        foreign key (document_id) 
        references pmb_doc;

    alter table pmb_section 
        add constraint FK_2op2mkymeoikyf50bu9nhp6q1 
        foreign key (document_id) 
        references pmb_doc;

    alter table pmb_section 
        add constraint FK_lixgjupjajkyau8ld78g9tdxf 
        foreign key (sectionTemplate_id) 
        references pmb_section_template;

    alter table pmb_section_template 
        add constraint FK_t6n45cqr9rv7rahrtx491ftm2 
        foreign key (template_id) 
        references pmb_doc_template;

    alter table pmb_section_text 
        add constraint FK_9gmh2b1jv9xyxte1vmgewb83i 
        foreign key (id) 
        references pmb_section;
