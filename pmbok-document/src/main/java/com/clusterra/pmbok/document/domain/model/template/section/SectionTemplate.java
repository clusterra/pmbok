/*
 * Copyright (c) 2014.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * 	http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.clusterra.pmbok.document.domain.model.template.section;

import com.clusterra.pmbok.document.domain.model.document.Document;
import com.clusterra.pmbok.document.domain.model.document.section.SectionContent;
import com.clusterra.pmbok.document.domain.model.template.Template;
import com.clusterra.pmbok.document.domain.service.document.SectionContentBuilder;
import org.apache.commons.lang3.Validate;
import org.hibernate.annotations.GenericGenerator;
import org.springframework.data.annotation.CreatedBy;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedBy;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import java.util.Date;

/**
 * @author Denis Kuchugurov.
 *         11.07.2014.
 */
@Entity
@EntityListeners({AuditingEntityListener.class})
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
@Table(name = "pmb_section_template")
public abstract class SectionTemplate {


    @Id
    @GeneratedValue(generator = "base64")
    @GenericGenerator(name = "base64", strategy = "com.clusterra.hibernate.base64.Base64IdGenerator")
    private String id;

    @Basic
    @CreatedDate
    private Date createdDate;

    @Basic
    @CreatedBy
    private String createdByUserId;

    @Basic
    @LastModifiedDate
    private Date modifiedDate;

    @Basic
    @LastModifiedBy
    private String modifiedByUserId;

    @Enumerated(EnumType.STRING)
    @Column(nullable = false)
    private SectionType type;

    @Basic
    private String name;

    @ManyToOne
    private Template template;

    @Basic
    @Column(nullable = false)
    private Integer orderIndex;

    SectionTemplate() {
    }

    public SectionTemplate(Template template, SectionType type, String name, Integer orderIndex) {
        Validate.notNull(template);
        Validate.notNull(type);
        Validate.notEmpty(name);
        Validate.notNull(orderIndex);
        this.template = template;
        this.type = type;
        this.name = name;
        Validate.inclusiveBetween(Integer.valueOf(0), Integer.valueOf(Integer.MAX_VALUE), orderIndex);
        this.orderIndex = orderIndex;
    }

    public abstract SectionContent getSectionContent(Document document, SectionContentBuilder builder);

    public Template getTemplate() {
        return template;
    }

    public String getTitle() {
        return String.format("%s. %s", orderIndex + 1, name);
    }

    public Integer getOrderIndex() {
        return orderIndex;
    }

    public void setOrderIndex(Integer orderIndex) {
        Validate.notNull(orderIndex);
        this.orderIndex = orderIndex;
    }

    public SectionTemplateId getSectionTemplateId() {
        return new SectionTemplateId(id);
    }

    public Date getCreatedDate() {
        return createdDate;
    }

    public String getCreatedByUserId() {
        return createdByUserId;
    }

    public Date getModifiedDate() {
        return modifiedDate;
    }

    public String getModifiedByUserId() {
        return modifiedByUserId;
    }

    public SectionType getType() {
        return type;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        Validate.notEmpty(name);
        this.name = name;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        SectionTemplate that = (SectionTemplate) o;

        if (id != null ? !id.equals(that.id) : that.id != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        return id != null ? id.hashCode() : 0;
    }

    @Override
    public String toString() {
        return "SectionTemplate{" +
                "id='" + id + '\'' +
                ", name='" + name + '\'' +
                ", orderIndex=" + orderIndex +
                ", type=" + type +
                '}';
    }
}
