/*
 * Copyright (c) 2014.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * 	http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.clusterra.pmbok.document.application.document;

import com.clusterra.iam.core.application.tracker.NotAuthenticatedException;
import com.clusterra.pmbok.document.domain.model.template.TemplateId;
import com.clusterra.pmbok.document.domain.model.template.section.SectionTemplateId;
import com.clusterra.pmbok.document.domain.service.document.DocumentAlreadyExistsException;
import com.clusterra.pmbok.document.domain.service.document.DocumentNotEditableException;
import com.clusterra.pmbok.project.domain.model.ProjectVersionId;
import com.clusterra.pmbok.term.domain.model.term.TermId;
import com.clusterra.pmbok.term.domain.service.TermNotFoundException;
import com.clusterra.pmbok.document.domain.model.document.Document;
import com.clusterra.pmbok.document.domain.model.document.DocumentId;
import com.clusterra.pmbok.document.domain.model.document.section.text.PersistedTextSectionContent;
import com.clusterra.pmbok.document.domain.model.template.SectionTemplateNotFoundException;
import com.clusterra.pmbok.document.domain.service.document.DocumentNotFoundException;
import com.clusterra.pmbok.document.domain.service.template.TemplateNotFoundException;
import com.clusterra.pmbok.project.domain.service.ProjectVersionNotFoundException;
import com.clusterra.pmbok.reference.domain.model.reference.ReferenceId;
import com.clusterra.pmbok.reference.domain.service.ReferenceNotFoundException;

/**
 * Created by dkuchugurov on 02.04.2014.
 */
public interface DocumentCommandService {

    Document create(ProjectVersionId projectVersionId, TemplateId templateId) throws ProjectVersionNotFoundException, DocumentAlreadyExistsException, TemplateNotFoundException, NotAuthenticatedException;

    void deleteBy(DocumentId documentId) throws DocumentNotFoundException;

    Document publish(DocumentId documentId, String message) throws DocumentNotFoundException;

    Document approve(DocumentId documentId, String message) throws DocumentNotFoundException;

    Document edit(DocumentId documentId) throws DocumentNotFoundException, DocumentNotEditableException;

    PersistedTextSectionContent updateTextSection(DocumentId documentId, SectionTemplateId sectionTemplateId, String text) throws DocumentNotFoundException, SectionTemplateNotFoundException;


    void createAssociation(DocumentId documentId, ReferenceId referenceId) throws NotAuthenticatedException, ReferenceNotFoundException, DocumentNotFoundException;

    void deleteAssociation(DocumentId documentId, ReferenceId referenceId) throws NotAuthenticatedException, ReferenceNotFoundException, DocumentNotFoundException;

    void createAssociation(DocumentId documentId, TermId termId) throws NotAuthenticatedException, TermNotFoundException, DocumentNotFoundException;

    void deleteAssociation(DocumentId documentId, TermId termId) throws NotAuthenticatedException, TermNotFoundException, DocumentNotFoundException;
}
