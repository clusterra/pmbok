/*
 * Copyright (c) 2014.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * 	http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.clusterra.pmbok.document.domain.service.document;

import com.clusterra.iam.core.application.tenant.TenantId;
import com.clusterra.pmbok.document.domain.model.document.Document;
import com.clusterra.pmbok.document.domain.model.document.DocumentId;
import com.clusterra.pmbok.document.domain.model.document.DocumentRevision;
import com.clusterra.pmbok.document.domain.model.document.section.SectionContent;
import com.clusterra.pmbok.document.domain.model.document.section.text.PersistedTextSectionContent;
import com.clusterra.pmbok.document.domain.model.template.SectionTemplateNotFoundException;
import com.clusterra.pmbok.document.domain.model.template.Template;
import com.clusterra.pmbok.document.domain.model.template.TemplateId;
import com.clusterra.pmbok.document.domain.model.template.section.SectionTemplateId;
import com.clusterra.pmbok.project.domain.model.ProjectId;
import com.clusterra.pmbok.project.domain.model.ProjectVersionId;
import com.clusterra.pmbok.project.domain.service.ProjectNotFoundException;
import com.clusterra.pmbok.project.domain.service.ProjectVersionNotFoundException;
import com.clusterra.pmbok.document.domain.service.template.TemplateNotFoundException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;
import java.util.Set;

/**
 * Created by dkuchugurov on 02.04.2014.
 */
public interface DocumentDomainService {

    Document create(TenantId tenantId, ProjectVersionId projectVersionId, TemplateId templateId) throws DocumentAlreadyExistsException, ProjectVersionNotFoundException, TemplateNotFoundException;

    void deleteBy(DocumentId documentId) throws DocumentNotFoundException;

    void deleteBy(ProjectVersionId projectVersionId) throws DocumentNotFoundException;

    Document publish(DocumentId documentId, String message) throws DocumentNotFoundException;

    Document approve(DocumentId documentId, String message) throws DocumentNotFoundException;

    Document edit(DocumentId documentId) throws DocumentNotFoundException, DocumentNotEditableException;


    Document findBy(DocumentId documentId) throws DocumentNotFoundException;

    List<Document> findBy(TemplateId documentId) throws TemplateNotFoundException;

    Page<Document> findBy(TenantId tenantId, Pageable pageable, String searchBy);

    Page<Document> findBy(TenantId tenantId, Pageable pageable, ProjectId projectId, String searchBy) throws ProjectNotFoundException;

    Page<Document> findBy(TenantId tenantId, Pageable pageable, ProjectVersionId projectVersionId, String searchBy);

    Set<Template> findUsedTemplates(ProjectVersionId projectVersionId);

    DocumentRevision getRevision(DocumentId documentId) throws DocumentNotFoundException;

    List<SectionContent> findSectionContentsBy(TenantId tenantId, DocumentId documentId) throws DocumentNotFoundException, TemplateNotFoundException;

    PersistedTextSectionContent updateTextSection(DocumentId documentId, SectionTemplateId sectionTemplateId, String text) throws DocumentNotFoundException, SectionTemplateNotFoundException;

}
