/*
 * Copyright (c) 2014.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * 	http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.clusterra.pmbok.document.domain.model.document.section.toc;

import com.clusterra.pmbok.document.domain.model.document.DocumentId;
import com.clusterra.pmbok.document.domain.model.document.section.SectionContentVisitor;
import com.clusterra.pmbok.document.domain.model.template.section.SectionTemplate;
import com.clusterra.pmbok.document.domain.model.document.section.ASectionContent;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by kepkap on 10/11/14.
 */
public class TocSectionContent extends ASectionContent {

    private final List<TocElement> tocElements;

    public TocSectionContent(SectionTemplate sectionTemplate, DocumentId documentId, List<SectionTemplate> sectionTemplates) {
        super(sectionTemplate, documentId);

        tocElements = new ArrayList<>(sectionTemplates.size());
        for (SectionTemplate template : sectionTemplates) {
            tocElements.add(new TocElement(
                    template.getTitle(),
                    template.getSectionTemplateId(),
                    template.getOrderIndex()
            ));
        }
    }

    @Override
    public <T> T accept(SectionContentVisitor<T> visitor) throws Exception {
        return visitor.visit(this);
    }

    public List<TocElement> getTocElements() {
        return tocElements;
    }
}
