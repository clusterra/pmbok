/*
 * Copyright (c) 2014.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * 	http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.clusterra.pmbok.document.domain.model.document.section.title;

import com.clusterra.iam.core.application.user.UserId;
import com.clusterra.pmbok.document.domain.model.template.section.SectionTemplate;
import com.clusterra.pmbok.document.domain.model.document.DocumentId;
import com.clusterra.pmbok.document.domain.model.document.DocumentRevision;
import com.clusterra.pmbok.document.domain.model.document.section.ASectionContent;
import com.clusterra.pmbok.document.domain.model.document.section.SectionContentVisitor;

/**
 * @author Denis Kuchugurov.
 *         01.08.2014.
 */
public class TitleSectionContent extends ASectionContent {

    private final String projectName;

    private final DocumentRevision revision;

    private final UserId managerId;

    private final String avatarId;

    public TitleSectionContent(SectionTemplate sectionTemplate, DocumentId documentId, String projectName, String avatarId, DocumentRevision revision, UserId managerId) {
        super(sectionTemplate, documentId);
        this.projectName = projectName;
        this.revision = revision;
        this.managerId = managerId;
        this.avatarId = avatarId;
    }

    public UserId getManagerId() {
        return managerId;
    }

    public String getProjectName() {
        return projectName;
    }

    public DocumentRevision getRevision() {
        return revision;
    }

    public String getAvatarId() {
        return avatarId;
    }

    @Override
    public <T> T accept(SectionContentVisitor<T> visitor) throws Exception {
        return visitor.visit(this);
    }
}
