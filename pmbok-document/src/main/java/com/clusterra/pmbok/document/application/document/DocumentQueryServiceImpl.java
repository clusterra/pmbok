/*
 * Copyright (c) 2014.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * 	http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.clusterra.pmbok.document.application.document;

import com.clusterra.iam.core.application.tenant.TenantId;
import com.clusterra.iam.core.application.tracker.IdentityTracker;
import com.clusterra.iam.core.application.tracker.NotAuthenticatedException;
import com.clusterra.pmbok.document.domain.model.document.DocumentId;
import com.clusterra.pmbok.document.domain.model.document.DocumentRevision;
import com.clusterra.pmbok.document.domain.model.template.Template;
import com.clusterra.pmbok.document.domain.service.term.TermAssociationService;
import com.clusterra.pmbok.project.domain.model.ProjectVersionId;
import com.clusterra.pmbok.project.domain.service.ProjectNotFoundException;
import com.clusterra.pmbok.reference.domain.model.reference.Reference;
import com.clusterra.pmbok.document.domain.model.document.Document;
import com.clusterra.pmbok.document.domain.model.document.section.SectionContent;
import com.clusterra.pmbok.document.domain.service.document.DocumentDomainService;
import com.clusterra.pmbok.document.domain.service.document.DocumentNotFoundException;
import com.clusterra.pmbok.document.domain.service.reference.ReferenceAssociationService;
import com.clusterra.pmbok.document.domain.service.template.TemplateNotFoundException;
import com.clusterra.pmbok.project.domain.model.ProjectId;
import com.clusterra.pmbok.term.domain.model.term.Term;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Set;

/**
 * Created by dkuchugurov on 29.03.2014.
 */
@Service
public class DocumentQueryServiceImpl implements DocumentQueryService {

    @Autowired
    private DocumentDomainService documentDomainService;

    @Autowired
    private TermAssociationService termAssociationService;

    @Autowired
    private ReferenceAssociationService referenceAssociationService;

    @Autowired
    private IdentityTracker identityTracker;

    @Transactional
    public Document findBy(DocumentId documentId) throws DocumentNotFoundException {
        return documentDomainService.findBy(documentId);
    }

    @Transactional
    public Page<Document> findBy(Pageable pageable, String searchBy) throws NotAuthenticatedException {
        TenantId tenantId = identityTracker.currentTenant();
        return documentDomainService.findBy(tenantId, pageable, searchBy);
    }

    @Transactional
    public Page<Document> findBy(Pageable pageable, ProjectId projectId, String searchBy) throws ProjectNotFoundException, NotAuthenticatedException {
        TenantId tenantId = identityTracker.currentTenant();
        return documentDomainService.findBy(tenantId, pageable, projectId, searchBy);
    }

    @Transactional
    public Page<Document> findBy(Pageable pageable, ProjectVersionId projectVersionId, String searchBy) throws NotAuthenticatedException {
        TenantId tenantId = identityTracker.currentTenant();
        return documentDomainService.findBy(tenantId, pageable, projectVersionId, searchBy);
    }

    @Transactional
    public Set<Template> findUsedTemplates(ProjectVersionId projectVersionId) {
        return documentDomainService.findUsedTemplates(projectVersionId);
    }

    @Transactional
    public DocumentRevision getRevision(DocumentId documentId) throws DocumentNotFoundException {
        return documentDomainService.getRevision(documentId);
    }

    @Transactional
    public List<SectionContent> findSectionContents(DocumentId documentId) throws DocumentNotFoundException, TemplateNotFoundException, NotAuthenticatedException {
        return documentDomainService.findSectionContentsBy(identityTracker.currentTenant(), documentId);
    }

    @Transactional
    public Page<Term> getAssociatedTerms(Pageable pageable, DocumentId documentId, String searchBy) throws DocumentNotFoundException, NotAuthenticatedException {
        return termAssociationService.findBy(identityTracker.currentTenant(), pageable, documentId, searchBy);
    }

    @Transactional
    public Page<Reference> getAssociatedReferences(Pageable pageable, DocumentId documentId, String searchBy) throws DocumentNotFoundException, NotAuthenticatedException {
        return referenceAssociationService.findBy(identityTracker.currentTenant(), pageable, documentId, searchBy);
    }
}
