/*
 * Copyright (c) 2014.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * 	http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.clusterra.pmbok.document.domain.service.document.listen;

import com.clusterra.pmbok.document.domain.service.document.DocumentDomainService;
import com.clusterra.pmbok.project.domain.model.event.ProjectVersionDeletingEvent;
import com.clusterra.pmbok.document.domain.service.document.DocumentNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationListener;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

/**
 * Created by dkuchugurov on 28.06.2014.
 */
@Component
public class ProjectVersionDeletingEvent4DocumentListener implements ApplicationListener<ProjectVersionDeletingEvent> {

    @Autowired
    private DocumentDomainService service;

    @Transactional(propagation = Propagation.MANDATORY)
    public void onApplicationEvent(ProjectVersionDeletingEvent event) {
        try {
            service.deleteBy(event.getProjectVersionId());
        } catch (DocumentNotFoundException e) {
            throw new RuntimeException(e);
        }
    }
}
