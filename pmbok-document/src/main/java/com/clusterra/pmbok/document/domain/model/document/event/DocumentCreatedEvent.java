/*
 * Copyright (c) 2014.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * 	http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.clusterra.pmbok.document.domain.model.document.event;

import com.clusterra.iam.core.application.tenant.TenantId;
import com.clusterra.iam.core.application.user.UserId;
import com.clusterra.pmbok.document.domain.model.document.DocumentId;
import com.clusterra.pmbok.document.domain.model.template.TemplateId;
import com.clusterra.pmbok.project.domain.model.event.ProjectUpdatedEvent;
import com.clusterra.pmbok.project.domain.model.ProjectId;

/**
 * Created by dkuchugurov on 02.04.2014.
 */
public class DocumentCreatedEvent extends ProjectUpdatedEvent {

    private final TenantId tenantId;
    private final DocumentId documentId;
    private final TemplateId templateId;
    private final UserId createdByUserId;

    public DocumentCreatedEvent(Object source, TenantId tenantId, ProjectId projectId, DocumentId documentId, TemplateId templateId, UserId createdByUserId) {
        super(source, projectId, "Created");
        this.tenantId = tenantId;
        this.documentId = documentId;
        this.templateId = templateId;
        this.createdByUserId = createdByUserId;
    }

    public DocumentId getDocumentId() {
        return documentId;
    }

    public UserId getCreatedByUserId() {
        return createdByUserId;
    }

    public TenantId getTenantId() {
        return tenantId;
    }

    public TemplateId getTemplateId() {
        return templateId;
    }
}
