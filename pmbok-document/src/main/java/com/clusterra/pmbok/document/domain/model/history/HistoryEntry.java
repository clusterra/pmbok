/*
 * Copyright (c) 2014.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * 	http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.clusterra.pmbok.document.domain.model.history;

import com.clusterra.pmbok.document.domain.model.document.Document;
import org.apache.commons.lang3.Validate;
import org.hibernate.annotations.GenericGenerator;
import org.springframework.data.annotation.CreatedBy;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import java.util.Date;

/**
 * Created by dkuchugurov on 19.03.14.
 */
@Entity
@EntityListeners({AuditingEntityListener.class})
@Table(name = "pmb_doc_history_entry")
public class HistoryEntry {

    @Id
    @GeneratedValue(generator = "base64")
    @GenericGenerator(name = "base64", strategy = "com.clusterra.hibernate.base64.Base64IdGenerator")
    private String id;

    @Basic
    @Column(nullable = false)
    private String revision;

    @Basic
    @CreatedDate
    private Date createdDate;

    @Basic
    @CreatedBy
    private String createdByUserId;

    @Basic
    @Column(length = 3000)
    private String comment;

    @ManyToOne
    private Document document;

    HistoryEntry() {
    }

    public HistoryEntry(Document document, String revision, String comment) {
        Validate.notNull(document);
        Validate.notEmpty(revision, "revision is empty");
        Validate.notEmpty(comment, "comment is empty");
        this.document = document;
        this.revision = revision;
        this.comment = comment;
    }

    public Document getDocument() {
        return document;
    }

    public String getId() {
        return id;
    }

    public String getRevision() {
        return revision;
    }

    public Date getCreatedDate() {
        return createdDate;
    }

    public String getCreatedByUserId() {
        return createdByUserId;
    }

    public String getComment() {
        return comment;
    }

}
