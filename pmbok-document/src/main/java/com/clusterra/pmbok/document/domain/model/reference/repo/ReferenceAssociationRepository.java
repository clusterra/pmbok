/*
 * Copyright (c) 2014.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * 	http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.clusterra.pmbok.document.domain.model.reference.repo;

import com.clusterra.pmbok.document.domain.model.document.Document;
import com.clusterra.pmbok.document.domain.model.reference.ReferenceAssociation;
import com.clusterra.pmbok.reference.domain.model.reference.Reference;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.util.List;


/**
 * Created by dkuchugurov on 26.06.2014.
 */
public interface ReferenceAssociationRepository extends PagingAndSortingRepository<ReferenceAssociation, String> {


    @Query("select r from Reference r, ReferenceAssociation a where r.id = a.referenceId and a.document = ?1")
    Page<Reference> findBy(Pageable pageable, Document document);

    @Query("select r from Reference r, ReferenceAssociation a where r.id = a.referenceId and a.tenantId = ?1 and a.document = ?2 and (lower(r.name) like lower(?3) or lower(r.number) like lower(?3) or lower(r.author) like lower(?3))")
    Page<Reference> findBy(Pageable pageable, String tenantId, Document document, String searchBy);

    @Query("select r from Reference r, ReferenceAssociation a where r.id = a.referenceId and a.tenantId = ?1 and a.document = ?2 ")
    List<Reference> findBy(String tenantId, Document document);

    @Query("select a from ReferenceAssociation a where a.tenantId = ?1 and a.referenceId = ?2 and a.document = ?3")
    ReferenceAssociation findBy(String tenantId, String referenceId, Document document);

    @Modifying
    @Query("delete from ReferenceAssociation a where a.tenantId = ?1 and a.document = ?2")
    void deleteBy(String tenantId, Document document);
}
