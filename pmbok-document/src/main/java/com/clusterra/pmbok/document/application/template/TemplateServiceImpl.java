/*
 * Copyright (c) 2014.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * 	http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.clusterra.pmbok.document.application.template;

import com.clusterra.iam.core.application.tracker.IdentityTracker;
import com.clusterra.iam.core.application.tracker.NotAuthenticatedException;
import com.clusterra.pmbok.document.domain.model.template.SectionTemplateAlreadyExistsException;
import com.clusterra.pmbok.document.domain.model.template.Template;
import com.clusterra.pmbok.document.domain.model.template.TemplateId;
import com.clusterra.pmbok.document.domain.model.template.section.SectionTemplate;
import com.clusterra.pmbok.document.domain.model.template.section.SectionTemplateId;
import com.clusterra.pmbok.document.domain.model.template.section.SectionType;
import com.clusterra.pmbok.document.domain.service.template.TemplateAlreadyExistsException;
import com.clusterra.pmbok.document.domain.model.template.SectionTemplateNotFoundException;
import com.clusterra.pmbok.document.domain.service.template.TemplateDomainService;
import com.clusterra.pmbok.document.domain.service.template.TemplateNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Set;

/**
 * @author Denis Kuchugurov.
 *         14.07.2014.
 */
@Service
public class TemplateServiceImpl implements TemplateService {

    @Autowired
    private TemplateDomainService service;

    @Autowired
    private IdentityTracker identityTracker;


    @Transactional
    public Template createTemplate(Integer majorVersion, Integer minorVersion, String name) throws NotAuthenticatedException, TemplateAlreadyExistsException {
        return service.createTemplate(identityTracker.currentTenant(), majorVersion, minorVersion, name);
    }

    @Transactional
    public Template updateTemplateName(TemplateId templateId, String name) throws TemplateNotFoundException {
        return service.updateTemplateName(templateId, name);
    }

    @Transactional
    public void deleteTemplate(TemplateId templateId) throws TemplateNotFoundException {
        service.deleteTemplate(templateId);
    }

    @Transactional
    public SectionTemplate addHistorySection(TemplateId templateId, String name) throws TemplateNotFoundException, SectionTemplateAlreadyExistsException {
        return service.addHistorySection(templateId, name);
    }

    @Transactional
    public SectionTemplate addReferenceSection(TemplateId templateId, String name) throws TemplateNotFoundException, SectionTemplateAlreadyExistsException {
        return service.addReferenceSection(templateId, name);
    }

    @Transactional
    public SectionTemplate addTermSection(TemplateId templateId, String name) throws TemplateNotFoundException, SectionTemplateAlreadyExistsException {
        return service.addTermSection(templateId, name);
    }

    @Transactional
    public SectionTemplate addTextSection(TemplateId templateId, String name) throws TemplateNotFoundException {
        return service.addTextSection(templateId, name);
    }

    @Transactional
    public SectionTemplate addTitleSection(TemplateId templateId, String name) throws TemplateNotFoundException {
        return service.addTitleSection(templateId, name);
    }

    @Transactional
    public SectionTemplate addTocSection(TemplateId templateId, String name) throws TemplateNotFoundException {
        return service.addTocSection(templateId, name);
    }

    @Transactional
    public void removeSection(TemplateId templateId, SectionTemplateId sectionTemplateId) throws TemplateNotFoundException, SectionTemplateNotFoundException {
        service.removeSection(templateId, sectionTemplateId);
    }

    @Transactional
    public SectionTemplate updateSectionOrder(TemplateId templateId, SectionTemplateId sectionTemplateId, Integer orderIndex) throws SectionTemplateNotFoundException, TemplateNotFoundException {
        return service.updateSectionOrder(templateId, sectionTemplateId, orderIndex);
    }

    @Transactional
    public SectionTemplate updateSectionName(TemplateId templateId, SectionTemplateId sectionTemplateId, String name) throws SectionTemplateNotFoundException, TemplateNotFoundException {
        return service.updateSectionName(templateId, sectionTemplateId, name);
    }

    @Transactional
    public List<Template> findAllTemplates() throws NotAuthenticatedException {
        return service.findAllTemplates(identityTracker.currentTenant());
    }

    @Transactional
    public Page<Template> findBy(Pageable pageable, String searchBy) throws NotAuthenticatedException {
        return service.findBy(identityTracker.currentTenant(), pageable, searchBy);
    }

    @Transactional
    public List<SectionTemplate> findSectionTemplates(TemplateId templateId) throws TemplateNotFoundException {
        return service.findSectionTemplates(templateId);
    }

    @Transactional
    public SectionTemplate findSectionTemplateBy(SectionTemplateId sectionTemplateId) throws SectionTemplateNotFoundException {
        return service.findSectionTemplateBy(sectionTemplateId);
    }

    @Transactional
    public Set<SectionType> findSectionTypesUsedBy(TemplateId templateId) throws TemplateNotFoundException {
        return service.findSectionTypesUsedBy(templateId);
    }

    @Transactional
    public Template findBy(TemplateId templateId) throws TemplateNotFoundException {
        return service.findBy(templateId);
    }

}
