/*
 * Copyright (c) 2014.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * 	http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.clusterra.pmbok.document.domain.model.document.repo;

import com.clusterra.pmbok.document.domain.model.template.Template;
import com.clusterra.pmbok.document.domain.model.document.Document;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.util.List;
import java.util.Set;

/**
 * Created by dkuchugurov on 29.03.2014.
 */
public interface DocumentRepository extends PagingAndSortingRepository<Document, String>, JpaSpecificationExecutor<Document> {

    @Query("select d from Document d where d.projectVersionId = ?1")
    List<Document> findByProjectVersionId(String projectVersionId);

    @Query("select d from Document d where d.projectVersionId = ?1 and d.template = ?2")
    Document findBy(String projectVersionId, Template template);

    @Query("select d.template from Document d where d.projectVersionId = ?1")
    Set<Template> findUsedTemplates(String projectVersionId);

    @Query("select d from Document d where d.template = ?1")
    List<Document> findBy(Template template);

    @Query("select count(d) from Document d where d.template = ?1")
    int countBy(Template template);
}
