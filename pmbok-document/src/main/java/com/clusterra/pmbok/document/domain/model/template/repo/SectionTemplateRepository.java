/*
 * Copyright (c) 2014.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * 	http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.clusterra.pmbok.document.domain.model.template.repo;

import com.clusterra.pmbok.document.domain.model.template.section.SectionTemplate;
import com.clusterra.pmbok.document.domain.model.template.section.SectionType;
import com.clusterra.pmbok.document.domain.model.document.Document;
import com.clusterra.pmbok.document.domain.model.template.Template;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import java.util.List;
import java.util.Set;

/**
 * @author Denis Kuchugurov.
 *         11.07.2014.
 */
public interface SectionTemplateRepository<T extends SectionTemplate> extends CrudRepository<T, String> {

    @Query("select s from SectionTemplate s where s.template = ?1 order by s.orderIndex asc")
    List<T> findBy(Template template);

    @Query("select s from SectionTemplate s, Document d where s.template = d.template and d = ?1 order by s.orderIndex asc")
    List<T> findBy(Document document);

    @Query("select count(s) from SectionTemplate s where s.template = ?1")
    int countBy(Template template);

    @Query("select s from SectionTemplate s where s.template = ?1 and s.type = ?2")
    T findBy(Template template, SectionType sectionType);

    @Query("select s.type from SectionTemplate s where s.template = ?1")
    Set<SectionType> findSectionTypesUsedBy(Template template);
}
