/*
 * Copyright (c) 2014.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * 	http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.clusterra.pmbok.document.domain.model.document.section.repo;

import com.clusterra.pmbok.document.domain.model.document.section.PersistedSectionContent;
import com.clusterra.pmbok.document.domain.model.template.section.SectionTemplate;
import com.clusterra.pmbok.document.domain.model.document.Document;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

/**
 * Created by dkuchugurov on 02.04.2014.
 */
public interface PersistedSectionContentRepository<T extends PersistedSectionContent> extends CrudRepository<T, String> {

    @Query("select s from PersistedSectionContent s where s.document = ?1 and s.sectionTemplate = ?2")
    <S> S findBy(Document document, SectionTemplate sectionTemplate);

    @Modifying
    @Query("delete from PersistedSectionContent s where s.document = ?1")
    void deleteBy(Document document);
}
