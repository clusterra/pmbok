/*
 * Copyright (c) 2014.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * 	http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.clusterra.pmbok.document.domain.model.document.event;

import com.clusterra.pmbok.document.domain.model.document.DocumentId;
import com.clusterra.pmbok.document.domain.model.template.section.SectionTemplateId;
import com.clusterra.pmbok.project.domain.model.ProjectId;

/**
 * @author Denis Kuchugurov.
 *         01.08.2014.
 */
public class SectionUpdatedEvent extends DocumentUpdatedEvent {

    private final SectionTemplateId sectionTemplateId;

    public SectionUpdatedEvent(Object source, ProjectId projectId, SectionTemplateId sectionTemplateId, DocumentId documentId, String message) {
        super(source, projectId, documentId, message);
        this.sectionTemplateId = sectionTemplateId;
    }

    public SectionTemplateId getSectionTemplateId() {
        return sectionTemplateId;
    }
}
