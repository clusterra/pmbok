/*
 * Copyright (c) 2014.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * 	http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.clusterra.pmbok.document.domain.service.reference;

import com.clusterra.pmbok.document.domain.model.document.DocumentId;
import com.clusterra.pmbok.document.domain.model.reference.repo.ReferenceAssociationRepository;
import com.clusterra.pmbok.reference.domain.model.reference.Reference;
import com.clusterra.pmbok.reference.domain.model.reference.repo.ReferenceRepository;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.Validate;
import com.clusterra.iam.core.application.tenant.TenantId;
import com.clusterra.pmbok.document.domain.model.document.Document;
import com.clusterra.pmbok.document.domain.model.reference.ReferenceAssociation;
import com.clusterra.pmbok.document.domain.service.document.DocumentDomainService;
import com.clusterra.pmbok.document.domain.service.document.DocumentNotFoundException;
import com.clusterra.pmbok.reference.domain.model.reference.ReferenceId;
import com.clusterra.pmbok.reference.domain.service.ReferenceNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * Created by dkuchugurov on 27.06.2014.
 */
@Service
@Transactional(propagation = Propagation.MANDATORY)
public class ReferenceAssociationServiceImpl implements ReferenceAssociationService {

    @Autowired
    private DocumentDomainService documentDomainService;

    @Autowired
    private ReferenceRepository referenceRepository;

    @Autowired
    private ReferenceAssociationRepository referenceAssociationRepository;

    public void createAssociation(TenantId tenantId, DocumentId documentId, ReferenceId referenceId) throws ReferenceNotFoundException, DocumentNotFoundException {
        Validate.notNull(documentId);
        Validate.notNull(referenceId);

        Document document = documentDomainService.findBy(documentId);

        Reference reference = referenceRepository.findOne(referenceId.getId());
        if (reference == null) {
            throw new ReferenceNotFoundException(referenceId);
        }

        ReferenceAssociation association = referenceAssociationRepository.findBy(tenantId.getId(), referenceId.getId(), document);
        if (association == null) {
            referenceAssociationRepository.save(new ReferenceAssociation(tenantId, reference.getReferenceId(), document));
        }
    }


    public void deleteAssociation(TenantId tenantId, DocumentId documentId, ReferenceId referenceId) throws ReferenceNotFoundException, DocumentNotFoundException {
        Validate.notNull(documentId);
        Validate.notNull(referenceId);
        Document document = documentDomainService.findBy(documentId);

        Reference reference = referenceRepository.findOne(referenceId.getId());
        if (reference == null) {
            throw new ReferenceNotFoundException(referenceId);
        }
        ReferenceAssociation association = referenceAssociationRepository.findBy(tenantId.getId(), referenceId.getId(), document);
        if (association != null) {
            referenceAssociationRepository.delete(association);
        }
    }

    public void deleteAllAssociations(TenantId tenantId, DocumentId documentId) throws DocumentNotFoundException {
        Validate.notNull(documentId);
        Document document = documentDomainService.findBy(documentId);

        referenceAssociationRepository.deleteBy(tenantId.getId(), document);
    }

    public Page<Reference> findBy(TenantId tenantId, Pageable pageable, DocumentId documentId, String searchBy) throws DocumentNotFoundException {
        Validate.notNull(tenantId);
        Validate.notNull(documentId);
        Document document = documentDomainService.findBy(documentId);
        if (!StringUtils.isEmpty(searchBy)) {
            return referenceAssociationRepository.findBy(pageable, tenantId.getId(), document, "%" + searchBy + "%");
        }
        return referenceAssociationRepository.findBy(pageable, document);
    }

    public List<Reference> findBy(TenantId tenantId, DocumentId documentId) throws DocumentNotFoundException {
        Validate.notNull(tenantId);
        Document document = documentDomainService.findBy(documentId);
        return referenceAssociationRepository.findBy(tenantId.getId(), document);
    }
}
