/*
 * Copyright (c) 2014.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * 	http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.clusterra.pmbok.document.domain.model.document;

import com.clusterra.pmbok.project.domain.model.ProjectVersion;

/**
 * @author Denis Kuchugurov.
 *         10.07.2014.
 */
public class DocumentRevision {

    private final String revision;
    private final String documentName;
    private final String projectName;
    private final String avatarId;

    public DocumentRevision(ProjectVersion projectVersion, Document document, String avatarId) {
        this.avatarId = avatarId;
        this.revision = String.format("%d.%d.%s", projectVersion.getValue(), document.getRevision(), projectVersion.getLabel());
        this.documentName = document.getTemplate().getName();
        this.projectName = projectVersion.getProject().getName();
    }

    public String getRevision() {
        return revision;
    }

    public String getDocumentName() {
        return documentName;
    }

    public String getProjectName() {
        return projectName;
    }

    public String getAvatarId() {
        return avatarId;
    }
}
