/*
 * Copyright (c) 2014.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * 	http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.clusterra.pmbok.document.domain.service.reference;

import com.clusterra.iam.core.application.tenant.TenantId;
import com.clusterra.pmbok.document.domain.model.document.DocumentId;
import com.clusterra.pmbok.reference.domain.model.reference.Reference;
import com.clusterra.pmbok.reference.domain.model.reference.ReferenceId;
import com.clusterra.pmbok.reference.domain.service.ReferenceNotFoundException;
import com.clusterra.pmbok.document.domain.service.document.DocumentNotFoundException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;

/**
 * Created by dkuchugurov on 27.06.2014.
 */
public interface ReferenceAssociationService {

    void createAssociation(TenantId tenantId, DocumentId documentId, ReferenceId referenceId) throws ReferenceNotFoundException, DocumentNotFoundException;

    void deleteAssociation(TenantId tenantId, DocumentId documentId, ReferenceId referenceId) throws ReferenceNotFoundException, DocumentNotFoundException;

    void deleteAllAssociations(TenantId tenantId, DocumentId documentId) throws DocumentNotFoundException;

    Page<Reference> findBy(TenantId tenantId, Pageable pageable, DocumentId documentId, String searchBy) throws DocumentNotFoundException;

    List<Reference> findBy(TenantId tenantId, DocumentId documentId) throws DocumentNotFoundException;
}
