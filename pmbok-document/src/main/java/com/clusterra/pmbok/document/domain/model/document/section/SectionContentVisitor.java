/*
 * Copyright (c) 2014.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * 	http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.clusterra.pmbok.document.domain.model.document.section;

import com.clusterra.pmbok.document.domain.model.document.section.history.HistorySectionContent;
import com.clusterra.pmbok.document.domain.model.document.section.term.TermSectionContent;
import com.clusterra.pmbok.document.domain.model.document.section.title.TitleSectionContent;
import com.clusterra.pmbok.document.domain.model.document.section.reference.ReferenceSectionContent;
import com.clusterra.pmbok.document.domain.model.document.section.text.TextSectionContent;
import com.clusterra.pmbok.document.domain.model.document.section.toc.TocSectionContent;

/**
 * Created by dkuchugurov on 05.08.2014.
 */
public interface SectionContentVisitor<T> {

    T visit(TitleSectionContent content) throws Exception;

    T visit(HistorySectionContent content) throws Exception;

    T visit(ReferenceSectionContent content) throws Exception;

    T visit(TermSectionContent content) throws Exception;

    T visit(TextSectionContent content) throws Exception;

    T visit(TocSectionContent content) throws Exception;
}
