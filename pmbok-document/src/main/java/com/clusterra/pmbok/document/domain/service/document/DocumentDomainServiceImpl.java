/*
 * Copyright (c) 2014.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * 	http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.clusterra.pmbok.document.domain.service.document;

import com.clusterra.pmbok.document.domain.model.document.Document;
import com.clusterra.pmbok.document.domain.model.document.DocumentId;
import com.clusterra.pmbok.document.domain.model.document.DocumentRevision;
import com.clusterra.pmbok.document.domain.model.document.Status;
import com.clusterra.pmbok.document.domain.model.document.event.DocumentApprovedEvent;
import com.clusterra.pmbok.document.domain.model.document.event.DocumentCreatedEvent;
import com.clusterra.pmbok.document.domain.model.document.event.DocumentDeletedEvent;
import com.clusterra.pmbok.document.domain.model.document.event.DocumentEditingEvent;
import com.clusterra.pmbok.document.domain.model.document.event.DocumentPublishedEvent;
import com.clusterra.pmbok.document.domain.model.document.event.TextSectionUpdatedEvent;
import com.clusterra.pmbok.document.domain.model.document.repo.DocumentProjectVersionSpecification;
import com.clusterra.pmbok.document.domain.model.document.repo.DocumentProjectVersionsSpecification;
import com.clusterra.pmbok.document.domain.model.document.repo.DocumentRepository;
import com.clusterra.pmbok.document.domain.model.document.repo.DocumentSearchBySpecification;
import com.clusterra.pmbok.document.domain.model.document.repo.DocumentTenantSpecification;
import com.clusterra.pmbok.document.domain.model.document.section.SectionContent;
import com.clusterra.pmbok.document.domain.model.document.section.repo.PersistedSectionContentRepository;
import com.clusterra.pmbok.document.domain.model.document.section.text.PersistedTextSectionContent;
import com.clusterra.pmbok.document.domain.model.template.SectionTemplateNotFoundException;
import com.clusterra.pmbok.document.domain.model.template.Template;
import com.clusterra.pmbok.document.domain.model.template.TemplateId;
import com.clusterra.pmbok.document.domain.model.template.repo.SectionTemplateRepository;
import com.clusterra.pmbok.document.domain.model.template.section.SectionTemplate;
import com.clusterra.pmbok.document.domain.model.template.section.SectionTemplateId;
import com.clusterra.pmbok.document.domain.service.template.TemplateDomainService;
import com.clusterra.pmbok.project.domain.model.ProjectId;
import com.clusterra.pmbok.project.domain.model.ProjectVersion;
import com.clusterra.pmbok.project.domain.model.ProjectVersionId;
import com.clusterra.pmbok.project.domain.model.repo.ProjectVersionRepository;
import com.clusterra.pmbok.project.domain.service.ProjectNotFoundException;
import com.clusterra.pmbok.project.domain.service.ProjectVersionNotFoundException;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.Validate;
import com.clusterra.iam.core.application.tenant.TenantId;
import com.clusterra.iam.core.application.tenant.TenantNotFoundException;
import com.clusterra.iam.core.application.tenant.TenantQueryService;
import com.clusterra.iam.core.domain.model.tenant.Tenant;
import com.clusterra.pmbok.document.domain.model.document.event.DocumentDeletingEvent;
import com.clusterra.pmbok.document.domain.model.document.section.PersistedSectionContent;
import com.clusterra.pmbok.document.domain.service.template.TemplateNotFoundException;
import com.clusterra.pmbok.project.domain.service.ProjectDomainQueryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.jpa.domain.Specifications;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

/**
 * Created by dkuchugurov on 02.04.2014.
 */
@Service
@Transactional(propagation = Propagation.MANDATORY)
public class DocumentDomainServiceImpl implements DocumentDomainService {

    @Autowired
    private DocumentRepository documentRepository;

    @Autowired
    private ProjectVersionRepository projectVersionRepository;

    @Autowired
    private TemplateDomainService templateDomainService;

    @Autowired
    private ApplicationEventPublisher publisher;

    @Autowired
    private ProjectDomainQueryService projectDomainQueryService;

    @Autowired
    private SectionTemplateRepository<SectionTemplate> sectionTemplateRepository;

    @Autowired
    private SectionContentBuilder sectionContentBuilder;

    @Autowired
    private PersistedSectionContentRepository<PersistedSectionContent> persistedSectionContentRepository;

    @Autowired
    private TenantQueryService tenantQueryService;

    public Document create(TenantId tenantId, ProjectVersionId projectVersionId, TemplateId templateId) throws DocumentAlreadyExistsException, ProjectVersionNotFoundException, TemplateNotFoundException {
        ProjectVersion projectVersion = projectVersionRepository.findOne(projectVersionId.getId());
        if (projectVersion == null) {
            throw new ProjectVersionNotFoundException(projectVersionId);
        }

        Template template = templateDomainService.findBy(templateId);

        Document existing = documentRepository.findBy(projectVersionId.getId(), template);
        if (existing != null) {
            throw new DocumentAlreadyExistsException(projectVersionId, templateId);
        }
        Document document = new Document(template, tenantId, projectVersion.getProject().getProjectId(), projectVersionId);
        documentRepository.save(document);
        publisher.publishEvent(new DocumentCreatedEvent(this, tenantId, document.getProjectId(), document.getDocumentId(), templateId, document.getCreatedByUserId()));
        return document;
    }

    public void deleteBy(DocumentId documentId) throws DocumentNotFoundException {
        Document document = getDocument(documentId);
        publisher.publishEvent(new DocumentDeletingEvent(this, documentId));

        documentRepository.delete(document);
        publisher.publishEvent(new DocumentDeletedEvent(this, document.getProjectId(), documentId));
    }

    public void deleteBy(ProjectVersionId projectVersionId) throws DocumentNotFoundException {
        List<Document> documents = documentRepository.findByProjectVersionId(projectVersionId.getId());
        for (Document document : documents) {
            deleteBy(document.getDocumentId());
        }
    }

    public Document publish(DocumentId documentId, String message) throws DocumentNotFoundException {
        Validate.notEmpty(message, "message is empty");
        Document document = getDocument(documentId);
        document.publish();
        publisher.publishEvent(new DocumentPublishedEvent(this, document.getDocumentId(), message));
        return document;
    }

    public Document approve(DocumentId documentId, String message) throws DocumentNotFoundException {
        Document document = getDocument(documentId);

        if (!document.getStatus().equals(Status.PUBLISHED)) {
            document.publish();
            publisher.publishEvent(new DocumentPublishedEvent(this, document.getDocumentId(), message));
        }

        document.approve();
        publisher.publishEvent(new DocumentApprovedEvent(this, document.getDocumentId(), message));
        return document;
    }

    public Document edit(DocumentId documentId) throws DocumentNotFoundException, DocumentNotEditableException {
        Document document = getDocument(documentId);

        if (document.getStatus().equals(Status.APPROVED)) {
            throw new DocumentNotEditableException(document);
        }

        document.edit();
        publisher.publishEvent(new DocumentEditingEvent(this, document));
        return document;
    }

    private Document getDocument(DocumentId documentId) throws DocumentNotFoundException {
        Document document = documentRepository.findOne(documentId.getId());
        if (document == null) {
            throw new DocumentNotFoundException(documentId);
        }
        return document;
    }

    public Document findBy(DocumentId documentId) throws DocumentNotFoundException {
        Validate.notNull(documentId);
        Document document = documentRepository.findOne(documentId.getId());
        if (document == null) {
            throw new DocumentNotFoundException(documentId);
        }
        return document;
    }

    public List<Document> findBy(TemplateId documentId) throws TemplateNotFoundException {
        Template template = templateDomainService.findBy(documentId);
        return documentRepository.findBy(template);
    }

    public Page<Document> findBy(TenantId tenantId, Pageable pageable, String searchBy) {
        Validate.notNull(tenantId);
        Specification<Document> tenantSpec = new DocumentTenantSpecification(tenantId.getId());
        Specifications<Document> specifications = Specifications.where(tenantSpec);

        if (!StringUtils.isEmpty(searchBy)) {
            specifications = specifications.and(new DocumentSearchBySpecification(searchBy));
        }
        return documentRepository.findAll(specifications, pageable);
    }

    public Page<Document> findBy(TenantId tenantId, Pageable pageable, ProjectId projectId, String searchBy) throws ProjectNotFoundException {
        Validate.notNull(tenantId);
        Validate.notNull(projectId);
        Specification<Document> tenantSpec = new DocumentTenantSpecification(tenantId.getId());
        Specifications<Document> specifications = Specifications.where(tenantSpec);

        List<ProjectVersion> projectVersions = projectDomainQueryService.findAllVersionsBy(projectId);

        specifications = specifications.and(new DocumentProjectVersionsSpecification(projectVersions));
        if (!StringUtils.isEmpty(searchBy)) {
            specifications = specifications.and(new DocumentSearchBySpecification(searchBy));
        }
        return documentRepository.findAll(specifications, pageable);
    }

    public Page<Document> findBy(TenantId tenantId, Pageable pageable, ProjectVersionId projectVersionId, String searchBy) {
        Validate.notNull(tenantId);
        Specification<Document> tenantSpec = new DocumentTenantSpecification(tenantId.getId());
        Specifications<Document> specifications = Specifications.where(tenantSpec);

        specifications = specifications.and(new DocumentProjectVersionSpecification(projectVersionId));

        if (!StringUtils.isEmpty(searchBy)) {
            specifications = specifications.and(new DocumentSearchBySpecification(searchBy));
        }
        return documentRepository.findAll(specifications, pageable);
    }

    public Set<Template> findUsedTemplates(ProjectVersionId projectVersionId) {
        return documentRepository.findUsedTemplates(projectVersionId.getId());
    }

    public DocumentRevision getRevision(DocumentId documentId) throws DocumentNotFoundException {
        try {
            Document document = findBy(documentId);
            ProjectVersion projectVersion = projectDomainQueryService.findVersionBy(document.getProjectVersionId());
            Tenant tenant = tenantQueryService.find(document.getTenantId());
            return new DocumentRevision(projectVersion, document, tenant.getAvatarId());
        } catch (ProjectVersionNotFoundException | TenantNotFoundException e) {
            throw new IllegalStateException(e);
        }
    }

    public List<SectionContent> findSectionContentsBy(TenantId tenantId, DocumentId documentId) throws DocumentNotFoundException, TemplateNotFoundException {
        Document document = findBy(documentId);
        List<SectionTemplate> sectionTemplates = sectionTemplateRepository.findBy(document);

        List<SectionContent> result = new ArrayList<>(sectionTemplates.size());
        for (SectionTemplate sectionTemplate : sectionTemplates) {
            result.add(sectionTemplate.getSectionContent(document, sectionContentBuilder));
        }
        return result;
    }

    public PersistedTextSectionContent updateTextSection(DocumentId documentId, SectionTemplateId sectionTemplateId, String text) throws DocumentNotFoundException, SectionTemplateNotFoundException {
        Document document = findBy(documentId);
        SectionTemplate sectionTemplate = templateDomainService.findSectionTemplateBy(sectionTemplateId);
        PersistedTextSectionContent section = persistedSectionContentRepository.findBy(document, sectionTemplate);
        if (section == null) {
            section = new PersistedTextSectionContent(document, sectionTemplate);
        }
        section.setText(text);
        String message = String.format("Section %s updated", sectionTemplate.getName());
        publisher.publishEvent(new TextSectionUpdatedEvent(this, document.getProjectId(), documentId, sectionTemplateId, message, text));
        return persistedSectionContentRepository.save(section);
    }

}
