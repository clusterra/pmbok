/*
 * Copyright (c) 2014.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * 	http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.clusterra.pmbok.document.domain.model.template;

import com.clusterra.pmbok.document.domain.model.template.section.SectionType;

/**
 * @author Denis Kuchugurov.
 *         14.07.2014.
 */
public class SectionTemplateAlreadyExistsException extends Exception {
    public SectionTemplateAlreadyExistsException(TemplateId templateId, SectionType sectionType) {
        super(String.format("document template %s already has section %s", templateId, sectionType));
    }
}
