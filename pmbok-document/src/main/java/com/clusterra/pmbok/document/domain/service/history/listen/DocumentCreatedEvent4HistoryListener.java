/*
 * Copyright (c) 2014.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * 	http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.clusterra.pmbok.document.domain.service.history.listen;

import com.clusterra.pmbok.document.domain.model.document.event.DocumentCreatedEvent;
import com.clusterra.pmbok.document.domain.service.document.DocumentNotFoundException;
import com.clusterra.pmbok.document.domain.service.history.HistoryEntryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationListener;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

/**
 * Created by dkuchugurov on 26.03.2014.
 */
@Component
public class DocumentCreatedEvent4HistoryListener implements ApplicationListener<DocumentCreatedEvent> {


    @Autowired
    private HistoryEntryService service;

    @Transactional(propagation = Propagation.MANDATORY)
    public void onApplicationEvent(DocumentCreatedEvent event) {
        try {
            service.addHistoryEntry(event.getDocumentId(), event.getMessage());
        } catch (DocumentNotFoundException e) {
            throw new RuntimeException(e);
        }
    }
}
