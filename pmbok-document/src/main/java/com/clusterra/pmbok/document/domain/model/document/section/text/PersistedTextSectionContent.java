/*
 * Copyright (c) 2014.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * 	http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.clusterra.pmbok.document.domain.model.document.section.text;

import com.clusterra.pmbok.document.domain.model.document.DocumentId;
import com.clusterra.pmbok.document.domain.model.document.section.PersistedSectionContent;
import com.clusterra.pmbok.document.domain.model.document.section.SectionContentVisitor;
import com.clusterra.pmbok.document.domain.model.template.section.SectionTemplate;
import com.clusterra.pmbok.document.domain.model.document.Document;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * Created by dkuchugurov on 04.04.2014.
 */
@Entity
@Table(name = "pmb_section_text")
public class PersistedTextSectionContent extends PersistedSectionContent implements TextSectionContent {

    @Basic
    @Column(length = 3000)
    private String text;

    PersistedTextSectionContent() {
    }

    @Override
    public <T> T accept(SectionContentVisitor<T> visitor) throws Exception {
        return visitor.visit(this);
    }

    @Override
    public DocumentId getDocumentId() {
        return getDocument().getDocumentId();
    }

    public PersistedTextSectionContent(Document document, SectionTemplate sectionTemplate) {
        super(document, sectionTemplate);
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }
}
