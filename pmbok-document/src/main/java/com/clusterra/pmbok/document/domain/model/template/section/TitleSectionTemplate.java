/*
 * Copyright (c) 2014.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * 	http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.clusterra.pmbok.document.domain.model.template.section;

import com.clusterra.pmbok.document.domain.model.document.Document;
import com.clusterra.pmbok.document.domain.model.document.section.SectionContent;
import com.clusterra.pmbok.document.domain.model.template.Template;
import com.clusterra.pmbok.document.domain.service.document.SectionContentBuilder;

import javax.persistence.Entity;

/**
 * Created by dkuchugurov on 04.08.2014.
 */
@Entity
public class TitleSectionTemplate extends SectionTemplate {

    TitleSectionTemplate() {
    }

    public TitleSectionTemplate(Template template, String name, Integer orderIndex) {
        super(template, SectionType.SECTION_TITLE, name, orderIndex);
    }

    public SectionContent getSectionContent(Document document, SectionContentBuilder builder) {
        return builder.buildTitleSectionContent(document, this);
    }
}
