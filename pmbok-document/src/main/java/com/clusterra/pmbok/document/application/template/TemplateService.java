/*
 * Copyright (c) 2014.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * 	http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.clusterra.pmbok.document.application.template;

import com.clusterra.iam.core.application.tracker.NotAuthenticatedException;
import com.clusterra.pmbok.document.domain.model.template.SectionTemplateAlreadyExistsException;
import com.clusterra.pmbok.document.domain.model.template.Template;
import com.clusterra.pmbok.document.domain.model.template.TemplateId;
import com.clusterra.pmbok.document.domain.model.template.section.SectionTemplate;
import com.clusterra.pmbok.document.domain.model.template.section.SectionTemplateId;
import com.clusterra.pmbok.document.domain.model.template.section.SectionType;
import com.clusterra.pmbok.document.domain.service.template.TemplateAlreadyExistsException;
import com.clusterra.pmbok.document.domain.model.template.SectionTemplateNotFoundException;
import com.clusterra.pmbok.document.domain.service.template.TemplateNotFoundException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;
import java.util.Set;

/**
 * @author Denis Kuchugurov.
 *         14.07.2014.
 */
public interface TemplateService {

    Template createTemplate(Integer majorVersion, Integer minorVersion, String name) throws NotAuthenticatedException, TemplateAlreadyExistsException;

    Template updateTemplateName(TemplateId templateId, String name) throws TemplateNotFoundException;

    void deleteTemplate(TemplateId templateId) throws TemplateNotFoundException;

    SectionTemplate addHistorySection(TemplateId templateId, String name) throws TemplateNotFoundException, SectionTemplateAlreadyExistsException;

    SectionTemplate addReferenceSection(TemplateId templateId, String name) throws TemplateNotFoundException, SectionTemplateAlreadyExistsException;

    SectionTemplate addTermSection(TemplateId templateId, String name) throws TemplateNotFoundException, SectionTemplateAlreadyExistsException;

    SectionTemplate addTextSection(TemplateId templateId, String name) throws TemplateNotFoundException;

    SectionTemplate addTitleSection(TemplateId templateId, String name) throws TemplateNotFoundException;

    SectionTemplate addTocSection(TemplateId templateId, String name) throws TemplateNotFoundException;

    void removeSection(TemplateId templateId, SectionTemplateId sectionTemplateId) throws TemplateNotFoundException, SectionTemplateNotFoundException;

    SectionTemplate updateSectionOrder(TemplateId templateId, SectionTemplateId sectionTemplateId, Integer orderIndex) throws SectionTemplateNotFoundException, TemplateNotFoundException;

    SectionTemplate updateSectionName(TemplateId templateId, SectionTemplateId sectionTemplateId, String name) throws SectionTemplateNotFoundException, TemplateNotFoundException;

    List<Template> findAllTemplates() throws NotAuthenticatedException;

    Page<Template> findBy(Pageable pageable, String searchBy) throws NotAuthenticatedException;

    List<SectionTemplate> findSectionTemplates(TemplateId templateId) throws TemplateNotFoundException;

    SectionTemplate findSectionTemplateBy(SectionTemplateId sectionTemplateId) throws SectionTemplateNotFoundException;

    Set<SectionType> findSectionTypesUsedBy(TemplateId templateId) throws TemplateNotFoundException;

    Template findBy(TemplateId templateId) throws TemplateNotFoundException;
}
