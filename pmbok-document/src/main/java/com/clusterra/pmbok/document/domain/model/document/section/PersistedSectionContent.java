/*
 * Copyright (c) 2014.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * 	http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.clusterra.pmbok.document.domain.model.document.section;

import com.clusterra.pmbok.document.domain.model.template.section.SectionTemplate;
import org.apache.commons.lang3.Validate;
import com.clusterra.pmbok.document.domain.model.document.Document;
import org.hibernate.annotations.GenericGenerator;
import org.springframework.data.annotation.CreatedBy;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedBy;
import org.springframework.data.annotation.LastModifiedDate;

import javax.persistence.Basic;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import java.util.Date;

/**
 * Created by dkuchugurov on 02.04.2014.
 */
@Entity
@Inheritance(strategy = InheritanceType.JOINED)
@Table(name = "pmb_section")
public abstract class PersistedSectionContent {

    @Id
    @GeneratedValue(generator = "base64")
    @GenericGenerator(name = "base64", strategy = "com.clusterra.hibernate.base64.Base64IdGenerator")
    private String id;

    @Basic
    @CreatedDate
    private Date createdDate;

    @Basic
    @CreatedBy
    private String createdByUserId;

    @Basic
    @LastModifiedDate
    private Date modifiedDate;

    @Basic
    @LastModifiedBy
    private String modifiedByUserId;

    @ManyToOne
    private Document document;

    @ManyToOne
    private SectionTemplate sectionTemplate;

    protected PersistedSectionContent() {
    }

    public PersistedSectionContent(Document document, SectionTemplate sectionTemplate) {
        Validate.notNull(document);
        Validate.notNull(sectionTemplate);
        this.document = document;
        this.sectionTemplate = sectionTemplate;
    }

    public Document getDocument() {
        return document;
    }

    public SectionTemplate getSectionTemplate() {
        return sectionTemplate;
    }

    public Date getCreatedDate() {
        return createdDate;
    }

    public String getCreatedByUserId() {
        return createdByUserId;
    }

    public Date getModifiedDate() {
        return modifiedDate;
    }

    public String getModifiedByUserId() {
        return modifiedByUserId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        PersistedSectionContent persistedSectionContent = (PersistedSectionContent) o;

        if (id != null ? !id.equals(persistedSectionContent.id) : persistedSectionContent.id != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        return id != null ? id.hashCode() : 0;
    }
}
