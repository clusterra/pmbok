/*
 * Copyright (c) 2014.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * 	http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.clusterra.pmbok.document.domain.model.template;

import com.clusterra.iam.core.application.tenant.TenantId;
import com.clusterra.pmbok.document.domain.model.document.Version;
import com.clusterra.pmbok.document.domain.model.template.repo.SectionTemplateRepository;
import com.clusterra.pmbok.document.domain.model.template.section.HistorySectionTemplate;
import com.clusterra.pmbok.document.domain.model.template.section.ReferenceSectionTemplate;
import com.clusterra.pmbok.document.domain.model.template.section.SectionTemplate;
import com.clusterra.pmbok.document.domain.model.template.section.SectionTemplateId;
import com.clusterra.pmbok.document.domain.model.template.section.SectionType;
import com.clusterra.pmbok.document.domain.model.template.section.TermSectionTemplate;
import com.clusterra.pmbok.document.domain.model.template.section.TextSectionTemplate;
import com.clusterra.pmbok.document.domain.model.template.section.TitleSectionTemplate;
import com.clusterra.pmbok.document.domain.model.template.section.TocSectionTemplate;
import org.apache.commons.lang3.Validate;
import org.hibernate.annotations.GenericGenerator;
import org.springframework.data.annotation.CreatedBy;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedBy;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import java.util.Date;
import java.util.List;

/**
 * @author Denis Kuchugurov.
 *         11.07.2014.
 */
@Entity
@EntityListeners({AuditingEntityListener.class})
@Table(name = "pmb_doc_template")
public class Template {

    @Id
    @GeneratedValue(generator = "base64")
    @GenericGenerator(name = "base64", strategy = "com.clusterra.hibernate.base64.Base64IdGenerator")
    private String id;

    @Basic
    @Column(nullable = false)
    private String tenantId;

    @Basic
    @CreatedDate
    private Date createdDate;

    @Basic
    @CreatedBy
    private String createdByUserId;

    @Basic
    @LastModifiedDate
    private Date modifiedDate;

    @Basic
    @LastModifiedBy
    private String modifiedByUserId;

    @Embedded
    private Version version;

    @Basic
    private String name;

    @Basic
    @Enumerated(EnumType.STRING)
    @Column(nullable = false)
    private TemplateStatus status;

    @Basic
    private int touchCounter;

    Template() {
    }

    public Template(TenantId tenantId, Version version, String name) {
        Validate.notNull(tenantId);
        Validate.notNull(version);
        this.tenantId = tenantId.getId();
        this.version = version;
        this.touchCounter = 0;
        this.status = TemplateStatus.NEW;
        setName(name);
    }

    public void touch() {
        touchCounter++;
        status = TemplateStatus.EDITING;
    }

    public void markReady() {
        status = TemplateStatus.READY;
    }

    public TemplateStatus getStatus() {
        return status;
    }

    public SectionTemplate addHistorySection(String name, SectionTemplateRepository<SectionTemplate> repository) throws SectionTemplateAlreadyExistsException {
        checkSectionNotExists(SectionType.SECTION_HISTORY, repository);
        int count = calculateOrderIndex(repository);
        HistorySectionTemplate sectionTemplate = new HistorySectionTemplate(this, name, count);
        return repository.save(sectionTemplate);
    }

    public SectionTemplate addTermSection(String name, SectionTemplateRepository<SectionTemplate> repository) throws SectionTemplateAlreadyExistsException {
        checkSectionNotExists(SectionType.SECTION_TERM, repository);
        int count = calculateOrderIndex(repository);
        TermSectionTemplate sectionTemplate = new TermSectionTemplate(this, name, count);
        return repository.save(sectionTemplate);
    }

    public ReferenceSectionTemplate addReferenceSection(String name, SectionTemplateRepository<SectionTemplate> repository) throws SectionTemplateAlreadyExistsException {
        checkSectionNotExists(SectionType.SECTION_REFERENCE, repository);
        int count = calculateOrderIndex(repository);
        ReferenceSectionTemplate sectionTemplate = new ReferenceSectionTemplate(this, name, count);
        return repository.save(sectionTemplate);
    }

    private void checkSectionNotExists(SectionType type, SectionTemplateRepository<SectionTemplate> repository) throws SectionTemplateAlreadyExistsException {
        SectionTemplate section = repository.findBy(this, type);
        if (section != null) {
            throw new SectionTemplateAlreadyExistsException(this.getTemplateId(), type);
        }
    }

    private int calculateOrderIndex(SectionTemplateRepository repository) {
        return repository.countBy(this);
    }

    public TextSectionTemplate addTextSection(String name, SectionTemplateRepository<SectionTemplate> repository) {
        int count = calculateOrderIndex(repository);
        TextSectionTemplate sectionTemplate = new TextSectionTemplate(this, name, count);
        repository.save(sectionTemplate);
        return sectionTemplate;
    }

    public SectionTemplate addTitleSection(String name, SectionTemplateRepository<SectionTemplate> repository) {
        int count = calculateOrderIndex(repository);
        TitleSectionTemplate sectionTemplate = new TitleSectionTemplate(this, name, count);
        repository.save(sectionTemplate);
        return sectionTemplate;
    }


    public SectionTemplate addTocSection(String name, SectionTemplateRepository<SectionTemplate> repository) {
        int count = calculateOrderIndex(repository);
        TocSectionTemplate sectionTemplate = new TocSectionTemplate(this, name, count);
        repository.save(sectionTemplate);
        return sectionTemplate;
    }

    public void removeAllSections(SectionTemplateRepository<SectionTemplate> repository) {
        List<SectionTemplate> sectionTemplates = repository.findBy(this);
        repository.delete(sectionTemplates);
    }

    public void removeSection(SectionTemplateId sectionTemplateId, SectionTemplateRepository<SectionTemplate> repository) throws SectionTemplateNotFoundException {
        SectionTemplate template = findSectionTemplate(sectionTemplateId, repository);
        repository.delete(template);
    }

    private SectionTemplate findSectionTemplate(SectionTemplateId sectionTemplateId, SectionTemplateRepository<SectionTemplate> repository) throws SectionTemplateNotFoundException {
        Validate.notNull(sectionTemplateId);
        SectionTemplate sectionTemplate = repository.findOne(sectionTemplateId.getId());
        if (sectionTemplate == null) {
            throw new SectionTemplateNotFoundException(sectionTemplateId);
        }
        return sectionTemplate;
    }

    public SectionTemplate setOrder(SectionTemplateId sectionTemplateId, Integer orderIndex, SectionTemplateRepository<SectionTemplate> repository) throws SectionTemplateNotFoundException {
        SectionTemplate sectionTemplate = findSectionTemplate(sectionTemplateId, repository);
        Integer count = calculateOrderIndex(repository);
        Validate.inclusiveBetween(Integer.valueOf(0), count, orderIndex);

        if (sectionTemplate.getOrderIndex().equals(orderIndex)) {
            return sectionTemplate;
        }

        int newIndexAdjustedWithOrder;
        if (sectionTemplate.getOrderIndex() >= orderIndex) {
            newIndexAdjustedWithOrder = orderIndex - 1;
        } else {
            newIndexAdjustedWithOrder = orderIndex + 1;
        }
        sectionTemplate.setOrderIndex(newIndexAdjustedWithOrder);

        List<SectionTemplate> sections = repository.findBy(this);

        for (SectionTemplate template : sections) {
            int i = sections.indexOf(template);
            template.setOrderIndex(i);
            repository.save(template);
        }
        return sectionTemplate;
    }

    public TemplateId getTemplateId() {
        return new TemplateId(id);
    }

    public Date getCreatedDate() {
        return createdDate;
    }

    public String getCreatedByUserId() {
        return createdByUserId;
    }

    public Date getModifiedDate() {
        return modifiedDate;
    }

    public String getModifiedByUserId() {
        return modifiedByUserId;
    }

    public Version getVersion() {
        return version;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        Validate.notNull(name);
        this.name = name;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Template that = (Template) o;

        if (id != null ? !id.equals(that.id) : that.id != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        return id != null ? id.hashCode() : 0;
    }

    @Override
    public String toString() {
        return "Template{" +
                "id='" + id + '\'' +
                ", tenantId='" + tenantId + '\'' +
                ", createdDate=" + createdDate +
                ", createdByUserId='" + createdByUserId + '\'' +
                ", modifiedDate=" + modifiedDate +
                ", modifiedByUserId='" + modifiedByUserId + '\'' +
                ", version=" + version +
                ", name='" + name + '\'' +
                ", status=" + status +
                ", touchCounter=" + touchCounter +
                '}';
    }
}
