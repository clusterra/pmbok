/*
 * Copyright (c) 2014.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * 	http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.clusterra.pmbok.document.domain.service.term;

import com.clusterra.pmbok.document.domain.model.document.Document;
import com.clusterra.pmbok.document.domain.model.document.DocumentId;
import com.clusterra.pmbok.document.domain.model.term.TermAssociation;
import com.clusterra.pmbok.document.domain.model.term.repo.TermAssociationRepository;
import com.clusterra.pmbok.term.domain.model.term.Term;
import com.clusterra.pmbok.term.domain.model.term.TermId;
import com.clusterra.pmbok.term.domain.model.term.repo.TermRepository;
import com.clusterra.pmbok.term.domain.service.TermNotFoundException;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.Validate;
import com.clusterra.iam.core.application.tenant.TenantId;
import com.clusterra.pmbok.document.domain.service.document.DocumentDomainService;
import com.clusterra.pmbok.document.domain.service.document.DocumentNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * Created by dkuchugurov on 27.06.2014.
 */
@Service
@Transactional(propagation = Propagation.MANDATORY)
public class TermAssociationServiceImpl implements TermAssociationService {

    @Autowired
    private TermRepository termRepository;

    @Autowired
    private TermAssociationRepository termAssociationRepository;

    @Autowired
    private DocumentDomainService documentDomainService;

    public void createAssociation(TenantId tenantId, DocumentId documentId, TermId termId) throws DocumentNotFoundException, TermNotFoundException {
        Validate.notNull(documentId);
        Validate.notNull(termId);
        Document document = documentDomainService.findBy(documentId);

        Term term = termRepository.findOne(termId.getId());
        if (term == null) {
            throw new TermNotFoundException(termId);
        }
        TermAssociation association = termAssociationRepository.findBy(tenantId.getId(), termId.getId(), document);
        if (association == null) {
            termAssociationRepository.save(new TermAssociation(tenantId, termId.getId(), document));
        }
    }

    public void deleteAssociation(TenantId tenantId, DocumentId documentId, TermId termId) throws DocumentNotFoundException, TermNotFoundException {
        Validate.notNull(documentId);
        Validate.notNull(termId);
        Document document = documentDomainService.findBy(documentId);


        Term term = termRepository.findOne(termId.getId());
        if (term == null) {
            throw new TermNotFoundException(termId);
        }

        TermAssociation association = termAssociationRepository.findBy(tenantId.getId(), termId.getId(), document);
        if (association != null) {
            termAssociationRepository.delete(association);
        }
    }

    public void deleteAllAssociations(TenantId tenantId, DocumentId documentId) throws DocumentNotFoundException {
        Validate.notNull(documentId);
        Document document = documentDomainService.findBy(documentId);

        termAssociationRepository.deleteBy(tenantId.getId(), document);
    }

    public Page<Term> findBy(TenantId tenantId, Pageable pageable, DocumentId documentId, String searchBy) throws DocumentNotFoundException {
        Validate.notNull(tenantId);
        Document document = documentDomainService.findBy(documentId);

        if (!StringUtils.isEmpty(searchBy)) {
            return termAssociationRepository.findBy(pageable, tenantId.getId(), document, "%" + searchBy + "%");
        }
        return termAssociationRepository.findBy(pageable, document);
    }

    public List<Term> findBy(TenantId tenantId, DocumentId documentId) throws DocumentNotFoundException {
        Validate.notNull(tenantId);
        Document document = documentDomainService.findBy(documentId);
        return termAssociationRepository.findBy(tenantId.getId(), document);
    }
}
