/*
 * Copyright (c) 2014.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * 	http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

// Created by Liventsev Andrey. 09.02.14 14:49

package com.clusterra.pmbok.document.domain.model.term.repo;

import com.clusterra.pmbok.document.domain.model.document.Document;
import com.clusterra.pmbok.document.domain.model.term.TermAssociation;
import com.clusterra.pmbok.term.domain.model.term.Term;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.util.List;

public interface TermAssociationRepository extends PagingAndSortingRepository<TermAssociation, String>, JpaSpecificationExecutor<TermAssociation> {


    @Query("select t from Term t, TermAssociation a where t.id = a.termId and a.document = ?1")
    Page<Term> findBy(Pageable pageable, Document document);

    @Query("select t from Term t, TermAssociation a where t.id = a.termId and a.tenantId = ?1 and a.document = ?2 and (lower(t.name) like lower(?3) or lower(t.description) like lower(?3))")
    Page<Term> findBy(Pageable pageable, String tenantId, Document document, String searchBy);

    @Query("select t from Term t, TermAssociation a where t.id = a.termId and a.tenantId = ?1 and a.document = ?2 ")
    List<Term> findBy(String tenantId, Document document);

    @Query("select a from TermAssociation a where a.tenantId = ?1 and a.termId = ?2 and a.document = ?3")
    TermAssociation findBy(String tenantId, String termId, Document document);

    @Modifying
    @Query("delete from TermAssociation a where a.tenantId = ?1 and a.document = ?2")
    void deleteBy(String tenantId, Document document);
}
