/*
 * Copyright (c) 2014.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * 	http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.clusterra.pmbok.document.domain.service.document;

import com.clusterra.pmbok.document.domain.model.document.DocumentRevision;
import com.clusterra.pmbok.document.domain.model.document.section.history.HistorySectionContent;
import com.clusterra.pmbok.document.domain.model.document.section.reference.ReferenceSectionContent;
import com.clusterra.pmbok.document.domain.model.document.section.repo.PersistedSectionContentRepository;
import com.clusterra.pmbok.document.domain.model.document.section.term.TermSectionContent;
import com.clusterra.pmbok.document.domain.model.document.section.text.TextSectionContent;
import com.clusterra.pmbok.document.domain.model.document.section.title.TitleSectionContent;
import com.clusterra.pmbok.document.domain.model.history.HistoryEntry;
import com.clusterra.pmbok.document.domain.model.reference.repo.ReferenceAssociationRepository;
import com.clusterra.pmbok.document.domain.model.template.repo.SectionTemplateRepository;
import com.clusterra.pmbok.document.domain.model.template.section.SectionTemplate;
import com.clusterra.pmbok.project.domain.service.ProjectNotFoundException;
import com.clusterra.pmbok.reference.domain.model.reference.Reference;
import com.clusterra.pmbok.term.domain.model.term.Term;
import org.apache.commons.lang3.Validate;
import com.clusterra.iam.core.application.tenant.TenantNotFoundException;
import com.clusterra.iam.core.application.tenant.TenantQueryService;
import com.clusterra.iam.core.domain.model.tenant.Tenant;
import com.clusterra.pmbok.document.domain.model.document.Document;
import com.clusterra.pmbok.document.domain.model.document.section.PersistedSectionContent;
import com.clusterra.pmbok.document.domain.model.document.section.text.EmptyTextSectionContent;
import com.clusterra.pmbok.document.domain.model.document.section.text.PersistedTextSectionContent;
import com.clusterra.pmbok.document.domain.model.document.section.toc.TocSectionContent;
import com.clusterra.pmbok.document.domain.model.history.repo.HistoryEntryRepository;
import com.clusterra.pmbok.document.domain.model.term.repo.TermAssociationRepository;
import com.clusterra.pmbok.project.domain.model.Project;
import com.clusterra.pmbok.project.domain.service.ProjectDomainQueryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * Created by dkuchugurov on 26.06.2014.
 */
@Component
@Transactional(propagation = Propagation.MANDATORY)
public class SectionContentBuilderImpl implements SectionContentBuilder {

    @Autowired
    private PersistedSectionContentRepository<PersistedSectionContent> repository;

    @Autowired
    private DocumentDomainService documentDomainService;

    @Autowired
    private HistoryEntryRepository historyEntryRepository;

    @Autowired
    private ReferenceAssociationRepository referenceAssociationRepository;

    @Autowired
    private ProjectDomainQueryService projectDomainQueryService;

    @Autowired
    private TenantQueryService tenantQueryService;

    @Autowired
    private TermAssociationRepository termAssociationRepository;

    @Autowired
    private SectionTemplateRepository<SectionTemplate> sectionTemplateRepository;


    public HistorySectionContent buildHistorySectionContent(Document document, SectionTemplate sectionTemplate) {
        Validate.notNull(document);
        Validate.notNull(sectionTemplate);
        List<HistoryEntry> entries = historyEntryRepository.findBy(document);
        return new HistorySectionContent(document.getDocumentId(), entries, sectionTemplate);
    }

    public ReferenceSectionContent buildReferenceSectionContent(Document document, SectionTemplate sectionTemplate) {
        Validate.notNull(document);
        Validate.notNull(sectionTemplate);
        List<Reference> references = referenceAssociationRepository.findBy(document.getTenantId().getId(), document);
        return new ReferenceSectionContent(document.getDocumentId(), sectionTemplate, references);
    }

    public TermSectionContent buildTermSectionContent(Document document, SectionTemplate sectionTemplate) {
        Validate.notNull(document);
        Validate.notNull(sectionTemplate);
        List<Term> terms = termAssociationRepository.findBy(document.getTenantId().getId(), document);
        return new TermSectionContent(document.getDocumentId(), sectionTemplate, terms);
    }

    public TextSectionContent buildTextSectionContent(Document document, SectionTemplate sectionTemplate) {
        Validate.notNull(document);
        Validate.notNull(sectionTemplate);
        PersistedTextSectionContent content = repository.findBy(document, sectionTemplate);
        return content != null ? content : new EmptyTextSectionContent(document.getDocumentId(), sectionTemplate);
    }

    public TitleSectionContent buildTitleSectionContent(Document document, SectionTemplate sectionTemplate) {
        Validate.notNull(document);
        Validate.notNull(sectionTemplate);
        try {
            DocumentRevision revision = documentDomainService.getRevision(document.getDocumentId());
            Project project = projectDomainQueryService.findBy(document.getProjectId());
            Tenant tenant = tenantQueryService.find(document.getTenantId());
            return new TitleSectionContent(sectionTemplate, document.getDocumentId(), project.getName(), tenant.getAvatarId(), revision, document.getModifiedByUserId());
        } catch (DocumentNotFoundException | ProjectNotFoundException | TenantNotFoundException e) {
            throw new RuntimeException(e);
        }

    }

    public TocSectionContent buildTocSectionContent(Document document, SectionTemplate sectionTemplate) {
        Validate.notNull(document);
        Validate.notNull(sectionTemplate);

        List<SectionTemplate> sectionTemplates = sectionTemplateRepository.findBy(document);

        return new TocSectionContent(sectionTemplate, document.getDocumentId(), sectionTemplates);
    }
}
