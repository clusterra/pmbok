/*
 * Copyright (c) 2014.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * 	http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.clusterra.pmbok.document.domain.model.document.section.history;

import com.clusterra.pmbok.document.domain.model.document.DocumentId;
import com.clusterra.pmbok.document.domain.model.document.section.ASectionContent;
import com.clusterra.pmbok.document.domain.model.document.section.SectionContentVisitor;
import com.clusterra.pmbok.document.domain.model.history.HistoryEntry;
import com.clusterra.pmbok.document.domain.model.template.section.SectionTemplate;

import java.util.List;

/**
 * @author Denis Kuchugurov.
 *         23.07.2014.
 */
public class HistorySectionContent extends ASectionContent {

    private final List<HistoryEntry> historyEntries;

    public HistorySectionContent(DocumentId documentId, List<HistoryEntry> historyEntries, SectionTemplate sectionTemplate) {
        super(sectionTemplate, documentId);
        this.historyEntries = historyEntries;
    }

    public List<HistoryEntry> getHistoryEntries() {
        return historyEntries;
    }

    @Override
    public <T> T accept(SectionContentVisitor<T> visitor) throws Exception {
        return visitor.visit(this);
    }
}
