/*
 * Copyright (c) 2014.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * 	http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.clusterra.pmbok.document.domain.service.history;

import com.clusterra.pmbok.document.domain.model.document.DocumentId;
import com.clusterra.pmbok.document.domain.model.history.HistoryEntry;
import com.clusterra.pmbok.document.domain.service.document.DocumentNotFoundException;

import java.util.List;

/**
 * Created by dkuchugurov on 26.06.2014.
 */
public interface HistoryEntryService {

    HistoryEntry addHistoryEntry(DocumentId documentId, String comment) throws DocumentNotFoundException;

    void deleteBy(DocumentId documentId) throws DocumentNotFoundException;

    List<HistoryEntry> findBy(DocumentId documentId) throws DocumentNotFoundException;

}
