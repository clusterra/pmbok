/*
 * Copyright (c) 2014.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * 	http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.clusterra.pmbok.document.domain.model.document.section.reference;

import com.clusterra.pmbok.document.domain.model.document.section.SectionContentVisitor;
import com.clusterra.pmbok.document.domain.model.template.section.SectionTemplate;
import com.clusterra.pmbok.reference.domain.model.reference.Reference;
import com.clusterra.pmbok.document.domain.model.document.DocumentId;
import com.clusterra.pmbok.document.domain.model.document.section.ASectionContent;

import java.util.List;

/**
 * @author Denis Kuchugurov.
 *         23.07.2014.
 */
public class ReferenceSectionContent extends ASectionContent {

    private final List<Reference> references;

    public ReferenceSectionContent(DocumentId documentId, SectionTemplate sectionTemplate, List<Reference> references) {
        super(sectionTemplate, documentId);
        this.references = references;
    }

    public List<Reference> getReferences() {
        return references;
    }

    @Override
    public <T> T accept(SectionContentVisitor<T> visitor) throws Exception {
        return visitor.visit(this);
    }
}
