/*
 * Copyright (c) 2014.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * 	http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.clusterra.pmbok.document.domain.service.reference.listen;

import com.clusterra.iam.core.application.user.UserNotFoundException;
import com.clusterra.iam.core.application.user.UserQueryService;
import com.clusterra.pmbok.reference.domain.model.reference.Reference;
import com.clusterra.pmbok.reference.domain.service.ReferenceDomainService;
import com.clusterra.pmbok.document.domain.model.document.Document;
import com.clusterra.pmbok.document.domain.model.document.event.DocumentCreatedEvent;
import com.clusterra.pmbok.document.domain.service.document.DocumentDomainService;
import com.clusterra.pmbok.document.domain.service.document.DocumentNotFoundException;
import com.clusterra.pmbok.document.domain.service.reference.ReferenceAssociationService;
import com.clusterra.pmbok.reference.domain.service.ReferenceNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationListener;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;

/**
 * Created by dkuchugurov on 26.03.2014.
 */
@Component
public class DocumentCreatedEvent4ReferenceListener implements ApplicationListener<DocumentCreatedEvent> {

    @Autowired
    private ReferenceDomainService referenceDomainService;

    @Autowired
    private ReferenceAssociationService referenceAssociationService;

    @Autowired
    private DocumentDomainService documentDomainService;

    @Autowired
    private UserQueryService userQueryService;

    @Transactional(propagation = Propagation.MANDATORY)
    public void onApplicationEvent(DocumentCreatedEvent event) {
        try {
            String author = userQueryService.findUser(event.getCreatedByUserId()).getPerson().getDisplayName();
            String revision = documentDomainService.getRevision(event.getDocumentId()).getRevision();
            Document document = documentDomainService.findBy(event.getDocumentId());
            String name = document.getTemplate().getName();
            Reference reference = referenceDomainService.create(event.getTenantId(), revision, name, new Date(), author);
            referenceAssociationService.createAssociation(event.getTenantId(), event.getDocumentId(), reference.getReferenceId());
        } catch (DocumentNotFoundException | ReferenceNotFoundException | UserNotFoundException e) {
            throw new RuntimeException(e);
        }
    }
}
