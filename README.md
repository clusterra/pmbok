#Clusterra PMBOK, Project Management Body of Knowledge

[![Apache License](https://img.shields.io/badge/Licence-Apache%202.0-blue.svg?style=flat-square)](http://www.apache.org/licenses/LICENSE-2.0)    
[![The Central Repository](https://img.shields.io/badge/Artifacts-The%20Central%20Repository-orange.svg?style=flat-square)][iam-artifacts-mvn]
[![Sonatype OSS](https://img.shields.io/badge/Artifacts-Sonatype%20OSS-orange.svg?style=flat-square)][iam-artifacts-sonatype]     
[![Build Status](https://drone.io/bitbucket.org/clusterra/pmbok/status.png)](https://drone.io/bitbucket.org/clusterra/pmbok/latest)

##1. Documentation
Check out project [wiki][] for Clusterra PMBOK overview.

##2. Issue Tracking
Report issues [here][issues]

##3. Building from Source
The IAM project uses a [Gradle][] build system. In the instructions
below, `./gradlew` is invoked from the root of the source tree and serves as
a cross-platform, self-contained bootstrap mechanism for the build.   
The gradle tasks of particular interest:   
`gradlew build`   
`gradlew idea` - generate IntelliJ IDEA project modules   
`gradlew intTest` - execute integration tests, will require postgresql db instance. 
`gradlew -q dependencies pmbok-server-runner:dependencies --configuration runtime` - useful for dependencies check


###3.1. Prerequisites
[Git][], [JDK8][] and [Postgresql][] database.   
Build tool is [Gradle][]

Be sure that your `JAVA_HOME` environment variable points to the `jdk1.8.0` installation.

##4. How to run server
From IDE select main class com.clusterra.iam.demo.server.ServerRunner.
Spring boot is used to bootstrap the application. Check out [environment set-up][env-set-up] for custom properties.


##5. Contributing
[Pull requests][pull-requests] are welcome; see the [contributor guidelines][contributing] for details.

[Git]: http://help.github.com/set-up-git-redirect
[Gradle]: http://gradle.org
[apache-license]: http://www.apache.org/licenses/LICENSE-2.0
[JDK8]: http://www.oracle.com/technetwork/java/javase/downloads
[Postgresql]: http://www.postgresql.org/
[wiki]: https://bitbucket.org/clusterra/pmbok/wiki/Home
[pull-requests]: https://bitbucket.org/clusterra/pmbok/pull-requests
[contributing]: https://bitbucket.org/clusterra/docs/wiki/Contributing
[iam-artifacts-mvn]: http://search.maven.org/#search%7Cga%7C1%7Cg%3A%22com.clusterra%22
[iam-artifacts-sonatype]: https://oss.sonatype.org/#nexus-search;quick~com.clusterra
[env-set-up]: https://bitbucket.org/clusterra/docs/wiki/Environment%20set-up
[issues]: https://bitbucket.org/clusterra/pmbok/issues