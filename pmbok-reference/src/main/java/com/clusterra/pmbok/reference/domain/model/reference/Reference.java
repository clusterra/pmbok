/*
 * Copyright 2014 www.equmo.org
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * 	http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.clusterra.pmbok.reference.domain.model.reference;

import org.apache.commons.lang3.Validate;
import org.hibernate.annotations.GenericGenerator;
import org.springframework.data.annotation.CreatedBy;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedBy;
import org.springframework.data.annotation.LastModifiedDate;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import java.util.Date;

/**
 * Created with IntelliJ IDEA.
 *
 * @author Denis Kuchugurov
 *         Date: 05.02.14
 */
@Entity
@Table(name = "pmb_reference")
public class Reference {

    @Id
    @GeneratedValue(generator = "base64")
    @GenericGenerator(name = "base64", strategy = "com.clusterra.hibernate.base64.Base64IdGenerator")
    private String id;

    @Basic
    @Column(nullable = false)
    private String tenantId;

    @Basic
    @CreatedDate
    private Date createdDate;

    @Basic
    @CreatedBy
    private String createdByUserId;

    @Basic
    @LastModifiedDate
    private Date modifiedDate;

    @Basic
    @LastModifiedBy
    private String modifiedByUserId;

    @Basic
    @Column(nullable = false)
    private String number;

    @Basic
    @Column(nullable = false)
    private String name;

    @Basic
    private Date publicationDate;

    @Basic
    private String author;

    Reference() {
    }

    public Reference(String tenantId, String number, String name, Date publicationDate, String author) {
        Validate.notNull(tenantId, "tenantId is null");
        Validate.notNull(number, "number is null");
        Validate.notNull(name, "name is null");
        this.tenantId = tenantId;
        this.number = number;
        this.name = name;
        this.publicationDate = publicationDate;
        this.author = author;
    }

    public ReferenceId getReferenceId() {
        return new ReferenceId(id);
    }

    public String getTenantId() {
        return tenantId;
    }

    public Date getCreatedDate() {
        return createdDate;
    }

    public String getCreatedByUserId() {
        return createdByUserId;
    }

    public Date getModifiedDate() {
        return modifiedDate;
    }

    public String getModifiedByUserId() {
        return modifiedByUserId;
    }

    public String getNumber() {
        return number;
    }

    public String getName() {
        return name;
    }

    public Date getPublicationDate() {
        return publicationDate;
    }

    public String getAuthor() {
        return author;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setPublicationDate(Date publicationDate) {
        this.publicationDate = publicationDate;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Reference that = (Reference) o;

        if (id != null ? !id.equals(that.id) : that.id != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        return id != null ? id.hashCode() : 0;
    }

    @Override
    public String toString() {
        return "Reference{" +
                "id='" + id + '\'' +
                ", name='" + name + '\'' +
                '}';
    }
}
