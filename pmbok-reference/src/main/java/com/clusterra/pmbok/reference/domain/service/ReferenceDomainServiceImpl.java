/*
 * Copyright (c) 2014.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * 	http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.clusterra.pmbok.reference.domain.service;

import com.clusterra.pmbok.reference.domain.model.reference.Reference;
import com.clusterra.pmbok.reference.domain.model.reference.repo.ReferenceRepository;
import com.clusterra.pmbok.reference.domain.model.reference.repo.ReferenceSearchBySpecification;
import org.apache.commons.lang3.Validate;
import com.clusterra.iam.core.application.tenant.TenantId;
import com.clusterra.pmbok.reference.domain.model.reference.ReferenceId;
import com.clusterra.pmbok.reference.domain.model.reference.repo.ReferenceTenantSpecification;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.jpa.domain.Specifications;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;

/**
 * Created by dkuchugurov on 27.06.2014.
 */
@Service
@Transactional(propagation = Propagation.MANDATORY)
public class ReferenceDomainServiceImpl implements ReferenceDomainService {

    @Autowired
    private ReferenceRepository referenceRepository;

    public Reference create(TenantId tenantId, String number, String name, Date publicationDate, String author) {
        Validate.notNull(tenantId);
        Reference reference = new Reference(tenantId.getId(), number, name, publicationDate, author);
        return referenceRepository.save(reference);
    }

    public Reference update(ReferenceId referenceId, String number, String name, Date publicationDate, String author) throws ReferenceNotFoundException {
        Validate.notNull(referenceId);
        Reference reference = referenceRepository.findOne(referenceId.getId());
        if (reference == null) {
            throw new ReferenceNotFoundException(referenceId);
        }
        reference.setAuthor(author);
        reference.setNumber(number);
        reference.setName(name);
        reference.setPublicationDate(publicationDate);
        return referenceRepository.save(reference);
    }

    public void delete(ReferenceId referenceId) {
        referenceRepository.delete(referenceId.getId());
    }


    public Page<Reference> findBy(TenantId tenantId, Pageable pageable, String searchBy) {
        Validate.notNull(tenantId);

        Specification<Reference> tenantSpec = new ReferenceTenantSpecification(tenantId.getId());
        Specifications<Reference> specifications = Specifications.where(tenantSpec);

        if (!searchBy.isEmpty()) {
            specifications = specifications.and(new ReferenceSearchBySpecification(searchBy));
        }

        return referenceRepository.findAll(specifications, pageable);

    }

    public Reference findBy(ReferenceId referenceId) throws ReferenceNotFoundException {
        Validate.notNull(referenceId);

        Reference reference = referenceRepository.findOne(referenceId.getId());
        if (reference == null) {
            throw new ReferenceNotFoundException(referenceId);
        }
        return reference;
    }

}
