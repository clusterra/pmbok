/*
 * Copyright 2014 www.equmo.org
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * 	http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.clusterra.pmbok.reference.application.csv;

import com.fasterxml.jackson.databind.MappingIterator;
import com.fasterxml.jackson.databind.ObjectReader;
import com.fasterxml.jackson.dataformat.csv.CsvMapper;
import com.fasterxml.jackson.dataformat.csv.CsvSchema;
import com.clusterra.iam.core.application.tenant.TenantId;
import com.clusterra.pmbok.reference.application.ReferenceService;
import org.joda.time.format.DateTimeFormat;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.util.Date;

/**
 * Created with IntelliJ IDEA.
 *
 * @author Denis Kuchugurov
 *         Date: 07.02.14
 */
@Component
public class ReferenceCsvLoaderImpl implements ReferenceCsvLoader {

    private static final char CSV_SEPARATOR = ';';

    private static Logger logger = LoggerFactory.getLogger(ReferenceCsvLoaderImpl.class);

    @Autowired
    private ReferenceService referenceService;

    public void validateContent(byte[] csvContent) throws InvalidCsvReferenceContentException {
        try {
            MappingIterator<CsvMapperData> values = readValues(csvContent);
            values.next();
        } catch (Exception e) {
            throw new InvalidCsvReferenceContentException("Invalid csv format, " + e.getMessage());
        }
    }

    public void loadFromCsv(TenantId tenantId, byte[] csvContent) throws ReferenceCsvLoaderException {

        MappingIterator<CsvMapperData> values = readValues(csvContent);

        while (values.hasNext()) {
            try {
                CsvMapperData data = values.next();
                Date publicationDate = parseDate(data.getPublicationDate());
                referenceService.create(tenantId, data.getNumber(), data.getName(), publicationDate, data.getAuthor());
            } catch (Exception e) {
                throw new ReferenceCsvLoaderException(e);
            }
        }

    }

    private Date parseDate(String dateString) throws ReferenceCsvLoaderException {
        if (dateString == null || dateString.isEmpty()) {
            return null;
        }
        try {

            return DateTimeFormat.forPattern("YYYY").parseDateTime(dateString).toDate();
        } catch (IllegalArgumentException e) {
            throw new ReferenceCsvLoaderException("unable to parse date " + dateString + ", only YYYY format is supported", e);
        }
    }

    private MappingIterator<CsvMapperData> readValues(byte[] csvContent) throws ReferenceCsvLoaderException {
        CsvMapper csvMapper = new CsvMapper();
        CsvSchema csvSchema = csvMapper.schemaFor(CsvMapperData.class).
                withColumnSeparator(CSV_SEPARATOR).
                withSkipFirstDataRow(true).withHeader().
                withoutColumns();

        ObjectReader objectReader = csvMapper.reader(CsvMapperData.class).with(csvSchema);
        try {
            return objectReader.readValues(csvContent);
        } catch (IOException e) {
            throw new ReferenceCsvLoaderException(e);
        }
    }
}
