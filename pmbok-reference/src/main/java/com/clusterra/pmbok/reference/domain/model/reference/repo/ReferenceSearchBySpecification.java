/*
 * Copyright 2014 www.equmo.org
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * 	http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.clusterra.pmbok.reference.domain.model.reference.repo;

import com.clusterra.pmbok.reference.domain.model.reference.Reference;
import org.springframework.data.jpa.domain.Specification;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

/**
 * Created with IntelliJ IDEA.
 *
 * @author Denis Kuchugurov
 *         Date: 20.12.13
 */
public class ReferenceSearchBySpecification implements Specification<Reference> {

    private final String searchBy;

    public ReferenceSearchBySpecification(String searchBy) {
        this.searchBy = ("%" + searchBy + "%").toLowerCase();
    }

    @Override
    public Predicate toPredicate(Root<Reference> root, CriteriaQuery<?> query, CriteriaBuilder cb) {

        Predicate number = cb.like(cb.lower(root.<String>get("number").as(String.class)), searchBy);
        Predicate name = cb.like(cb.lower(root.<String>get("name").as(String.class)), searchBy);
        Predicate author = cb.like(cb.lower(root.<String>get("author").as(String.class)), searchBy);

        return cb.or(number, name, author);
    }
}
