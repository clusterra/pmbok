/*
 * Copyright (c) 2014.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * 	http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.clusterra.pmbok.reference.domain.service;

import com.clusterra.iam.core.application.tenant.TenantId;
import com.clusterra.pmbok.reference.domain.model.reference.Reference;
import com.clusterra.pmbok.reference.domain.model.reference.ReferenceId;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.Date;

/**
 * Created by dkuchugurov on 27.06.2014.
 */
public interface ReferenceDomainService {

    Reference create(TenantId tenantId, String number, String name, Date publicationDate, String author);

    Reference update(ReferenceId referenceId, String number, String name, Date publicationDate, String author) throws ReferenceNotFoundException;

    void delete(ReferenceId referenceId);

    Reference findBy(ReferenceId referenceId) throws ReferenceNotFoundException;

    Page<Reference> findBy(TenantId tenantId, Pageable pageable, String searchBy);

}
