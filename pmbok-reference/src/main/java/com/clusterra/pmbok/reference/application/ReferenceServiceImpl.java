/*
 * Copyright 2014 www.equmo.org
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * 	http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.clusterra.pmbok.reference.application;

import com.clusterra.iam.core.application.tenant.TenantId;
import com.clusterra.iam.core.application.tracker.IdentityTracker;
import com.clusterra.iam.core.application.tracker.NotAuthenticatedException;
import com.clusterra.pmbok.reference.domain.model.reference.Reference;
import com.clusterra.pmbok.reference.domain.model.reference.ReferenceId;
import com.clusterra.pmbok.reference.domain.service.ReferenceDomainService;
import com.clusterra.pmbok.reference.domain.service.ReferenceNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;

/**
 * Created with IntelliJ IDEA.
 *
 * @author Denis Kuchugurov
 *         Date: 05.02.14
 */
@Service
public class ReferenceServiceImpl implements ReferenceService {

    @Autowired
    private IdentityTracker identityTracker;

    @Autowired
    private ReferenceDomainService service;


    @Transactional
    public Reference create(TenantId tenantId, String number, String name, Date publicationDate, String author) {
        return service.create(tenantId, number, name, publicationDate, author);
    }

    @Transactional
    public Reference update(ReferenceId referenceId, String number, String name, Date publicationDate, String author) throws ReferenceNotFoundException {
        return service.update(referenceId, number, name, publicationDate, author);
    }

    @Transactional
    public void delete(ReferenceId referenceId) {
        service.delete(referenceId);
    }


    @Transactional
    public Page<Reference> findBy(Pageable pageable, String searchBy) throws NotAuthenticatedException {
        TenantId tenantId = identityTracker.currentTenant();
        return service.findBy(tenantId, pageable, searchBy);
    }

    @Transactional
    public Reference findBy(ReferenceId referenceId) throws ReferenceNotFoundException {
        return service.findBy(referenceId);
    }
}
