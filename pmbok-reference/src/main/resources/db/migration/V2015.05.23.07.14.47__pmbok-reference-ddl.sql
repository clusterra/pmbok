
    create table pmb_reference (
        id varchar(255) not null,
        author varchar(255),
        createdByUserId varchar(255),
        createdDate timestamp,
        modifiedByUserId varchar(255),
        modifiedDate timestamp,
        name varchar(255) not null,
        number varchar(255) not null,
        publicationDate timestamp,
        tenantId varchar(255) not null,
        primary key (id)
    );
