/*
 * Copyright 2014 www.equmo.org
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * 	http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.clusterra.pmbok.reference.domain.service;

import com.clusterra.iam.core.application.tenant.TenantCommandService;
import com.clusterra.iam.core.application.tenant.TenantId;
import com.clusterra.iam.core.domain.model.tenant.Tenant;
import com.clusterra.iam.core.testutil.UserRandomUtil;
import com.clusterra.pmbok.reference.domain.model.reference.Reference;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.ConfigFileApplicationContextInitializer;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.testng.AbstractTransactionalTestNGSpringContextTests;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.util.Date;

import static org.apache.commons.lang3.RandomStringUtils.randomAlphabetic;
import static org.fest.assertions.api.Assertions.assertThat;

/**
 * Created with IntelliJ IDEA.
 *
 * @author Denis Kuchugurov
 *         Date: 05.02.14
 */
@ContextConfiguration(
        value = {"classpath*:META-INF/spring/*.xml"},
        initializers = ConfigFileApplicationContextInitializer.class)
public class ReferenceDomainServiceTest extends AbstractTransactionalTestNGSpringContextTests {

    @Autowired
    private ReferenceDomainService referenceDomainService;

    private Reference reference1;
    private Reference reference2;

    @Autowired
    private TenantCommandService tenantCommandService;

    private TenantId tenantId;


    @BeforeMethod
    public void beforeMethod() throws Exception {
        Tenant tenant = tenantCommandService.create(randomAlphabetic(10), UserRandomUtil.getRandomEmail());
        tenantId = new TenantId(tenant.getId());
        reference1 = referenceDomainService.create(tenantId, randomAlphabetic(5), randomAlphabetic(5), new Date(), randomAlphabetic(5));
        reference2 = referenceDomainService.create(tenantId, randomAlphabetic(5), randomAlphabetic(5), new Date(), randomAlphabetic(5));
    }

    @Test
    public void test_find_all() throws Exception {
        Page<Reference> list = referenceDomainService.findBy(tenantId, new PageRequest(0, 100), "");
        assertThat(list.getContent()).isNotEmpty();
        assertThat(list.getContent()).contains(reference1, reference2);
    }

    @Test
    public void test_find_by_ids() throws Exception {
        Reference id1 = referenceDomainService.create(tenantId, randomAlphabetic(5), randomAlphabetic(5), new Date(), randomAlphabetic(5));
        Reference id2 = referenceDomainService.create(tenantId, randomAlphabetic(5), randomAlphabetic(5), new Date(), randomAlphabetic(5));
        Reference id3 = referenceDomainService.create(tenantId, randomAlphabetic(5), randomAlphabetic(5), new Date(), randomAlphabetic(5));

        Page<Reference> list = referenceDomainService.findBy(tenantId, new PageRequest(0, 100), "");
        assertThat(list.getContent()).contains(id1, id2, id3);
    }

    @Test
    public void test_find_all_search_by() throws Exception {

        Tenant otherTenant = tenantCommandService.create(randomAlphabetic(5), UserRandomUtil.getRandomEmail());

        referenceDomainService.create(tenantId, "abcdef" + randomAlphabetic(5), randomAlphabetic(5), new Date(), randomAlphabetic(5));
        referenceDomainService.create(tenantId, "abcdef" + randomAlphabetic(5), randomAlphabetic(5), new Date(), randomAlphabetic(5));
        referenceDomainService.create(tenantId, randomAlphabetic(5), randomAlphabetic(5) + "abcdef", new Date(), randomAlphabetic(5));
        referenceDomainService.create(tenantId, randomAlphabetic(5), randomAlphabetic(5) + "abcdef", new Date(), randomAlphabetic(5));
        referenceDomainService.create(tenantId, randomAlphabetic(5), randomAlphabetic(5), new Date(), randomAlphabetic(5));
        referenceDomainService.create(tenantId, randomAlphabetic(5), randomAlphabetic(5), new Date(), randomAlphabetic(5));

        Page<Reference> page = referenceDomainService.findBy(tenantId, new PageRequest(0, 100), "abcdef");
        assertThat(page.getContent().size()).isEqualTo(4);

    }

    @Test(expectedExceptions = ReferenceNotFoundException.class)
    public void test_delete() throws Exception {
        Reference r_1 = referenceDomainService.findBy(reference1.getReferenceId());
        assertThat(r_1).isNotNull();

        referenceDomainService.delete(reference1.getReferenceId());

        referenceDomainService.findBy(reference1.getReferenceId());
    }

    //TODO
    public void test_delete_document_deletes_self_reference() {

    }

}
