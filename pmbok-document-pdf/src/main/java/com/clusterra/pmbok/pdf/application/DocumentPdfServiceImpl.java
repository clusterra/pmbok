/*
 * Copyright (c) 2014.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * 	http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.clusterra.pmbok.pdf.application;

import com.clusterra.iam.avatar.application.AvatarData;
import com.clusterra.iam.avatar.application.AvatarNotFoundException;
import com.clusterra.iam.avatar.application.AvatarService;
import com.clusterra.iam.core.application.tracker.IdentityTracker;
import com.clusterra.pmbok.document.application.document.DocumentQueryService;
import com.clusterra.pmbok.document.domain.model.document.Document;
import com.clusterra.pmbok.document.domain.model.document.DocumentId;
import com.clusterra.pmbok.document.domain.model.document.DocumentRevision;
import com.clusterra.pmbok.document.domain.service.document.DocumentDomainService;
import com.clusterra.pmbok.document.domain.service.document.DocumentNotFoundException;
import com.clusterra.pmbok.generate.pdf.PdfGeneratorService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by dkuchugurov on 18.03.14.
 */
@Service
public class DocumentPdfServiceImpl implements DocumentPdfService {


    @Autowired
    private DocumentDomainService documentDomainService;

    @Autowired
    private PdfGeneratorService pdfGeneratorService;

    @Autowired
    private DocumentToHtmlTransformer documentToHtmlTransformer;

    @Autowired
    private IdentityTracker identityTracker;

    @Autowired
    private AvatarService avatarService;

    @Autowired
    private DocumentQueryService documentQueryService;

    @Transactional
    public byte[] generatePdf(DocumentId documentId) throws Exception {
        String html = documentToHtmlTransformer.transform(identityTracker.currentTenant(), documentId);
        Map<String, Resource> resources = new HashMap<>();
        resources.put("main_pdf.css", new ClassPathResource("css/main_pdf.css"));
        Map<String, byte[]> content = ResourceHelper.getContent(resources);

        DocumentRevision revision = documentQueryService.getRevision(documentId);

        content.putAll(getAvatar(revision.getAvatarId()));
        return pdfGeneratorService.generatePdfFromHtml(html, content);
    }

    @Transactional
    public String getFileName(DocumentId documentId) throws DocumentNotFoundException {
        Document document = documentDomainService.findBy(documentId);
        return String.format("%s-%s.pdf", document.getTemplate().getName(), documentDomainService.getRevision(documentId).getRevision());
    }

    private Map<String, byte[]> getAvatar(String avatarId) {
        try {
            Map<String, byte[]> result = new HashMap<>(1);
            if (avatarId == null) {
                return result;
            }
            AvatarData avatarData = avatarService.find(avatarId);
            result.put("organization_avatar.png", avatarData.getImage128());
            return result;
        } catch (AvatarNotFoundException e) {
            throw new RuntimeException(e);
        }
    }
}
