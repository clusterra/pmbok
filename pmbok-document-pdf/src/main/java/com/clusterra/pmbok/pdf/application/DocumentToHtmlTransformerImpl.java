/*
 * Copyright (c) 2014.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * 	http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.clusterra.pmbok.pdf.application;

import com.clusterra.freemarker.renderer.FreemarkerTemplateRenderer;
import com.clusterra.iam.avatar.application.AvatarService;
import com.clusterra.iam.core.application.tenant.TenantId;
import com.clusterra.iam.core.application.user.UserId;
import com.clusterra.iam.core.application.user.UserQueryService;
import com.clusterra.pmbok.document.application.document.DocumentQueryService;
import com.clusterra.pmbok.document.domain.model.document.Document;
import com.clusterra.pmbok.document.domain.model.document.DocumentId;
import com.clusterra.pmbok.document.domain.model.document.DocumentRevision;
import com.clusterra.pmbok.document.domain.model.document.section.SectionContent;
import com.clusterra.pmbok.document.domain.model.document.section.SectionContentVisitor;
import com.clusterra.pmbok.document.domain.model.document.section.history.HistorySectionContent;
import com.clusterra.pmbok.document.domain.model.document.section.reference.ReferenceSectionContent;
import com.clusterra.pmbok.document.domain.model.document.section.term.TermSectionContent;
import com.clusterra.pmbok.document.domain.model.document.section.text.TextSectionContent;
import com.clusterra.pmbok.document.domain.model.document.section.title.TitleSectionContent;
import com.clusterra.pmbok.document.domain.model.document.section.toc.TocSectionContent;
import com.clusterra.pmbok.document.domain.model.history.HistoryEntry;
import com.clusterra.pmbok.document.domain.model.template.Template;
import com.clusterra.pmbok.pdf.section.HistoryData;
import com.clusterra.pmbok.template.Section;
import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by dkuchugurov on 06.04.2014.
 */
@Component
public class DocumentToHtmlTransformerImpl implements DocumentToHtmlTransformer, SectionContentVisitor<Section> {


    @Autowired
    private UserQueryService userQueryService;

    @Autowired
    private MessageSource messageSource;

    @Autowired
    private AvatarService avatarService;

    @Autowired
    private DocumentQueryService documentQueryService;

    @Autowired
    private FreemarkerTemplateRenderer freemarkerTemplateRenderer;

    public String transform(TenantId tenantId, DocumentId documentId) throws Exception {
        List<Section> sections = new ArrayList<>();

        Document document = documentQueryService.findBy(documentId);
        Template template = document.getTemplate();

        List<SectionContent> sectionContents = documentQueryService.findSectionContents(documentId);

        StringBuilder sectionsHtml = new StringBuilder();
        for (SectionContent section : sectionContents) {
            Section s = section.accept(this);
            if (s != null) {
                sections.add(s);
                sectionsHtml.append(s.getHtml());
            }
        }

        StringBuilder partsHtml = new StringBuilder();

        DocumentRevision revision = documentQueryService.getRevision(documentId);
        partsHtml.append(getHeader(document.getModifiedDate(), revision.getRevision(), revision.getAvatarId(), revision.getDocumentName(), revision.getProjectName()));
        partsHtml.append(getFooter(revision.getProjectName()));

        String name = template.getName() + "_" + document.getRevision().toString();

        Map<String, Object> vars = new HashMap<>();
        vars.put("documentTitle", name);
        vars.put("bookmarksHtml", "TODO bookmarks html");
        vars.put("partsHtml", partsHtml.toString());
        vars.put("sectionsHtml", sectionsHtml.toString());
        return freemarkerTemplateRenderer.render("layout.ftl", vars);
    }

    @Override
    public Section visit(TitleSectionContent content) throws Exception {
        Map<String, Object> vars = new HashMap<>();
        vars.put("projectName", content.getProjectName());
        vars.put("docName", content.getRevision().getDocumentName());
        vars.put("docVersion", content.getRevision().getRevision());
        String render = freemarkerTemplateRenderer.render("part/title.ftl", vars);
        return new Section(render);
    }

    @Override
    public Section visit(HistorySectionContent content) throws Exception {
        List<HistoryData> revisions = new ArrayList<>();

        for (HistoryEntry update : content.getHistoryEntries()) {
            String createdByUserId = update.getCreatedByUserId();
            String author;
            if (createdByUserId == null) {
                author = "N/A";
            } else {
                author = userQueryService.findUser(new UserId(createdByUserId)).getPerson().getDisplayName();
            }
            String createdDate = new DateTime(update.getCreatedDate()).toString(DateTimeFormatters.LS);
            revisions.add(new HistoryData(createdDate, update.getRevision(), update.getComment(), author));
        }
        Map<String, Object> vars = new HashMap<>();
        vars.put("revisions", revisions);
        vars.put("sectionId", 1);
        vars.put("title", content.getSectionTemplate().getName());
        return new Section(freemarkerTemplateRenderer.render("section/revisionHistorySection.ftl", vars));
    }

    @Override
    public Section visit(ReferenceSectionContent content) throws Exception {
        Map<String, Object> vars = new HashMap<>();
        vars.put("sectionId", content.getSectionTemplate().getSectionTemplateId().toString());
        vars.put("title", content.getSectionTemplate().getName());
        vars.put("references", content.getReferences());
        String render = freemarkerTemplateRenderer.render("section/normativeReferenceSection.ftl", vars);

        return new Section(render);
    }

    @Override
    public Section visit(TermSectionContent content) throws Exception {
        Map<String, Object> vars = new HashMap<>();
        vars.put("sectionId", content.getSectionTemplate().getSectionTemplateId().toString());
        vars.put("title", content.getSectionTemplate().getName());
        vars.put("vocabulary", content.getTerms());
        String render = freemarkerTemplateRenderer.render("section/vocabularySection.ftl", vars);

        return new Section(render);
    }

    @Override
    public Section visit(TextSectionContent content) throws Exception {

        Map<String, Object> vars = new HashMap<>();
        vars.put("sectionId", content.getSectionTemplate().getSectionTemplateId().toString());
        vars.put("title", content.getSectionTemplate().getName());
        vars.put("text", content.getText());
        String render = freemarkerTemplateRenderer.render("section/textSection.ftl", vars);

        return new Section(render);
    }

    @Override
    public Section visit(TocSectionContent content) throws Exception {
        Map<String, Object> vars = new HashMap<>();
        vars.put("title", content.getSectionTemplate().getName());

        vars.put("sections", content.getTocElements());

        String render = freemarkerTemplateRenderer.render("part/toc.ftl", vars);
        return new Section(render);
    }


    private String getHeader(Date modifiedDate, String documentRevision, String avatarId, String docName, String projectName) {
        String docUpdatedDateLabel = messageSource.getMessage("document.date.modified.label", null, "", LocaleContextHolder.getLocale());
        String docUpdatedDate = new DateTime(modifiedDate).toString(DateTimeFormatters.LS);
        String docReferenceLabel = messageSource.getMessage("document.reference.label", null, "", LocaleContextHolder.getLocale());
        Map<String, Object> vars = new HashMap<>();
        vars.put("projectName", projectName);
        vars.put("docVersion", documentRevision);
        vars.put("docName", docName);
        String render = freemarkerTemplateRenderer.render("part/header.ftl", vars);
        return render;
    }

    private String getFooter(String projectName) {
        String footerLabel = messageSource.getMessage("document.footer.label", null, "", LocaleContextHolder.getLocale());
        String footerPageLabel = messageSource.getMessage("document.footer.page.label", null, "", LocaleContextHolder.getLocale());
        String footerPageOfLabel = messageSource.getMessage("document.footer.page.of.label", null, "", LocaleContextHolder.getLocale());
        Map<String, Object> vars = new HashMap<>();


        String render = freemarkerTemplateRenderer.render("part/footer.ftl", vars);
        return render;
    }

}
