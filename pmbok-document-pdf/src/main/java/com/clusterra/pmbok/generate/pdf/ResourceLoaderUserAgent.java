/*
 * Copyright (c) 2015.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * 	http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

// Created by Liventsev Andrey. 02.01.14 23:22

package com.clusterra.pmbok.generate.pdf;

import org.xhtmlrenderer.pdf.ITextOutputDevice;
import org.xhtmlrenderer.pdf.ITextUserAgent;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;

/**
 * Replace content of external resources by resources from resource map
 */
public class ResourceLoaderUserAgent extends ITextUserAgent {

    private Map<String, byte[]> resourceMap = new HashMap<>();

    public ResourceLoaderUserAgent(ITextOutputDevice outputDevice, Map<String, byte[]> resourceMap) {
        super(outputDevice);
        this.resourceMap = resourceMap;
    }

    protected InputStream resolveAndOpenStream(String uri) {
        String fileName = new File(uri).getName();
        byte[] externalResource = resourceMap.get(fileName);
        if (externalResource != null) {
            return new ByteArrayInputStream(externalResource);
        }

        return super.resolveAndOpenStream(uri);
    }
}