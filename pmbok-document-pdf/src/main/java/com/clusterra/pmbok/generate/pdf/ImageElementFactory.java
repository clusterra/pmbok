/*
 * Copyright (c) 2015.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * 	http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

// Created by Liventsev Andrey. 02.01.14 22:57

package com.clusterra.pmbok.generate.pdf;

import com.itextpdf.text.Image;
import org.w3c.dom.Element;
import org.xhtmlrenderer.extend.FSImage;
import org.xhtmlrenderer.extend.ReplacedElement;
import org.xhtmlrenderer.extend.ReplacedElementFactory;
import org.xhtmlrenderer.extend.UserAgentCallback;
import org.xhtmlrenderer.layout.LayoutContext;
import org.xhtmlrenderer.pdf.ITextFSImage;
import org.xhtmlrenderer.pdf.ITextImageElement;
import org.xhtmlrenderer.render.BlockBox;
import org.xhtmlrenderer.simple.extend.FormSubmissionListener;

import java.io.File;
import java.util.Map;

/**
 * Replace img target resource by resource from resource map
 */
public class ImageElementFactory implements ReplacedElementFactory {

    private final ReplacedElementFactory superFactory;

    private Map<String, byte[]> resourceMap;

    public ImageElementFactory(ReplacedElementFactory superFactory, Map<String, byte[]> resourceMap) {
        this.superFactory = superFactory;
        this.resourceMap = resourceMap;
    }

    @Override
    public ReplacedElement createReplacedElement(LayoutContext layoutContext, BlockBox blockBox, UserAgentCallback userAgentCallback, int cssWidth, int cssHeight) {
        Element element = blockBox.getElement();
        if (element == null) {
            return null;
        }

        String nodeName = element.getNodeName();
        String src = element.getAttribute("src");
        String fileName = new File(src).getName();
        // Replace any <img src="/src/some/path/image.png" /> with the binary data of `image.png` into the PDF
        if ("img".equals(nodeName) && src != null && resourceMap.get(fileName) != null) {
            return handleImageLink(element, resourceMap.get(fileName), cssWidth, cssHeight);
        }

        return this.superFactory.createReplacedElement(layoutContext, blockBox, userAgentCallback, cssWidth, cssHeight);
    }

    private static ReplacedElement handleImageLink(Element imageNode, byte[] imageContent, int cssWidth, int cssHeight) {
        try {
            Image image = Image.getInstance(imageContent);
            FSImage fsImage = new ITextFSImage(image);
            if ((cssWidth != -1) || (cssHeight != -1)) {
                fsImage.scale(cssWidth, cssHeight);
            }
            return new ITextImageElement(fsImage);

        } catch (Exception e) {
            throw new RuntimeException("There was a problem trying to read an image content: " + imageNode.getAttribute("src"), e);
        }
    }

    @Override
    public void reset() {
        this.superFactory.reset();
    }

    @Override
    public void remove(Element e) {
        this.superFactory.remove(e);
    }

    @Override
    public void setFormSubmissionListener(FormSubmissionListener listener) {
        this.superFactory.setFormSubmissionListener(listener);
    }
}