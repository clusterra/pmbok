<li id="${sectionId}">
    <h2>${title}</h2>

    <table>
    <#list revisions as revision>
        <tr>
            <td>${revision.date}</td>
            <td>${revision.version}</td>
            <td>${revision.description}</td>
            <td>${revision.authorName}</td>
        </tr>
    </#list>
    </table>

    <p>
        Редакция – номер редакции документа (если вносятся значительные изменения в документ, то первая цифра
        увеличивается, а вторая обнуляется; если вносятся незначительные изменения, то меняется вторая цифра).
        Описание – детальное описание редакции и внесённых изменений (к примеру: Издание для внутренних комментариев,
        Издание для использования, Издание с изменениями главы ТАКОЙ-ТО и т.д.).
        Автор - Ф.И.О. автора изменений.
    </p>
</li>
