<html lang="en">
<head>
    <meta content="text/html; charset=utf-8" http-equiv="Content-Type"/>
    <link rel="stylesheet" href="main_pdf.css"/>

    <title>
    ${documentTitle}
    </title>

<#if bookmarksHtml??>
${bookmarksHtml}
</#if>

</head>

<body>

<div>
${partsHtml}
    <ol class="section-wrapper">
    ${sectionsHtml}
    </ol>
</div>
</body>
</html>