<h2 class="on-new-page">${title}</h2>
<ol class="toc-list">
<#list sections as section>
    <li>
        <a href="#${section.sectionTemplateId}" class="toc-item">${section.title}</a>
    </li>
</#list>

</ol>
