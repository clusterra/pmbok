<table class="table--full-width document-header">
    <tr>
        <td class="rowspan3"><img height="26px" src="img/organization_avatar.png"/></td>
        <td>${projectName}</td>
        <td>${docVersion}</td>
    </tr>
    <tr>
        <td>${docName}</td>
        <td>docUpdatedDateLabel: $docUpdatedDate</td>
    </tr>
    <tr>
        <td class="colspan2">docReferenceLabel: docReferenceValue</td>
    </tr>
</table>

