<table class="table--full-width document-header">
    <tr>
        <td class="rowspan2"><img height="128px" src="img/organization_avatar.png"/></td>
        <td>docReferenceLabel: $docReferenceValue</td>
    </tr>
    <tr>
        <td>organizationName</td>
    </tr>
</table>
<div class="right">
    <p class="big">
        "Приказываю назначить Руководителя проекта и<br/>
        начать проект в соответствии с данным Уставом проекта"<br/>
        Директор organizationName<br/>
        organizationCEO<br/>
        docUpdatedDateLabel: docUpdatedDate
    </p>

    <h1 class="uppercased">
    ${projectName}<br/>
    ${docName}<br/>
    ${docVersion}
    </h1>
</div>
<div class="clear"></div>
