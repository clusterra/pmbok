/*
 * Copyright (c) 2015.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * 	http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.clusterra.pmbok.pdf.application;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.springframework.beans.factory.annotation.Autowired;
import org.testng.annotations.Test;

import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStream;

import static org.fest.assertions.api.Assertions.assertThat;

/**
 * Created by Denis Kuchugurov
 * on 13/05/15 23:01.
 */
public class DocumentToHtmlTransformerTest extends DocumentPdfServiceTest {

    @Autowired
    private DocumentToHtmlTransformer documentToHtmlTransformer;

    @Autowired
    private String tmpPath;


    @Test(enabled = true)
    public void test_transform() throws Exception {
        String html = documentToHtmlTransformer.transform(tenantId, documentId);
        assertThat(html).isNotEmpty();

        File directory = new File(tmpPath);

        System.out.println("...creating directory " + directory.getAbsolutePath());
        FileUtils.forceMkdir(directory);
        OutputStream os = new FileOutputStream(tmpPath + File.separator + DateTime.now().toString(DateTimeFormat.forPattern("yyyy.MM.dd.HH.mm.ss")) + ".html");
        os.write(html.getBytes());
        IOUtils.closeQuietly(os);
    }

}