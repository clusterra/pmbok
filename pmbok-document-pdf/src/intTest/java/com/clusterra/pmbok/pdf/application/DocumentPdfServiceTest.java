/*
 * Copyright (c) 2014.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * 	http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.clusterra.pmbok.pdf.application;

import com.clusterra.iam.avatar.application.AvatarId;
import com.clusterra.iam.avatar.application.AvatarService;
import com.clusterra.iam.avatar.domain.model.AvatarType;
import com.clusterra.iam.core.application.group.GroupDescriptor;
import com.clusterra.iam.core.application.group.GroupService;
import com.clusterra.iam.core.application.membership.AuthorizedMembershipService;
import com.clusterra.iam.core.application.role.RoleDescriptor;
import com.clusterra.iam.core.application.role.RoleService;
import com.clusterra.iam.core.application.tenant.TenantCommandService;
import com.clusterra.iam.core.application.tenant.TenantId;
import com.clusterra.iam.core.application.tracker.IdentityTrackerLifeCycle;
import com.clusterra.iam.core.application.user.DefaultRole;
import com.clusterra.iam.core.application.user.UserCommandService;
import com.clusterra.iam.core.application.user.UserId;
import com.clusterra.iam.core.domain.model.tenant.Tenant;
import com.clusterra.iam.core.testutil.UserRandomUtil;
import com.clusterra.pmbok.document.domain.model.document.Document;
import com.clusterra.pmbok.document.domain.model.document.DocumentId;
import com.clusterra.pmbok.document.domain.model.template.Template;
import com.clusterra.pmbok.document.domain.model.template.section.SectionTemplate;
import com.clusterra.pmbok.document.domain.service.document.DocumentDomainService;
import com.clusterra.pmbok.document.domain.service.reference.ReferenceAssociationService;
import com.clusterra.pmbok.document.domain.service.template.TemplateDomainService;
import com.clusterra.pmbok.document.domain.service.term.TermAssociationService;
import com.clusterra.pmbok.project.domain.model.Project;
import com.clusterra.pmbok.project.domain.model.ProjectId;
import com.clusterra.pmbok.project.domain.model.ProjectVersion;
import com.clusterra.pmbok.project.domain.service.ProjectDomainQueryService;
import com.clusterra.pmbok.project.domain.service.ProjectDomainService;
import com.clusterra.pmbok.reference.domain.model.reference.Reference;
import com.clusterra.pmbok.reference.domain.service.ReferenceDomainService;
import com.clusterra.pmbok.term.domain.model.term.Term;
import com.clusterra.pmbok.term.domain.service.TermDomainService;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.ConfigFileApplicationContextInitializer;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.testng.AbstractTransactionalTestNGSpringContextTests;
import org.springframework.test.context.web.WebAppConfiguration;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.util.Date;

import static org.apache.commons.lang3.RandomStringUtils.randomAlphabetic;
import static org.fest.assertions.api.Assertions.assertThat;

/**
 * Created with IntelliJ IDEA.
 *
 * @author Denis Kuchugurov
 *         Date: 07.12.13
 */
@ContextConfiguration(
        value = {"classpath*:META-INF/spring/*.xml"},
        initializers = ConfigFileApplicationContextInitializer.class)
@WebAppConfiguration
public class DocumentPdfServiceTest extends AbstractTransactionalTestNGSpringContextTests {

    @Autowired
    private DocumentPdfService documentPdfService;

    @Autowired
    private DocumentDomainService documentDomainService;

    @Autowired
    private ProjectDomainService projectDomainService;

    private ProjectId projectId;

    protected DocumentId documentId;

    @Autowired
    private String tmpPath;

    @Autowired
    private AvatarService avatarService;

    @Autowired
    private ReferenceDomainService referenceDomainService;

    @Autowired
    private TermDomainService termDomainService;

    @Autowired
    private ProjectDomainQueryService projectDomainQueryService;

    @Autowired
    private TemplateDomainService templateDomainService;

    @Autowired
    private ReferenceAssociationService referenceAssociationService;

    @Autowired
    private TermAssociationService termAssociationService;

    @Autowired
    private TenantCommandService tenantCommandService;

    protected TenantId tenantId;

    @Autowired
    private UserCommandService userCommandService;

    @Autowired
    private RoleService roleService;

    @Autowired
    private GroupService groupService;

    @Autowired
    private AuthorizedMembershipService authorizedMembershipService;

    @Autowired
    private IdentityTrackerLifeCycle identityTrackerLifeCycle;

    @BeforeMethod
    public void beforeMethod() throws Exception {

        Tenant tenant = tenantCommandService.create(randomAlphabetic(10), UserRandomUtil.getRandomEmail());
        tenantId = new TenantId(tenant.getId());

        startTrackingUser();

        AvatarId avatarId = avatarService.findDefaultAvatarId(AvatarType.TENANT);
        tenantCommandService.updateAvatar(tenantId, avatarId.getId());
        Project project = projectDomainService.createProject(tenantId, randomAlphabetic(5));
        projectId = project.getProjectId();
        ProjectVersion projectVersion = projectDomainQueryService.findAllVersionsBy(projectId).iterator().next();

        Template charter = templateDomainService.createTemplate(tenantId, 0, 1, randomAlphabetic(10));


        Document document = documentDomainService.create(tenantId, projectVersion.getProjectVersionId(), charter.getTemplateId());
        documentId = document.getDocumentId();

        for (int i = 0; i < 10; i++) {
            Reference reference = referenceDomainService.create(tenantId, randomAlphabetic(5), randomAlphabetic(5), new Date(), randomAlphabetic(5));
            referenceAssociationService.createAssociation(tenantId, documentId, reference.getReferenceId());
        }

        templateDomainService.addTitleSection(charter.getTemplateId(), "TITLE");
        templateDomainService.addHistorySection(charter.getTemplateId(), "History title");
        templateDomainService.addReferenceSection(charter.getTemplateId(), "Reference title");
        templateDomainService.addTermSection(charter.getTemplateId(), "Term title");
        for (int i = 0; i < 10; i++) {
            Term term = termDomainService.create(tenantId, randomAlphabetic(5), randomAlphabetic(5));
            termAssociationService.createAssociation(tenantId, documentId, term.getTermId());
        }


        for (int i = 0; i < 10; i++) {
            SectionTemplate textTemplate = templateDomainService.addTextSection(charter.getTemplateId(), "text section " + i);
            String txt = new DateTime().toString(DateTimeFormat.fullDateTime()) + " => " + randomAlphabetic(2000);
            documentDomainService.updateTextSection(documentId, textTemplate.getSectionTemplateId(), txt);
        }

        documentDomainService.publish(documentId, randomAlphabetic(20));
        documentDomainService.publish(documentId, randomAlphabetic(20));
        documentDomainService.approve(documentId, randomAlphabetic(20));
        documentDomainService.publish(documentId, randomAlphabetic(20));
    }

    private void startTrackingUser() throws Exception {
        UserId userId = new UserId(userCommandService.create(tenantId, randomAlphabetic(5), UserRandomUtil.getRandomEmail(), UserRandomUtil.randomPassword(), randomAlphabetic(5), randomAlphabetic(5)).getId());

        RoleDescriptor role = roleService.createRole(tenantId, DefaultRole.ADMIN);
        GroupDescriptor group = groupService.createGroup(tenantId, randomAlphabetic(5));

        authorizedMembershipService.createAuthorizedMembershipIfNotExists(tenantId, userId, role, group);

        identityTrackerLifeCycle.startTracking(userId, tenantId);
    }


    @Test(enabled = true)
    public void test_generate_pdf() throws Exception {
        byte[] bytes = documentPdfService.generatePdf(documentId);
        assertThat(bytes).isNotEmpty();

        File directory = new File(tmpPath);

        System.out.println("...creating directory " + directory.getAbsolutePath());
        FileUtils.forceMkdir(directory);
        OutputStream os = new FileOutputStream(tmpPath + File.separator + DateTime.now().toString(DateTimeFormat.forPattern("yyyy.MM.dd.HH.mm.ss")) + ".pdf");
        os.write(bytes);
        IOUtils.closeQuietly(os);
    }
}
