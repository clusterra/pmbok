/*
 * Copyright (c) 2014.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * 	http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.clusterra.pmbok.doc.template.listen;

import com.clusterra.iam.core.application.tenant.TenantId;
import com.clusterra.iam.core.application.tenant.TenantNotFoundException;
import com.clusterra.iam.core.application.tenant.TenantQueryService;
import com.clusterra.pmbok.document.domain.model.template.SectionTemplateAlreadyExistsException;
import com.clusterra.pmbok.document.domain.model.template.SectionTemplateNotFoundException;
import com.clusterra.pmbok.document.domain.service.template.TemplateAlreadyExistsException;
import com.clusterra.pmbok.document.domain.service.template.TemplateNotFoundException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.util.Arrays;
import java.util.List;

/**
 * @author Denis Kuchugurov.
 *         14.07.2014.
 */
@Component
public class ContextRefreshedEvent4DefaultTemplatesListener implements ApplicationListener<ContextRefreshedEvent> {

    private static Logger logger = LoggerFactory.getLogger(ContextRefreshedEvent4DefaultTemplatesListener.class);

    @Autowired
    private TemplateFactory templateFactory;

    @Autowired
    private TenantQueryService tenantQueryService;

    @Transactional
    public void onApplicationEvent(ContextRefreshedEvent event) {
        List<String> tenantNames = Arrays.asList("simpsons", "futurama");
        for (String tenantName : tenantNames) {
            try {
                TenantId tenantId = new TenantId(tenantQueryService.findByName(tenantName).getId());
                createFromConfigs(tenantId);
            } catch (TenantNotFoundException e) {
                logger.debug("tenant not found by name {}", tenantNames);
            }
        }
    }

    private void createFromConfigs(TenantId tenantId) {
        try {
            templateFactory.createFromConfigs(tenantId);
        } catch (TemplateAlreadyExistsException | TemplateNotFoundException | SectionTemplateAlreadyExistsException | SectionTemplateNotFoundException e) {
            throw new RuntimeException(e);
        }
    }
}
