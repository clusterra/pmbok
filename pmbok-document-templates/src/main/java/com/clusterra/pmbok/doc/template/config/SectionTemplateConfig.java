/*
 * Copyright (c) 2014.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * 	http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.clusterra.pmbok.doc.template.config;

import com.clusterra.pmbok.document.domain.model.template.section.SectionType;

/**
 * Created by dkuchugurov on 02.04.2014.
 */
public class SectionTemplateConfig {

    private final SectionType type;

    private final String title;

    private final Integer orderIndex;

    public SectionTemplateConfig(SectionType type, String title, Integer orderIndex) {
        this.type = type;
        this.title = title;
        this.orderIndex = orderIndex;
    }

    public SectionType getType() {
        return type;
    }

    public String getTitle() {
        return title;
    }

    public Integer getOrderIndex() {
        return orderIndex;
    }
}
