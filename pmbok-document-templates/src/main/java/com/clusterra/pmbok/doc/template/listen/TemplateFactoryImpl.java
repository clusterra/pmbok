/*
 * Copyright (c) 2014.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * 	http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.clusterra.pmbok.doc.template.listen;

import com.clusterra.iam.core.application.tenant.TenantId;
import com.clusterra.pmbok.document.domain.model.template.SectionTemplateAlreadyExistsException;
import com.clusterra.pmbok.document.domain.model.template.section.SectionTemplate;
import com.clusterra.pmbok.document.domain.service.template.TemplateAlreadyExistsException;
import com.clusterra.pmbok.doc.template.config.SectionTemplateConfig;
import com.clusterra.pmbok.doc.template.config.TemplateConfig;
import com.clusterra.pmbok.document.domain.model.template.SectionTemplateNotFoundException;
import com.clusterra.pmbok.document.domain.model.template.Template;
import com.clusterra.pmbok.document.domain.service.template.TemplateDomainService;
import com.clusterra.pmbok.document.domain.service.template.TemplateNotFoundException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;


/**
 * @author Denis Kuchugurov.
 *         15.07.2014.
 */
@Component
public class TemplateFactoryImpl implements TemplateFactory {

    private static Logger logger = LoggerFactory.getLogger(TemplateFactoryImpl.class);

    @Autowired
    private TemplateDomainService templateDomainService;

    @Autowired
    private List<TemplateConfig> templateConfigs;

    public void createFromConfigs(TenantId tenantId) throws SectionTemplateAlreadyExistsException, TemplateNotFoundException, SectionTemplateNotFoundException, TemplateAlreadyExistsException {

        for (TemplateConfig templateConfig : templateConfigs) {

            Template template = templateDomainService.findByName(tenantId, templateConfig.getName());
            if (template != null) {
                continue;
            }
            template = templateDomainService.createTemplate(tenantId, 1, 0, templateConfig.getName());

            for (SectionTemplateConfig sectionTemplateConfig : templateConfig.getSectionTemplateConfigs()) {

                switch (sectionTemplateConfig.getType()) {

                    case SECTION_HISTORY:
                        SectionTemplate historySection = templateDomainService.addHistorySection(template.getTemplateId(), sectionTemplateConfig.getTitle());
                        templateDomainService.updateSectionOrder(template.getTemplateId(), historySection.getSectionTemplateId(), sectionTemplateConfig.getOrderIndex());
                        break;
                    case SECTION_REFERENCE:
                        SectionTemplate referenceSection = templateDomainService.addReferenceSection(template.getTemplateId(), sectionTemplateConfig.getTitle());
                        templateDomainService.updateSectionOrder(template.getTemplateId(), referenceSection.getSectionTemplateId(), sectionTemplateConfig.getOrderIndex());
                        break;
                    case SECTION_TERM:
                        SectionTemplate termSection = templateDomainService.addTermSection(template.getTemplateId(), sectionTemplateConfig.getTitle());
                        templateDomainService.updateSectionOrder(template.getTemplateId(), termSection.getSectionTemplateId(), sectionTemplateConfig.getOrderIndex());
                        break;
                    case SECTION_TEXT:
                        SectionTemplate textSection = templateDomainService.addTextSection(template.getTemplateId(), sectionTemplateConfig.getTitle());
                        templateDomainService.updateSectionOrder(template.getTemplateId(), textSection.getSectionTemplateId(), sectionTemplateConfig.getOrderIndex());
                        break;
                    case SECTION_TITLE:
                        SectionTemplate titleSection = templateDomainService.addTitleSection(template.getTemplateId(), sectionTemplateConfig.getTitle());
                        templateDomainService.updateSectionOrder(template.getTemplateId(), titleSection.getSectionTemplateId(), sectionTemplateConfig.getOrderIndex());
                        break;
                    case SECTION_CONTENTS:
                        SectionTemplate contentsSection = templateDomainService.addTocSection(template.getTemplateId(), sectionTemplateConfig.getTitle());
                        templateDomainService.updateSectionOrder(template.getTemplateId(), contentsSection.getSectionTemplateId(), sectionTemplateConfig.getOrderIndex());
                        break;
                    default:
                        throw new IllegalArgumentException("unsupported section type " + sectionTemplateConfig.getType());
                }
            }
            template.markReady();
            logger.info("demo document template {} created for tenant {}", template.getName(), tenantId.getId());
        }
    }
}
