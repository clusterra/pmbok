/*
 * Copyright (c) 2014.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * 	http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.clusterra.pmbok.doc.template.listen;

import com.clusterra.iam.core.application.tenant.event.TenantActivatedEvent;
import com.clusterra.pmbok.document.domain.model.template.SectionTemplateAlreadyExistsException;
import com.clusterra.pmbok.document.domain.service.template.TemplateAlreadyExistsException;
import com.clusterra.pmbok.document.domain.model.template.SectionTemplateNotFoundException;
import com.clusterra.pmbok.document.domain.service.template.TemplateNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationListener;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

/**
 * @author Denis Kuchugurov.
 *         14.07.2014.
 */
@Component
public class TenantActivatedEvent4DefaultTemplatesListener implements ApplicationListener<TenantActivatedEvent> {

    @Autowired
    private TemplateFactory templateFactory;

    @Transactional
    public void onApplicationEvent(TenantActivatedEvent event) {
        try {
            templateFactory.createFromConfigs(event.getTenantId());
        } catch (TemplateAlreadyExistsException | TemplateNotFoundException | SectionTemplateAlreadyExistsException | SectionTemplateNotFoundException e) {
            throw new RuntimeException(e);
        }
    }
}
