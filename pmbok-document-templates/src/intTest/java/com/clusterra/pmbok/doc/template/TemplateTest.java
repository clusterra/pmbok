/*
 * Copyright 2014 www.equmo.org
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * 	http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.clusterra.pmbok.doc.template;

import com.clusterra.iam.core.application.tenant.TenantCommandService;
import com.clusterra.iam.core.application.tenant.TenantId;
import com.clusterra.iam.core.domain.model.tenant.Tenant;
import com.clusterra.iam.core.testutil.UserRandomUtil;
import com.clusterra.pmbok.document.domain.model.document.Document;
import com.clusterra.pmbok.document.domain.model.document.DocumentId;
import com.clusterra.pmbok.document.domain.model.document.section.PersistedSectionContent;
import com.clusterra.pmbok.document.domain.model.document.section.repo.PersistedSectionContentRepository;
import com.clusterra.pmbok.document.domain.model.template.Template;
import com.clusterra.pmbok.document.domain.model.template.TemplateId;
import com.clusterra.pmbok.document.domain.service.document.DocumentDomainService;
import com.clusterra.pmbok.document.domain.service.template.TemplateDomainService;
import com.clusterra.pmbok.project.domain.model.Project;
import com.clusterra.pmbok.project.domain.model.ProjectVersion;
import com.clusterra.pmbok.project.domain.service.ProjectDomainService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.testng.AbstractTransactionalTestNGSpringContextTests;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import static org.apache.commons.lang3.RandomStringUtils.randomAlphabetic;

/**
 * Created with IntelliJ IDEA.
 *
 * @author Denis Kuchugurov
 *         Date: 07.12.13
 */
@ContextConfiguration({
        "classpath*:META-INF/spring/*.xml"
})
public class TemplateTest extends AbstractTransactionalTestNGSpringContextTests {

    @Autowired
    private DocumentDomainService documentDomainService;

    @Autowired
    private ProjectDomainService projectDomainService;

    @Autowired
    private PersistedSectionContentRepository<PersistedSectionContent> sectionRepository;

    @Autowired
    private TemplateDomainService templateDomainService;

    private DocumentId documentId;

    @Autowired
    private TenantCommandService tenantCommandService;

    private TenantId tenantId;


    @BeforeMethod
    public void beforeMethod() throws Exception {
        Tenant tenant = tenantCommandService.create(randomAlphabetic(10), UserRandomUtil.getRandomEmail());
        tenantId = new TenantId(tenant.getId());

        Project project = projectDomainService.createProject(tenantId, randomAlphabetic(5));
        ProjectVersion projectVersion = projectDomainService.createVersion(tenantId, project.getProjectId(), randomAlphabetic(5), 2, true);

        Template charter = templateDomainService.createTemplate(tenantId, 0, 1, randomAlphabetic(10));
        TemplateId templateId = charter.getTemplateId();
        templateDomainService.addHistorySection(templateId, randomAlphabetic(5));
        templateDomainService.addTermSection(templateId, randomAlphabetic(5));
        templateDomainService.addReferenceSection(templateId, randomAlphabetic(5));
        templateDomainService.addTextSection(templateId, randomAlphabetic(5));
        templateDomainService.addTextSection(templateId, randomAlphabetic(5));
        templateDomainService.addTextSection(templateId, randomAlphabetic(5));
        templateDomainService.addTextSection(templateId, randomAlphabetic(5));

        Document document = documentDomainService.create(tenantId, projectVersion.getProjectVersionId(), charter.getTemplateId());
        documentId = document.getDocumentId();
    }

    @Test(enabled = false)
    public void test_sections() throws Exception {
//        List<Section> sections = sectionRepository.findBy(documentId.getId());
//        assertThat(sections).hasSize(7);
    }

}
