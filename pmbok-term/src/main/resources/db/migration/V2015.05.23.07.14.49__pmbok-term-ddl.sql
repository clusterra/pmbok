
    create table pmb_term (
        id varchar(255) not null,
        createdByUserId varchar(255),
        createdDate timestamp,
        description varchar(255) not null,
        modifiedByUserId varchar(255),
        modifiedDate timestamp,
        name varchar(255) not null,
        tenantId varchar(255) not null,
        primary key (id)
    );

    alter table pmb_term 
        add constraint UK_4w0y9r8am6jcndp17emxjido4  unique (tenantId, name, description);
