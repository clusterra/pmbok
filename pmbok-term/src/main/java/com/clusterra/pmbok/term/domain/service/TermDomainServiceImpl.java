/*
 * Copyright (c) 2014.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * 	http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.clusterra.pmbok.term.domain.service;

import com.clusterra.pmbok.term.domain.model.term.Term;
import com.clusterra.pmbok.term.domain.model.term.TermId;
import com.clusterra.pmbok.term.domain.model.term.repo.TermRepository;
import com.clusterra.pmbok.term.domain.model.term.repo.TermSearchBySpecification;
import com.clusterra.pmbok.term.domain.model.term.repo.TermTenantSpecification;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.Validate;
import com.clusterra.iam.core.application.tenant.TenantId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.jpa.domain.Specifications;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

/**
 * Created by dkuchugurov on 27.06.2014.
 */
@Service
@Transactional(propagation = Propagation.MANDATORY)
public class TermDomainServiceImpl implements TermDomainService {

    @Autowired
    private TermRepository termRepository;

    public Term create(TenantId tenantId, String name, String description) {
        Validate.notNull(tenantId);
        Term term = new Term(tenantId, name, description);
        return termRepository.save(term);
    }

    public Term update(TermId termId, String name, String description) throws TermNotFoundException {
        Validate.notNull(termId);
        Term term = termRepository.findOne(termId.getId());
        if (term == null) {
            throw new TermNotFoundException(termId);
        }
        term.setDescription(description);
        term.setName(name);
        return termRepository.save(term);
    }

    public void delete(TermId termId) {
        termRepository.delete(termId.getId());
    }

    public Page<Term> findBy(TenantId tenantId, Pageable pageable, String searchBy) {
        Validate.notNull(tenantId);
        Specification<Term> tenantSpec = new TermTenantSpecification(tenantId.getId());
        Specifications<Term> specifications = Specifications.where(tenantSpec);

        if (!StringUtils.isEmpty(searchBy)) {
            specifications = specifications.and(new TermSearchBySpecification(searchBy));
        }

        return termRepository.findAll(specifications, pageable);
    }


    public Term findBy(TermId termId) throws TermNotFoundException {
        Validate.notNull(termId);
        Term term = termRepository.findOne(termId.getId());
        if (term == null) {
            throw new TermNotFoundException(termId);
        }
        return term;
    }

}
