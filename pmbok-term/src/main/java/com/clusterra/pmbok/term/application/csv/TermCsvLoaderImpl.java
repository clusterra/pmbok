/*
 * Copyright 2014 www.equmo.org
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * 	http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.clusterra.pmbok.term.application.csv;

import com.clusterra.pmbok.term.application.TermService;
import com.fasterxml.jackson.databind.MappingIterator;
import com.fasterxml.jackson.databind.ObjectReader;
import com.fasterxml.jackson.dataformat.csv.CsvMapper;
import com.fasterxml.jackson.dataformat.csv.CsvSchema;
import com.clusterra.iam.core.application.tenant.TenantId;
import org.joda.time.format.DateTimeFormat;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.util.Date;

/**
 * Created with IntelliJ IDEA.
 *
 * @author Denis Kuchugurov
 *         Date: 07.02.14
 */
@Component
public class TermCsvLoaderImpl implements TermCsvLoader {

    private static final char CSV_SEPARATOR = ';';

    private static Logger logger = LoggerFactory.getLogger(TermCsvLoaderImpl.class);

    @Autowired
    private TermService termService;


    public void validateContent(byte[] csvContent) throws InvalidCsvTermContentException {
        try {
            MappingIterator<CsvMapperData> values = readValues(csvContent);
            values.next();
        } catch (Exception e) {
            throw new InvalidCsvTermContentException("Invalid csv format, " + e.getMessage());
        }
    }

    public void loadFromCsv(TenantId tenantId, byte[] csvContent) throws TermCsvLoaderException {
        MappingIterator<CsvMapperData> values = readValues(csvContent);

        while (values.hasNext()) {
            CsvMapperData data = values.next();
            termService.create(tenantId, data.getName(), data.getDescription());
        }

    }

    private Date parseDate(String dateString) throws TermCsvLoaderException {
        if (dateString == null || dateString.isEmpty()) {
            return null;
        }
        try {

            return DateTimeFormat.forPattern("YYYY").parseDateTime(dateString).toDate();
        } catch (IllegalArgumentException e) {
            throw new TermCsvLoaderException("unable to parse date " + dateString + ", only YYYY format is supported", e);
        }
    }

    private MappingIterator<CsvMapperData> readValues(byte[] csvContent) throws TermCsvLoaderException {
        CsvMapper csvMapper = new CsvMapper();
        CsvSchema csvSchema = csvMapper.schemaFor(CsvMapperData.class).
                withColumnSeparator(CSV_SEPARATOR).
                withSkipFirstDataRow(true).withHeader().
                withoutColumns();

        ObjectReader objectReader = csvMapper.reader(CsvMapperData.class).with(csvSchema);
        try {
            return objectReader.readValues(csvContent);
        } catch (IOException e) {
            throw new TermCsvLoaderException(e);
        }
    }
}
