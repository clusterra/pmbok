/*
 * Copyright (c) 2014.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * 	http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.clusterra.pmbok.term.domain.service;

import com.clusterra.iam.core.application.tenant.TenantId;
import com.clusterra.pmbok.term.domain.model.term.Term;
import com.clusterra.pmbok.term.domain.model.term.TermId;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

/**
 * Created by dkuchugurov on 27.06.2014.
 */
public interface TermDomainService {

    Term create(TenantId tenantId, String name, String description);

    Term update(TermId termId, String name, String description) throws TermNotFoundException;

    void delete(TermId termId);

    Term findBy(TermId termId) throws TermNotFoundException;

    Page<Term> findBy(TenantId tenantId, Pageable pageable, String searchBy);

}
