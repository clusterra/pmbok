/*
 * Copyright 2014 www.equmo.org
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * 	http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.clusterra.pmbok.term.domain.service;

import com.clusterra.iam.core.application.tenant.TenantCommandService;
import com.clusterra.iam.core.application.tenant.TenantId;
import com.clusterra.iam.core.domain.model.tenant.Tenant;
import com.clusterra.iam.core.testutil.UserRandomUtil;
import com.clusterra.pmbok.term.domain.model.term.Term;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.ConfigFileApplicationContextInitializer;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.testng.AbstractTransactionalTestNGSpringContextTests;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.util.List;

import static org.apache.commons.lang3.RandomStringUtils.randomAlphabetic;
import static org.fest.assertions.api.Assertions.assertThat;

/**
 * Created with IntelliJ IDEA.
 *
 * @author Denis Kuchugurov
 *         Date: 05.02.14
 */
@ContextConfiguration(
        value = {"classpath*:META-INF/spring/*.xml"},
        initializers = ConfigFileApplicationContextInitializer.class)
public class TermDomainServiceTest extends AbstractTransactionalTestNGSpringContextTests {

    @Autowired
    private TermDomainService termDomainService;

    private Term term1;

    @Autowired
    private TenantCommandService tenantCommandService;

    private TenantId tenantId;

    @BeforeMethod
    public void beforeMethod() throws Exception {
        Tenant tenant = tenantCommandService.create(randomAlphabetic(10), UserRandomUtil.getRandomEmail());
        tenantId = new TenantId(tenant.getId());
        term1 = termDomainService.create(tenantId, randomAlphabetic(5), randomAlphabetic(5));
    }

    @Test
    public void test_find_all() throws Exception {
        Page<Term> list = termDomainService.findBy(tenantId, new PageRequest(0, 100), "");
        assertThat(list).isNotEmpty();
        Term one = termDomainService.findBy(term1.getTermId());
        assertThat(list).contains(one);
    }

    @Test
    public void test_find_by_ids() throws Exception {
        Term id1 = termDomainService.create(tenantId, randomAlphabetic(5), randomAlphabetic(5));
        Term id2 = termDomainService.create(tenantId, randomAlphabetic(5), randomAlphabetic(5));
        Term id3 = termDomainService.create(tenantId, randomAlphabetic(5), randomAlphabetic(5));

        List<Term> list = termDomainService.findBy(tenantId, new PageRequest(0, 100), "").getContent();
        assertThat(list).isNotEmpty();
        assertThat(list).hasSize(4);
    }

    @Test
    public void test_update() throws Exception {
        Term term_1 = termDomainService.findBy(term1.getTermId());
        String oldName = term_1.getName();
        String oldDescription = term_1.getDescription();

        String name = randomAlphabetic(5);
        String description = randomAlphabetic(5);
        termDomainService.update(term_1.getTermId(), name, description);

        Term term_2 = termDomainService.findBy(term1.getTermId());
        assertThat(term_1.getTermId()).isEqualTo(term_2.getTermId());
        assertThat(oldName).isNotEqualTo(term_2.getName());
        assertThat(oldDescription).isNotEqualTo(term_2.getDescription());
    }

    @Test(expectedExceptions = TermNotFoundException.class)
    public void test_delete() throws Exception {
        Term r_1 = termDomainService.findBy(term1.getTermId());
        assertThat(r_1).isNotNull();

        termDomainService.delete(term1.getTermId());

        termDomainService.findBy(term1.getTermId());
    }

    @Test(enabled = false, expectedExceptions = DataIntegrityViolationException.class)
    public void test_create_violating_unique() throws Exception {
        Term term = termDomainService.findBy(term1.getTermId());

        Term term2 = termDomainService.create(tenantId, term.getName(), term.getDescription());
        termDomainService.create(tenantId, term2.getName(), term.getDescription());
    }

}
