/*
 * Copyright 2014 www.equmo.org
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * 	http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.clusterra.pmbok.term.application.csv;

import com.clusterra.iam.core.application.group.GroupDescriptor;
import com.clusterra.iam.core.application.group.GroupService;
import com.clusterra.iam.core.application.membership.AuthorizedMembershipService;
import com.clusterra.iam.core.application.role.RoleDescriptor;
import com.clusterra.iam.core.application.role.RoleService;
import com.clusterra.iam.core.application.tenant.TenantCommandService;
import com.clusterra.iam.core.application.tenant.TenantId;
import com.clusterra.iam.core.application.tracker.IdentityTrackerLifeCycle;
import com.clusterra.iam.core.application.user.DefaultRole;
import com.clusterra.iam.core.application.user.UserCommandService;
import com.clusterra.iam.core.application.user.UserId;
import com.clusterra.iam.core.domain.model.tenant.Tenant;
import com.clusterra.iam.core.testutil.UserRandomUtil;
import com.clusterra.pmbok.term.application.TermService;
import com.clusterra.pmbok.term.domain.model.term.Term;
import org.apache.commons.io.IOUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.ConfigFileApplicationContextInitializer;
import org.springframework.core.io.Resource;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.testng.AbstractTestNGSpringContextTests;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import static org.apache.commons.lang3.RandomStringUtils.randomAlphabetic;
import static org.fest.assertions.api.Assertions.assertThat;

/**
 * Created with IntelliJ IDEA.
 *
 * @author Denis Kuchugurov
 *         Date: 07.02.14
 */
@ContextConfiguration(
        value = {"classpath*:META-INF/spring/*.xml"},
        initializers = ConfigFileApplicationContextInitializer.class)
public class TermCsvLoaderTest extends AbstractTestNGSpringContextTests {

    @Autowired
    private Resource termCsvResource;

    @Autowired
    private Resource invalidTermCsvResource;

    @Autowired
    private TermCsvLoader termCsvLoader;

    @Autowired
    private TermService termService;

    @Autowired
    private TenantCommandService tenantCommandService;

    private TenantId tenantId;

    @Autowired
    private UserCommandService userCommandService;

    @Autowired
    private RoleService roleService;

    @Autowired
    private GroupService groupService;

    @Autowired
    private AuthorizedMembershipService authorizedMembershipService;

    @Autowired
    private IdentityTrackerLifeCycle identityTrackerLifeCycle;


    @BeforeMethod
    public void before() throws Exception {
        Tenant tenant = tenantCommandService.create(randomAlphabetic(10), UserRandomUtil.getRandomEmail());
        tenantId = new TenantId(tenant.getId());
        startTrackingUser();
    }

    private void startTrackingUser() throws Exception {
        UserId userId = new UserId(userCommandService.create(tenantId, randomAlphabetic(5), UserRandomUtil.getRandomEmail(), UserRandomUtil.randomPassword(), randomAlphabetic(5), randomAlphabetic(5)).getId());

        RoleDescriptor role = roleService.createRole(tenantId, DefaultRole.ADMIN);
        GroupDescriptor group = groupService.createGroup(tenantId, randomAlphabetic(5));

        authorizedMembershipService.createAuthorizedMembershipIfNotExists(tenantId, userId, role, group);

        identityTrackerLifeCycle.startTracking(userId, tenantId);
    }


    @Test
    public void testLoadFromCsvStream() throws Exception {
        Page<Term> before = termService.findBy(new PageRequest(0, 100), "");
        assertThat(before.getContent()).isEmpty();

        termCsvLoader.loadFromCsv(tenantId, IOUtils.toByteArray(termCsvResource.getInputStream()));

        Page<Term> after = termService.findBy(new PageRequest(0, 100), "");
        assertThat(after.getContent()).isNotEmpty();
    }

    @Test
    public void test_valid() throws Exception {
        termCsvLoader.validateContent(IOUtils.toByteArray(termCsvResource.getInputStream()));
    }

    @Test(expectedExceptions = InvalidCsvTermContentException.class)
    public void test_invalid() throws Exception {
        termCsvLoader.validateContent(IOUtils.toByteArray(invalidTermCsvResource.getInputStream()));
    }

}
