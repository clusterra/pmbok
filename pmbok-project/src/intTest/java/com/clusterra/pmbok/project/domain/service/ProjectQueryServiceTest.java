/*
 * Copyright (c) 2014.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * 	http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.clusterra.pmbok.project.domain.service;

import com.clusterra.pmbok.project.domain.model.ProjectVersion;
import com.clusterra.pmbok.project.domain.model.ProjectVersionId;
import com.clusterra.pmbok.project.domain.AbstractProjectTransactionalTest;
import com.clusterra.pmbok.project.domain.model.Project;
import org.springframework.data.domain.PageRequest;
import org.testng.annotations.Test;

import java.util.List;

import static org.apache.commons.lang3.RandomStringUtils.randomAlphabetic;
import static org.fest.assertions.api.Assertions.assertThat;

/**
 * Created with IntelliJ IDEA.
 *
 * @author Denis Kuchugurov
 *         Date: 03.12.13
 */
public class ProjectQueryServiceTest extends AbstractProjectTransactionalTest {


    @Test
    public void test_find_all() throws Exception {

        projectDomainService.createProject(tenantId, randomAlphabetic(5));
        projectDomainService.createProject(tenantId, randomAlphabetic(5));

        List<Project> all = projectDomainQueryService.findBy(tenantId, new PageRequest(0, 10), null).getContent();
        assertThat(all.size()).isGreaterThanOrEqualTo(2);
    }

    @Test
    public void test_find() throws Exception {
        Project p = projectDomainQueryService.findBy(projectId);
        assertThat(p).isNotNull();
    }

    @Test
    public void test_find_project_version() throws Exception {
        ProjectVersion lastProjectVersion = projectDomainQueryService.findAllVersionsBy(projectId).iterator().next();
        ProjectVersion projectVersion = projectDomainQueryService.findVersionBy(lastProjectVersion.getProjectVersionId());
        assertThat(projectVersion).isEqualTo(lastProjectVersion);
    }

    @Test(expectedExceptions = ProjectVersionNotFoundException.class)
    public void test_find_project_version_throws_exception() throws Exception {
        projectDomainQueryService.findVersionBy(new ProjectVersionId(randomAlphabetic(5)));
    }

}
