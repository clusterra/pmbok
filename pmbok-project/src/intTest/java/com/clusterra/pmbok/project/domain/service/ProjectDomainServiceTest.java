/*
 * Copyright (c) 2014.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * 	http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.clusterra.pmbok.project.domain.service;

import com.clusterra.pmbok.project.domain.model.ProjectVersionId;
import com.clusterra.pmbok.project.domain.AbstractProjectTransactionalTest;
import com.clusterra.pmbok.project.domain.model.Project;
import com.clusterra.pmbok.project.domain.model.ProjectVersion;
import org.fest.assertions.core.Condition;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.dao.DataIntegrityViolationException;
import org.testng.annotations.Test;

import java.util.Comparator;
import java.util.LinkedList;
import java.util.List;

import static org.apache.commons.lang3.RandomStringUtils.randomAlphabetic;
import static org.fest.assertions.api.Assertions.assertThat;
import static org.fest.assertions.api.Assertions.fail;

public class ProjectDomainServiceTest extends AbstractProjectTransactionalTest {


    @Autowired
    private ApplicationEventPublisher publisher;

    @Test
    public void test_create_version_has_1_main() throws Exception {
        List<ProjectVersion> versions = projectDomainQueryService.findAllVersionsBy(projectId);
        assertThat(versions).hasSize(1);
        assertThat(versions.get(0).isMain()).isTrue();
        assertThat(versions.get(0).getValue()).isEqualTo(1);

        projectDomainService.createVersion(tenantId, projectId, randomAlphabetic(5), 3, false);
        projectDomainService.createVersion(tenantId, projectId, randomAlphabetic(5), 4, false);
        ProjectVersion version = projectDomainService.createVersion(tenantId, projectId, randomAlphabetic(5), 5, true);
        projectDomainService.createVersion(tenantId, projectId, randomAlphabetic(5), 6, false);

        versions = projectDomainQueryService.findAllVersionsBy(projectId);
        assertThat(versions).hasSize(5);
        assertThat(versions).haveExactly(1, new Condition<ProjectVersion>() {
            @Override
            public boolean matches(ProjectVersion value) {
                return value.isMain();
            }
        });
        assertThat(projectDomainQueryService.findVersionBy(version.getProjectVersionId()).isMain()).isTrue();
    }

    @Test
    public void testSetVersionAsMain() throws Exception {
        List<ProjectVersion> versions = projectDomainQueryService.findAllVersionsBy(projectId);
        assertThat(versions).hasSize(1);
        ProjectVersion v2 = projectDomainService.createVersion(tenantId, projectId, randomAlphabetic(5), 3, true);
        ProjectVersion v3 = projectDomainService.createVersion(tenantId, projectId, randomAlphabetic(5), 4, false);
        ProjectVersion v4 = projectDomainService.createVersion(tenantId, projectId, randomAlphabetic(5), 5, false);
        ProjectVersion v5 = projectDomainService.createVersion(tenantId, projectId, randomAlphabetic(5), 6, false);
        versions = projectDomainQueryService.findAllVersionsBy(projectId);

        assertThat(versions).areExactly(1, new Condition<ProjectVersion>() {
            @Override
            public boolean matches(ProjectVersion value) {
                return value.isMain();
            }
        });
        assertThat(projectDomainQueryService.findVersionBy(v2.getProjectVersionId()).isMain()).isTrue();

        projectDomainService.setVersionAsMain(v3.getProjectVersionId());

        versions = projectDomainQueryService.findAllVersionsBy(projectId);
        assertThat(versions).areExactly(1, new Condition<ProjectVersion>() {
            @Override
            public boolean matches(ProjectVersion value) {
                return value.isMain();
            }
        });
        assertThat(projectDomainQueryService.findVersionBy(v3.getProjectVersionId()).isMain()).isTrue();
    }

    @Test
    public void test_delete_project_deletes_all_versions() throws Exception {
        ProjectVersion v2 = projectDomainService.createVersion(tenantId, projectId, randomAlphabetic(5), 3, true);
        ProjectVersion v3 = projectDomainService.createVersion(tenantId, projectId, randomAlphabetic(5), 4, false);
        ProjectVersion v4 = projectDomainService.createVersion(tenantId, projectId, randomAlphabetic(5), 5, false);

        projectDomainService.deleteProject(projectId);

        assertThat(projectDomainQueryService.findAllVersionsBy(projectId)).isEmpty();

    }

    @Test(enabled = false, expectedExceptions = DataIntegrityViolationException.class)
    public void test_validation_name_unique() throws Exception {
        Project project = projectDomainService.createProject(tenantId, randomAlphabetic(5));
        projectDomainService.createProject(tenantId, project.getName());
    }

    @Test(enabled = false)
    public void test_validation_name_size() throws Exception {
        projectDomainService.createProject(tenantId, randomAlphabetic(5));
    }

    @Test(expectedExceptions = ProjectNotFoundException.class)
    public void test_delete_project() throws Exception {
        projectDomainQueryService.findBy(projectId);
        projectDomainService.deleteProject(projectId);
        projectDomainQueryService.findBy(projectId);
    }

    @Test
    public void test_add_version() throws Exception {
        List<ProjectVersion> projectVersions = projectDomainQueryService.findAllVersionsBy(projectId);

        assertThat(projectVersions).hasSize(1);

        ProjectVersion projectVersion2 = projectDomainService.createVersion(tenantId, projectId, randomAlphabetic(5), 3, false);
        ProjectVersion projectVersion3 = projectDomainService.createVersion(tenantId, projectId, randomAlphabetic(5), 4, false);

        projectVersions = projectDomainQueryService.findAllVersionsBy(projectId);

        assertThat(projectVersions).hasSize(3);
    }

    @Test(expectedExceptions = IncorrectProjectVersionException.class)
    public void test_add_version_validate_max() throws Exception {
        List<ProjectVersion> projectVersions = projectDomainQueryService.findAllVersionsBy(projectId);

        assertThat(projectVersions).hasSize(1);
        assertThat(projectVersions.get(0).getValue()).isEqualTo(1);

        ProjectVersion projectVersion3 = projectDomainService.createVersion(tenantId, projectId, randomAlphabetic(5), 1, false);
    }

    @Test
    public void test_project_version_create_as_main() throws Exception {
        List<ProjectVersion> versions = projectDomainQueryService.findAllVersionsBy(projectId);
        assertThat(versions).hasSize(1);
        assertThat(versions.get(0).isMain()).isTrue();
        ProjectVersion main = projectDomainService.createVersion(tenantId, projectId, randomAlphabetic(5), 5, true);
        versions = projectDomainQueryService.findAllVersionsBy(projectId);
        assertThat(versions).hasSize(2);
        assertThat(versions.get(0).isMain()).isFalse();
        assertThat(versions.get(1).isMain()).isTrue();
        assertThat(versions.get(1)).isEqualTo(main);
    }


    @Test
    public void test_add_version_label_is_required() throws Exception {
        projectDomainService.createVersion(tenantId, projectId, "", 3, false);
        Project p = projectDomainQueryService.findBy(projectId);
        LinkedList<ProjectVersion> versions = (LinkedList<ProjectVersion>) projectDomainQueryService.findAllVersionsBy(p.getProjectId());
        assertThat(versions.getLast().getLabel()).isNotNull();
    }

    @Test
    public void test_get_versions_order() throws Exception {
        projectDomainService.createVersion(tenantId, projectId, "2", 3, false);
        projectDomainService.createVersion(tenantId, projectId, "3", 4, false);
        projectDomainService.createVersion(tenantId, projectId, "4", 5, false);
        List<ProjectVersion> versions = projectDomainQueryService.findAllVersionsBy(projectId);
        assertThat(versions).isSortedAccordingTo(new Comparator<ProjectVersion>() {
            @Override
            public int compare(ProjectVersion o1, ProjectVersion o2) {
                return o1.getCreatedDate().compareTo(o2.getCreatedDate());
            }
        });
    }

    @Test
    public void test_delete() throws Exception {
        ProjectVersionId projectVersionId = projectDomainService.createVersion(tenantId, projectId, "2", 3, false).getProjectVersionId();

        projectDomainService.deleteVersion(projectVersionId);

        try {
            projectDomainQueryService.findVersionBy(projectVersionId);
            fail("projectVersion should have been deleted " + projectVersionId);
        } catch (ProjectVersionNotFoundException e) {

        }
    }

}