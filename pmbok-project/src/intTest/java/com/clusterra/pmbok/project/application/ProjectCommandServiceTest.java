/*
 * Copyright (c) 2014.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * 	http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.clusterra.pmbok.project.application;

import com.clusterra.pmbok.project.domain.AbstractProjectTransactionalTest;
import com.clusterra.pmbok.project.domain.model.Project;
import com.clusterra.pmbok.project.domain.model.stakeholder.Stakeholder;
import com.clusterra.pmbok.project.domain.model.stakeholder.StakeholderClassification;
import com.clusterra.pmbok.project.domain.model.stakeholder.StakeholderRole;
import com.clusterra.pmbok.project.domain.model.stakeholder.StakeholderType;
import org.fest.assertions.api.Assertions;
import org.springframework.beans.factory.annotation.Autowired;
import org.testng.annotations.Test;

import static org.apache.commons.lang3.RandomStringUtils.randomAlphabetic;
import static org.fest.assertions.api.Assertions.assertThat;


public class ProjectCommandServiceTest extends AbstractProjectTransactionalTest {

    @Autowired
    private ProjectCommandService projectCommandService;

    @Autowired
    private ProjectQueryService projectQueryService;

    @Test
    public void testAddStakeholder() throws Exception {
        Assertions.assertThat(projectQueryService.findBy(projectId).getStakeholders()).isEmpty();
        projectCommandService.addStakeholder(projectId, new Stakeholder(tenantId, randomAlphabetic(10), StakeholderType.EXTERNAL, StakeholderRole.CUSTOMER, StakeholderClassification.NEUTRAL));
        Assertions.assertThat(projectQueryService.findBy(projectId).getStakeholders()).hasSize(1);
    }

    @Test
    public void testRemoveStakeholder() throws Exception {
        Assertions.assertThat(projectQueryService.findBy(projectId).getStakeholders()).isEmpty();
        projectCommandService.addStakeholder(projectId, new Stakeholder(tenantId, randomAlphabetic(10), StakeholderType.EXTERNAL, StakeholderRole.CUSTOMER, StakeholderClassification.NEUTRAL));
        Project project = projectQueryService.findBy(projectId);
        assertThat(project.getStakeholders()).hasSize(1);
        projectCommandService.removeStakeholder(projectId, project.getStakeholders().get(0));
        Assertions.assertThat(projectQueryService.findBy(projectId).getStakeholders()).isEmpty();

    }
}