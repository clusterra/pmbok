/*
 * Copyright (c) 2014.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * 	http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.clusterra.pmbok.project.domain.service;

import com.clusterra.pmbok.project.domain.model.ProjectVersionId;
import com.clusterra.pmbok.project.domain.model.repo.ProjectRepository;
import com.clusterra.pmbok.project.domain.model.repo.ProjectTenantSpecification;
import com.clusterra.pmbok.project.domain.model.repo.ProjectVersionRepository;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.Validate;
import com.clusterra.iam.core.application.tenant.TenantId;
import com.clusterra.pmbok.project.domain.model.Project;
import com.clusterra.pmbok.project.domain.model.ProjectId;
import com.clusterra.pmbok.project.domain.model.ProjectVersion;
import com.clusterra.pmbok.project.domain.model.repo.ProjectSearchBySpecification;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.jpa.domain.Specifications;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * Created with IntelliJ IDEA.
 *
 * @author Denis Kuchugurov
 *         Date: 03.12.13
 */
@Service
@Transactional(propagation = Propagation.MANDATORY)
public class ProjectDomainQueryServiceImpl implements ProjectDomainQueryService {

    @Autowired
    private ProjectRepository projectRepository;

    @Autowired
    private ProjectVersionRepository projectVersionRepository;

    public Project findBy(ProjectId projectId) throws ProjectNotFoundException {
        Validate.notNull(projectId);
        Project project = projectRepository.findOne(projectId.getId());
        if (project == null) {
            throw new ProjectNotFoundException(projectId);
        }
        return project;
    }

    public Page<Project> findBy(TenantId tenantId, Pageable pageable, String searchBy) {
        Validate.notNull(tenantId);
        Specification<Project> tenantSpec = new ProjectTenantSpecification(tenantId.getId());
        Specifications<Project> specifications = Specifications.where(tenantSpec);

        if (!StringUtils.isEmpty(searchBy)) {
            specifications = specifications.and(new ProjectSearchBySpecification(searchBy));
        }
        return projectRepository.findAll(specifications, pageable);
    }

    public ProjectVersion findVersionBy(ProjectVersionId projectVersionId) throws ProjectVersionNotFoundException {
        Validate.notNull(projectVersionId);
        ProjectVersion projectVersion = projectVersionRepository.findOne(projectVersionId.getId());
        if (projectVersion == null) {
            throw new ProjectVersionNotFoundException(projectVersionId);
        }
        return projectVersion;
    }

    public List<ProjectVersion> findAllVersionsBy(ProjectId projectId) {
        Validate.notNull(projectId);
        return projectVersionRepository.findBy(projectId.getId());
    }

    public ProjectVersion findMainVersionsBy(ProjectId projectId) throws ProjectVersionNotFoundException {
        Validate.notNull(projectId);
        ProjectVersion projectVersion = projectVersionRepository.findMainBy(projectId.getId());
        if (projectVersion == null) {
            throw new ProjectVersionNotFoundException(projectId);
        }
        return projectVersion;
    }
}
