/*
 * Copyright (c) 2014.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * 	http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.clusterra.pmbok.project.domain.service;

import com.clusterra.pmbok.project.domain.model.ProjectId;
import com.clusterra.pmbok.project.domain.model.ProjectVersion;
import com.clusterra.pmbok.project.domain.model.ProjectVersionId;
import com.clusterra.pmbok.project.domain.model.event.ProjectDeletedEvent;
import com.clusterra.pmbok.project.domain.model.event.ProjectDeletingEvent;
import com.clusterra.pmbok.project.domain.model.event.ProjectVersionCreatedEvent;
import com.clusterra.pmbok.project.domain.model.event.ProjectVersionDeletedEvent;
import com.clusterra.pmbok.project.domain.model.repo.ProjectRepository;
import com.clusterra.pmbok.project.domain.model.repo.ProjectVersionRepository;
import org.apache.commons.lang3.Validate;
import com.clusterra.iam.core.application.tenant.TenantId;
import com.clusterra.pmbok.project.domain.model.Project;
import com.clusterra.pmbok.project.domain.model.event.ProjectCreatedEvent;
import com.clusterra.pmbok.project.domain.model.event.ProjectVersionDeletingEvent;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.LinkedList;

/**
 * Created with IntelliJ IDEA.
 *
 * @author Denis Kuchugurov
 *         Date: 03.12.13
 */
@Service
@Transactional(propagation = Propagation.MANDATORY)
public class ProjectDomainServiceImpl implements ProjectDomainService {

    @Autowired
    private ProjectRepository projectRepository;

    @Autowired
    private ProjectVersionRepository projectVersionRepository;

    @Autowired
    private ApplicationEventPublisher publisher;


    public Project createProject(TenantId tenantId, String name) {
        Project project = new Project(tenantId, name);
        projectRepository.save(project);
        projectVersionRepository.save(new ProjectVersion(tenantId, project, "initial", 1, true));
        publisher.publishEvent(new ProjectCreatedEvent(this, project.getProjectId()));
        return project;
    }

    public void deleteProject(ProjectId projectId) throws ProjectNotFoundException {
        Project project = projectRepository.findOne(projectId.getId());
        if (project == null) {
            throw new ProjectNotFoundException(projectId);
        }

        publisher.publishEvent(new ProjectDeletingEvent(this, projectId));
        projectRepository.delete(project);
        publisher.publishEvent(new ProjectDeletedEvent(this, projectId));
    }

    public ProjectVersion createVersion(TenantId tenantId, ProjectId projectId, String label, Integer value, Boolean main) throws ProjectNotFoundException, IncorrectProjectVersionException {

        Project project = findProject(projectId);

        LinkedList<ProjectVersion> versions = projectVersionRepository.findBy(projectId.getId());
        if (!versions.isEmpty()) {
            ProjectVersion max = versions.getLast();
            if (max.getValue() >= value) {
                throw new IncorrectProjectVersionException("specified value " + value + " must be greater than " + max.getValue());
            }

            if (main) {
                for (ProjectVersion version : versions) {
                    version.setMain(false);
                    projectVersionRepository.save(version);
                }
            }
        }

        ProjectVersion projectVersion = projectVersionRepository.save(new ProjectVersion(tenantId, project, label, value, main));
        publisher.publishEvent(new ProjectVersionCreatedEvent(this, projectVersion.getProjectVersionId()));
        return projectVersion;
    }

    private Project findProject(ProjectId projectId) throws ProjectNotFoundException {
        Project project = projectRepository.findOne(projectId.getId());
        if (project == null) {
            throw new ProjectNotFoundException(projectId);
        }
        return project;
    }

    public ProjectVersion setVersionAsMain(ProjectVersionId projectVersionId) throws ProjectVersionNotFoundException {
        ProjectVersion version = findVersion(projectVersionId);
        version.setMain(true);

        LinkedList<ProjectVersion> versions = projectVersionRepository.findBy(version.getProject().getProjectId().getId());
        for (ProjectVersion projectVersion : versions) {
            if (!projectVersion.getProjectVersionId().equals(projectVersionId)) {
                projectVersion.setMain(false);
            }
        }
        return version;
    }

    public void deleteAllVersions(ProjectId projectId) throws ProjectNotFoundException {
        Validate.notNull(projectId);
        Project project = findProject(projectId);
        LinkedList<ProjectVersion> versions = projectVersionRepository.findBy(project.getProjectId().getId());
        for (ProjectVersion version : versions) {
            delete(version.getProjectVersionId());
        }
    }

    public void deleteVersion(ProjectVersionId projectVersionId) throws ProjectVersionNotFoundException {
        Validate.notNull(projectVersionId);
        findVersion(projectVersionId);
        delete(projectVersionId);
    }

    private ProjectVersion findVersion(ProjectVersionId projectVersionId) throws ProjectVersionNotFoundException {
        ProjectVersion projectVersion = projectVersionRepository.findOne(projectVersionId.getId());
        if (projectVersion == null) {
            throw new ProjectVersionNotFoundException(projectVersionId);
        }
        return projectVersion;
    }

    private void delete(ProjectVersionId projectVersionId) {
        publisher.publishEvent(new ProjectVersionDeletingEvent(this, projectVersionId));
        projectVersionRepository.delete(projectVersionId.getId());
        publisher.publishEvent(new ProjectVersionDeletedEvent(this, projectVersionId));
    }

}
