/*
 * Copyright 2012 - 2013 www.equmo.org
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * 	http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.clusterra.pmbok.project.application;

import com.clusterra.iam.core.application.tracker.IdentityTracker;
import com.clusterra.iam.core.application.tracker.NotAuthenticatedException;
import com.clusterra.pmbok.project.domain.model.ProjectId;
import com.clusterra.pmbok.project.domain.model.ProjectVersion;
import com.clusterra.pmbok.project.domain.model.ProjectVersionId;
import com.clusterra.pmbok.project.domain.model.repo.ProjectRepository;
import com.clusterra.pmbok.project.domain.model.stakeholder.Stakeholder;
import com.clusterra.pmbok.project.domain.service.IncorrectProjectVersionException;
import com.clusterra.pmbok.project.domain.service.ProjectDomainService;
import com.clusterra.pmbok.project.domain.service.ProjectNotFoundException;
import com.clusterra.pmbok.project.domain.service.ProjectVersionNotFoundException;
import com.clusterra.pmbok.project.domain.model.Project;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.Transient;

/**
 * Created with IntelliJ IDEA.
 *
 * @author Denis Kuchugurov
 *         Date: 03.12.13
 */
@Service
public class ProjectCommandServiceImpl implements ProjectCommandService {

    @Autowired
    private ProjectDomainService service;

    @Autowired
    private IdentityTracker identityTracker;

    @Autowired
    private ProjectRepository projectRepository;

    @Transactional
    public Project createProject(String name) throws NotAuthenticatedException {
        return service.createProject(identityTracker.currentTenant(), name);
    }

    @Transactional
    public Project addStakeholder(ProjectId projectId, Stakeholder stakeholder) throws ProjectNotFoundException {
        Project project = findProject(projectId);
        project.addStakeholder(stakeholder);
        return project;
    }

    @Transient
    public Project removeStakeholder(ProjectId projectId, Stakeholder stakeholder) throws ProjectNotFoundException {
        Project project = findProject(projectId);
        project.removeStakeholder(stakeholder);
        return project;
    }

    private Project findProject(ProjectId projectId) throws ProjectNotFoundException {
        Project project = projectRepository.findOne(projectId.getId());
        if (project == null) {
            throw new ProjectNotFoundException(projectId);
        }
        return project;
    }

    @Transactional
    public void deleteProject(ProjectId projectId) throws ProjectNotFoundException {
        service.deleteProject(projectId);
    }

    @Transactional
    public ProjectVersion createVersion(ProjectId projectId, String label, Integer value, Boolean main) throws ProjectNotFoundException, IncorrectProjectVersionException, NotAuthenticatedException {
        return service.createVersion(identityTracker.currentTenant(), projectId, label, value, main);
    }

    @Transactional
    public ProjectVersion setVersionAsMain(ProjectVersionId projectVersionId) throws ProjectVersionNotFoundException {
        return service.setVersionAsMain(projectVersionId);
    }

    @Transactional
    public void deleteVersion(ProjectVersionId projectVersionId) throws ProjectVersionNotFoundException {
        service.deleteVersion(projectVersionId);
    }

}
