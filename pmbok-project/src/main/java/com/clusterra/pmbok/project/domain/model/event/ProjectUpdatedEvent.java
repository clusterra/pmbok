/*
 * Copyright (c) 2014.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * 	http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.clusterra.pmbok.project.domain.model.event;

import com.clusterra.pmbok.project.domain.model.ProjectId;
import org.springframework.context.ApplicationEvent;

/**
 * @author Denis Kuchugurov.
 *         01.08.2014.
 */
public class ProjectUpdatedEvent extends ApplicationEvent {

    private final ProjectId projectId;

    private final String message;

    public ProjectUpdatedEvent(Object source, ProjectId projectId, String message) {
        super(source);
        this.projectId = projectId;
        this.message = message;
    }

    public final ProjectId getProjectId() {
        return projectId;
    }

    public final String getMessage() {
        return message;
    }
}
