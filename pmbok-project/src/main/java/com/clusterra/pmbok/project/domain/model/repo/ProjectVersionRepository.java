/*
 * Copyright (c) 2014.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * 	http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.clusterra.pmbok.project.domain.model.repo;

import com.clusterra.pmbok.project.domain.model.ProjectVersion;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import java.util.LinkedList;

/**
 * Created by dkuchugurov on 10.05.2014.
 */
public interface ProjectVersionRepository extends CrudRepository<ProjectVersion, String> {

    @Query("select v from ProjectVersion v where v.project.id = ?1 order by v.createdDate")
    LinkedList<ProjectVersion> findBy(String projectId);

    @Query("select v from ProjectVersion v where v.project.id = ?1 and v.main = true")
    ProjectVersion findMainBy(String projectId);
}
