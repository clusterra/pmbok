/*
 * Copyright 2012 - 2013 www.equmo.org
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * 	http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.clusterra.pmbok.project.application;


import com.clusterra.iam.core.application.tracker.NotAuthenticatedException;
import com.clusterra.pmbok.project.domain.model.ProjectVersionId;
import com.clusterra.pmbok.project.domain.model.stakeholder.Stakeholder;
import com.clusterra.pmbok.project.domain.service.IncorrectProjectVersionException;
import com.clusterra.pmbok.project.domain.service.ProjectNotFoundException;
import com.clusterra.pmbok.project.domain.service.ProjectVersionNotFoundException;
import com.clusterra.pmbok.project.domain.model.Project;
import com.clusterra.pmbok.project.domain.model.ProjectId;
import com.clusterra.pmbok.project.domain.model.ProjectVersion;

/**
 * Created with IntelliJ IDEA.
 *
 * @author Denis Kuchugurov
 *         Date: 03.12.13
 */
public interface ProjectCommandService {

    Project createProject(String name) throws NotAuthenticatedException;

    Project addStakeholder(ProjectId projectId, Stakeholder stakeholder) throws ProjectNotFoundException;

    Project removeStakeholder(ProjectId projectId, Stakeholder stakeholder) throws ProjectNotFoundException;

    ProjectVersion createVersion(ProjectId projectId, String label, Integer value, Boolean main) throws ProjectNotFoundException, IncorrectProjectVersionException, NotAuthenticatedException;

    ProjectVersion setVersionAsMain(ProjectVersionId projectVersionId) throws ProjectVersionNotFoundException;

    void deleteProject(ProjectId projectId) throws ProjectNotFoundException;

    void deleteVersion(ProjectVersionId projectVersionId) throws ProjectVersionNotFoundException;
}
