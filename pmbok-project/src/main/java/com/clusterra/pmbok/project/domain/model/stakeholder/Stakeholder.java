/*
 * Copyright (c) 2014.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * 	http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.clusterra.pmbok.project.domain.model.stakeholder;

import org.apache.commons.lang3.Validate;
import com.clusterra.iam.core.application.tenant.TenantId;
import org.hibernate.annotations.GenericGenerator;
import org.springframework.data.annotation.CreatedBy;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedBy;
import org.springframework.data.annotation.LastModifiedDate;

import javax.persistence.*;
import java.util.Date;

/**
 * Created by kepkap on 15/11/14.
 */
@Entity
@Table(name = "pmb_stakeholder")

public class Stakeholder {

    @Id
    @GeneratedValue(generator = "base64")
    @GenericGenerator(name = "base64", strategy = "com.clusterra.hibernate.base64.Base64IdGenerator")
    private String id;

    @Basic
    @Column(nullable = false)
    private String tenantId;

    @Basic
    @CreatedBy
    private String createdByUserId;

    @Basic
    @CreatedDate
    private Date createdDate;

    @Basic
    @LastModifiedBy
    private String modifiedByUserId;

    @Basic
    @LastModifiedDate
    private Date modifiedDate;

    @Basic
    @Enumerated(EnumType.STRING)
    private StakeholderType type;

    @Basic
    @Enumerated(EnumType.STRING)
    private StakeholderRole role;

    @Basic
    @Enumerated(EnumType.STRING)
    private StakeholderClassification classification;

    @Basic
    private String name;


    Stakeholder() {
    }

    public Stakeholder(TenantId tenantId, String name, StakeholderType type, StakeholderRole role, StakeholderClassification classification) {
        Validate.notNull(tenantId);
        Validate.notNull(type);
        Validate.notNull(role);
        Validate.notNull(classification);
        Validate.notEmpty(name);
        this.type = type;
        this.role = role;
        this.name = name;
        this.classification = classification;
        this.tenantId = tenantId.getId();
    }

    public StakeholderType getType() {
        return type;
    }

    public StakeholderRole getRole() {
        return role;
    }

    public String getName() {
        return name;
    }

    public StakeholderClassification getClassification() {
        return classification;
    }

    public String getId() {
        return id;
    }

    public String getTenantId() {
        return tenantId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Stakeholder that = (Stakeholder) o;

        if (id != null ? !id.equals(that.id) : that.id != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        return id != null ? id.hashCode() : 0;
    }
}
