/*
 * Copyright 2014 www.equmo.org
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * 	http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.clusterra.pmbok.project.application;

import com.clusterra.iam.core.application.tenant.TenantId;
import com.clusterra.iam.core.application.tracker.IdentityTracker;
import com.clusterra.iam.core.application.tracker.NotAuthenticatedException;
import com.clusterra.pmbok.project.domain.model.Project;
import com.clusterra.pmbok.project.domain.model.ProjectId;
import com.clusterra.pmbok.project.domain.model.ProjectVersion;
import com.clusterra.pmbok.project.domain.model.ProjectVersionId;
import com.clusterra.pmbok.project.domain.service.ProjectDomainQueryService;
import com.clusterra.pmbok.project.domain.service.ProjectNotFoundException;
import com.clusterra.pmbok.project.domain.service.ProjectVersionNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * Created with IntelliJ IDEA.
 *
 * @author Denis Kuchugurov
 *         Date: 03.12.13
 */
@Service
public class ProjectQueryServiceImpl implements ProjectQueryService {

    @Autowired
    private ProjectDomainQueryService projectDomainQueryService;

    @Autowired
    private IdentityTracker identityTracker;


    @Transactional
    public Project findBy(ProjectId projectId) throws ProjectNotFoundException {
        return projectDomainQueryService.findBy(projectId);
    }

    @Transactional
    public Page<Project> findBy(Pageable pageable, String searchBy) throws NotAuthenticatedException {
        TenantId tenantId = identityTracker.currentTenant();
        return projectDomainQueryService.findBy(tenantId, pageable, searchBy);
    }

    @Transactional
    public ProjectVersion findVersionBy(ProjectVersionId projectVersionId) throws ProjectVersionNotFoundException {
        return projectDomainQueryService.findVersionBy(projectVersionId);
    }

    @Transactional
    public List<ProjectVersion> findAllVersionsBy(ProjectId projectId) throws ProjectNotFoundException {
        return projectDomainQueryService.findAllVersionsBy(projectId);
    }
}
