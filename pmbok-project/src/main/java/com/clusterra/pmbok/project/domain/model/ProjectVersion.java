/*
 * Copyright (c) 2014.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * 	http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.clusterra.pmbok.project.domain.model;

import org.apache.commons.lang3.Validate;
import com.clusterra.iam.core.application.tenant.TenantId;
import org.hibernate.annotations.GenericGenerator;
import org.springframework.data.annotation.CreatedBy;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedBy;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import java.util.Date;

/**
 * Created by dkuchugurov on 10.05.2014.
 */
@Entity
@EntityListeners({AuditingEntityListener.class})
@Table(name = "pmb_project_version")
public class ProjectVersion {

    @Id
    @GeneratedValue(generator = "base64")
    @GenericGenerator(name = "base64", strategy = "com.clusterra.hibernate.base64.Base64IdGenerator")
    private String id;

    @Basic
    private String label;

    @Basic
    @Column(nullable = false)
    private Integer value;

    @Basic
    @Column(nullable = false)
    private Boolean main;

    @Basic
    @Column(nullable = false)
    private String tenantId;

    @ManyToOne
    private Project project;

    @Basic
    @CreatedBy
    private String createdByUserId;

    @Basic
    @CreatedDate
    private Date createdDate;

    @Basic
    @LastModifiedBy
    private String modifiedByUserId;

    @Basic
    @LastModifiedDate
    private Date modifiedDate;

    ProjectVersion() {
    }

    public ProjectVersion(TenantId tenantId, Project project, String label, Integer value, Boolean main) {
        Validate.notNull(tenantId, "tenantId is null");
        Validate.notNull(value, "value is null");
        Validate.notNull(project, "project is null");
        Validate.notNull(label, "label is null");
        this.tenantId = tenantId.getId();
        this.label = label;
        this.value = value;
        this.project = project;
        setMain(main);
    }

    public Project getProject() {
        return project;
    }

    public ProjectVersionId getProjectVersionId() {
        return new ProjectVersionId(id);
    }

    public String getLabel() {
        return label;
    }

    public Integer getValue() {
        return value;
    }

    public void setMain(Boolean main) {
        Validate.notNull(main, "main is null");
        this.main = main;
    }

    public Boolean isMain() {
        return main;
    }

    public String getCreatedByUserId() {
        return createdByUserId;
    }

    public Date getCreatedDate() {
        return createdDate;
    }

    public String getModifiedByUserId() {
        return modifiedByUserId;
    }

    public Date getModifiedDate() {
        return modifiedDate;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        ProjectVersion that = (ProjectVersion) o;

        if (id != null ? !id.equals(that.id) : that.id != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        return id != null ? id.hashCode() : 0;
    }
}
