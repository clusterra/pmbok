/*
 * Copyright 2012 - 2013 www.equmo.org
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * 	http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.clusterra.pmbok.project.domain.model.repo;

import com.clusterra.pmbok.project.domain.model.Project;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;

/**
 * Created with IntelliJ IDEA.
 *
 * @author Denis Kuchugurov
 *         Date: 03.12.13
 */
public interface ProjectRepository extends PagingAndSortingRepository<Project, String>, JpaSpecificationExecutor<Project> {

    @Query("select p from Project p where p.tenantId = ?1 and p.name = ?2")
    Project findByTenantIdAndName(String tenantId, String name);

    @Query("select v.project from ProjectVersion v where v.id = ?1")
    Project findByProjectVersionId(String projectVersionId);
}
