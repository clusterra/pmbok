/*
 * Copyright (c) 2014.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * 	http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.clusterra.pmbok.project.domain.service;

import com.clusterra.pmbok.project.domain.model.ProjectVersionId;
import com.clusterra.pmbok.common.domain.EntityNotFoundException;
import com.clusterra.pmbok.project.domain.model.ProjectId;

/**
 * Created by dkuchugurov on 18.05.2014.
 */
public class ProjectVersionNotFoundException extends EntityNotFoundException {
    public ProjectVersionNotFoundException(ProjectVersionId projectVersionId) {
        super("project version not found by " + projectVersionId);
    }

    public ProjectVersionNotFoundException(ProjectId projectId) {
        super("main project version not found by " + projectId);
    }
}
