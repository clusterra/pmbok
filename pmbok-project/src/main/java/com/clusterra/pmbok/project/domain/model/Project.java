/*
 * Copyright 2012 - 2013 www.equmo.org
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * 	http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.clusterra.pmbok.project.domain.model;

import com.clusterra.iam.core.application.tenant.TenantId;
import com.clusterra.iam.core.application.user.UserId;
import com.clusterra.pmbok.project.domain.model.stakeholder.Stakeholder;
import org.apache.commons.lang3.Validate;
import org.hibernate.annotations.GenericGenerator;
import org.springframework.data.annotation.CreatedBy;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedBy;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 *
 * @author Denis Kuchugurov
 *         Date: 03.12.13
 */
@Entity
@EntityListeners({AuditingEntityListener.class})
@Table(name = "pmb_project", uniqueConstraints = {@UniqueConstraint(columnNames = {"tenantId", "name"})})
public class Project {

    @Id
    @GeneratedValue(generator = "base64")
    @GenericGenerator(name = "base64", strategy = "com.clusterra.hibernate.base64.Base64IdGenerator")
    private String id;

    @Basic
    @Column(nullable = false)
    private String tenantId;

    @Basic
    private String name;

    @Basic
    @CreatedBy
    private String createdByUserId;

    @Basic
    @CreatedDate
    private Date createdDate;

    @Basic
    @LastModifiedBy
    private String modifiedByUserId;

    @Basic
    @LastModifiedDate
    private Date modifiedDate;

    @Basic
    private int touchCounter;

    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    private List<Stakeholder> stakeholders;


    Project() {
    }

    public Project(TenantId tenantId, String name) {
        Validate.notNull(tenantId, "tenantId is null");
        Validate.notNull(name, "name is null");
        this.tenantId = tenantId.getId();
        this.name = name;
        this.touchCounter = 0;
        this.stakeholders = new ArrayList<>();
    }

    public void addStakeholder(Stakeholder stakeholder) {
        Validate.notNull(stakeholder);
        stakeholders.add(stakeholder);
    }

    public void removeStakeholder(Stakeholder stakeholder) {
        Validate.notNull(stakeholder);
        stakeholders.remove(stakeholder);
    }

    public List<Stakeholder> getStakeholders() {
        return Collections.unmodifiableList(stakeholders);
    }

    public void touch() {
        touchCounter++;
    }

    public ProjectId getProjectId() {
        return new ProjectId(id);
    }

    public String getName() {
        return name;
    }

    public Date getCreatedDate() {
        return createdDate;
    }

    public UserId getCreatedByUserId() {
        return new UserId(createdByUserId);
    }

    public Date getModifiedDate() {
        return modifiedDate;
    }

    public UserId getModifiedByUserId() {
        return new UserId(modifiedByUserId);
    }

    public TenantId getTenantId() {
        return new TenantId(tenantId);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Project project = (Project) o;

        if (id != null ? !id.equals(project.id) : project.id != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        return id != null ? id.hashCode() : 0;
    }

}
