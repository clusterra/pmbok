/*
 * Copyright 2012 - 2013 www.equmo.org
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * 	http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.clusterra.pmbok.project.domain.service;

import com.clusterra.pmbok.project.domain.model.ProjectVersionId;
import com.clusterra.pmbok.common.domain.EntityNotFoundException;
import com.clusterra.pmbok.project.domain.model.ProjectId;

/**
 * Created with IntelliJ IDEA.
 *
 * @author Denis Kuchugurov
 *         Date: 09.12.13
 */
public class ProjectNotFoundException extends EntityNotFoundException {


    public ProjectNotFoundException(ProjectId projectId) {
        super("project not found by " + projectId);
    }

    public ProjectNotFoundException(String message) {
        super(message);
    }

    public ProjectNotFoundException(ProjectVersionId projectVersionId) {
        super("project not found by " + projectVersionId);
    }
}
