/*
 * Copyright 2012 - 2013 www.equmo.org
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * 	http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.clusterra.pmbok.project.domain.todo;

import org.apache.commons.lang3.Validate;

import java.util.HashSet;
import java.util.Set;

/**
 * Created with IntelliJ IDEA.
 *
 * @author Denis Kuchugurov
 *         Date: 07.12.13
 */
public class Sow {

    private Set<BusinessNeed> businessNeeds;

    private String productScopeDescription;

    private String strategicPlan;

    Sow() {
    }

    public Sow(BusinessNeed businessNeed, String productScopeDescription, String strategicPlan) {
        businessNeeds = new HashSet<>();
        addBusinessNeed(businessNeed);
        setProductScopeDescription(productScopeDescription);
        setStrategicPlan(strategicPlan);
    }

    public void addBusinessNeed(BusinessNeed businessNeed) {
        Validate.notNull(businessNeed, "businessNeed is null");
        this.businessNeeds.add(businessNeed);
    }

    public void removeBusinessNeed(BusinessNeed businessNeed) {
        Validate.notNull(businessNeed, "businessNeed is null");
        Validate.isTrue(businessNeeds.size() > 1, "businessNeeds size must be > 1");
        this.businessNeeds.remove(businessNeed);
    }

    public String getProductScopeDescription() {
        return productScopeDescription;
    }

    public String getStrategicPlan() {
        return strategicPlan;
    }

    public void setProductScopeDescription(String productScopeDescription) {
        Validate.notEmpty(productScopeDescription, "productScopeDescription is empty");
        this.productScopeDescription = productScopeDescription;
    }

    public void setStrategicPlan(String strategicPlan) {
        Validate.notEmpty(strategicPlan, "strategicPlan is empty");
        this.strategicPlan = strategicPlan;
    }

}
