
    create table pmb_project (
        id varchar(255) not null,
        createdByUserId varchar(255),
        createdDate timestamp,
        modifiedByUserId varchar(255),
        modifiedDate timestamp,
        name varchar(255),
        tenantId varchar(255) not null,
        touchCounter int4 not null,
        primary key (id)
    );

    create table pmb_project_pmb_stakeholder (
        pmb_project_id varchar(255) not null,
        stakeholders_id varchar(255) not null
    );

    create table pmb_project_version (
        id varchar(255) not null,
        createdByUserId varchar(255),
        createdDate timestamp,
        label varchar(255),
        main boolean not null,
        modifiedByUserId varchar(255),
        modifiedDate timestamp,
        tenantId varchar(255) not null,
        value int4 not null,
        project_id varchar(255),
        primary key (id)
    );

    create table pmb_stakeholder (
        id varchar(255) not null,
        classification varchar(255),
        createdByUserId varchar(255),
        createdDate timestamp,
        modifiedByUserId varchar(255),
        modifiedDate timestamp,
        name varchar(255),
        role varchar(255),
        tenantId varchar(255) not null,
        type varchar(255),
        primary key (id)
    );

    alter table pmb_project 
        add constraint UK_chrkljxl5583yclr8fs1xnuqs  unique (tenantId, name);

    alter table pmb_project_pmb_stakeholder 
        add constraint UK_ryd5i19ypjxoi6dyyjw0gpvt8  unique (stakeholders_id);

    alter table pmb_project_pmb_stakeholder 
        add constraint FK_ryd5i19ypjxoi6dyyjw0gpvt8 
        foreign key (stakeholders_id) 
        references pmb_stakeholder;

    alter table pmb_project_pmb_stakeholder 
        add constraint FK_m7or937yaw6nfpfkva46u8pvf 
        foreign key (pmb_project_id) 
        references pmb_project;

    alter table pmb_project_version 
        add constraint FK_gvtaev2sr4psy2g0o9yq89hi6 
        foreign key (project_id) 
        references pmb_project;
